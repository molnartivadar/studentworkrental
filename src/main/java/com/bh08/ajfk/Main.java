package com.bh08.ajfk;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.bh08.ajfk.models.Admin;
import com.bh08.ajfk.models.City;
import com.bh08.ajfk.models.Company;
import com.bh08.ajfk.models.CompanyRating;
import com.bh08.ajfk.models.Contract;
import com.bh08.ajfk.models.ContractState;
import com.bh08.ajfk.models.Country;
import com.bh08.ajfk.models.Days;
import com.bh08.ajfk.models.Gender;
import com.bh08.ajfk.models.Job;
import com.bh08.ajfk.models.News;
import com.bh08.ajfk.models.Skill;
import com.bh08.ajfk.models.Student;
import com.bh08.ajfk.models.quiz.Answer;
import com.bh08.ajfk.models.quiz.CorrectAnswer;
import com.bh08.ajfk.models.quiz.Question;

public class Main {


	public static void main(String[] args) {
/*

		Skill skill = new Skill();
		skill.setSkillName("Java");
		Skill skill2 = new Skill();
		skill2.setSkillName("Matek");
		Skill skill3 = new Skill();
		skill3.setSkillName("Angol");
		Skill skill4 = new Skill();
		skill4.setSkillName("MS Excel");
		Skill skill5 = new Skill();
		skill5.setSkillName("Takarítás");
		Skill skill6 = new Skill();
		skill6.setSkillName("Hegesztés");
		Skill skill7 = new Skill();
		skill7.setSkillName("HTML");

		List<Skill> skills = new ArrayList<Skill>();
		skills.add(skill);
		skills.add(skill3);
		skills.add(skill2);

		Job job = new Job();
		job.setJobName("Java programozo gyakornok");
		job.setJobDescription(
				"Ez egy allas, ahol megtanulhatsz Javaban programozni, nagyon izgalmas es sokretu feladatokkal fogsz talalkozni");
		job.setHourlyWage(new BigDecimal(1500));
		job.setSkills(skills);
		job.setWorkPlace("Pécs");
		job.setHoursOfWorkInOneWeek(20);
		job.setActive(true);
		
		Job job2 = new Job();
		job2.setJobName("Hegesztő");
		job2.setJobDescription(
				"Ez egy allas, ahol megtanulhatsz Hegeszteni, különböző eszközökkel");
		job2.setHourlyWage(new BigDecimal(1500));
		List<Skill> skills2 = new ArrayList<Skill>();
		skills2.add(skill6);
		job2.setSkills(skills2);
		job2.setWorkPlace("Pécs");
		job2.setHoursOfWorkInOneWeek(20);
		job2.setActive(true);
		
		
		Job job3 = new Job();
		job3.setJobName("Takarító");
		job3.setJobDescription(
				"Ez egy allas, ahol megtanulhatsz takarítani, különböző eszközökkel");
		job3.setHourlyWage(new BigDecimal(1300));
		List<Skill> skills3 = new ArrayList<Skill>();
		skills3.add(skill5);
		job3.setSkills(skills3);
		job3.setWorkPlace("Pécs");
		job3.setHoursOfWorkInOneWeek(20);
		job3.setActive(true);
		
		Job job4 = new Job();
		job4.setJobName("Titkárnő");
		job4.setJobDescription(
				"Ez egy allas, ahol megtanulhatsz irodát vezetni, különböző eszközökkel");
		job4.setHourlyWage(new BigDecimal(2000));
		List<Skill> skills4 = new ArrayList<Skill>();
		skills4.add(skill3);
		skills4.add(skill4);
		job4.setSkills(skills4);
		job4.setWorkPlace("Szeged");
		job4.setHoursOfWorkInOneWeek(30);
		job4.setActive(true);
		
		Job job5 = new Job();
		job5.setJobName("Front end fejlesztő");
		job5.setJobDescription(
				"Ez egy allas, ahol megtanulhatsz front end technikákat, különböző eszközökkel");
		job5.setHourlyWage(new BigDecimal(2000));
		List<Skill> skills5 = new ArrayList<Skill>();
		skills5.add(skill2);
		skills5.add(skill3);
		skills5.add(skill7);
		job5.setSkills(skills5);
		job5.setWorkPlace("Debrecen");
		job5.setHoursOfWorkInOneWeek(40);
		job5.setActive(true);
		
		Job job6 = new Job();
		job6.setJobName("Munka6");
		job6.setJobDescription(
				"Ez egy allas, ahol megtanulhatsz Munka6, különböző eszközökkel");
		job6.setHourlyWage(new BigDecimal(2500));
		List<Skill> skills6 = new ArrayList<Skill>();
		skills6.add(skill);
		skills6.add(skill2);
		skills6.add(skill4);
		job6.setSkills(skills6);
		job6.setWorkPlace("Debrecen");
		job6.setHoursOfWorkInOneWeek(30);
		job6.setActive(true);
		
		Job job7 = new Job();
		job7.setJobName("Munka7");
		job7.setJobDescription(
				"Ez egy allas, ahol megtanulhatsz Munka7, különböző eszközökkel");
		job7.setHourlyWage(new BigDecimal(3000));
		List<Skill> skills7 = new ArrayList<Skill>();
		skills7.add(skill);
		skills7.add(skill2);
		skills7.add(skill4);
		skills7.add(skill7);
		job7.setSkills(skills7);
		job7.setWorkPlace("Győr");
		job7.setHoursOfWorkInOneWeek(25);
		job7.setActive(true);
		
		Job job8 = new Job();
		job8.setJobName("Munka8");
		job8.setJobDescription(
				"Ez egy allas, ahol megtanulhatsz Munka8, különböző eszközökkel");
		job8.setHourlyWage(new BigDecimal(1400));
		List<Skill> skills8 = new ArrayList<Skill>();
		skills8.add(skill3);
		skills8.add(skill7);
		job8.setSkills(skills8);
		job8.setWorkPlace("Győr");
		job8.setHoursOfWorkInOneWeek(32);
		job8.setActive(true);
		
		Job job9 = new Job();
		job9.setJobName("Munka9");
		job9.setJobDescription(
				"Ez egy allas, ahol megtanulhatsz Munka9, különböző eszközökkel");
		job9.setHourlyWage(new BigDecimal(1200));
		List<Skill> skills9 = new ArrayList<Skill>();
		skills9.add(skill);
		job9.setSkills(skills9);
		job9.setWorkPlace("Győr");
		job9.setHoursOfWorkInOneWeek(30);
		job9.setActive(true);
		
		Job job10 = new Job();
		job10.setJobName("Munka10");
		job10.setJobDescription(
				"Ez egy allas, ahol megtanulhatsz Munka10, különböző eszközökkel");
		job10.setHourlyWage(new BigDecimal(1000));
		List<Skill> skills10 = new ArrayList<Skill>();
		skills10.add(skill3);
		skills10.add(skill2);
		job10.setSkills(skills10);
		job10.setWorkPlace("Győr");
		job10.setHoursOfWorkInOneWeek(15);
		job10.setActive(true);

		List<String> preferedWorkPlace = new ArrayList<String>();
		preferedWorkPlace.add("Budapest");
		preferedWorkPlace.add("Esztergom");
		Map<Skill, Integer> skillsMap = new TreeMap<Skill, Integer>();
		skillsMap.put(skill, 2);
		skillsMap.put(skill2, 4);
		skillsMap.put(skill3, 5);
		
		Map<Skill, Double> rankMap = new TreeMap<Skill, Double>();
		rankMap.put(skill, 2.2);
		rankMap.put(skill2, 4.12);
		rankMap.put(skill3, 3.765);

		Days monday = new Days("Hétfő");
		Days tuesday = new Days("Kedd");
		Days wednesday = new Days("Szerda");
		Days thursday = new Days("Csütörtök");
		Days friday = new Days("Péntek");
		Days saturday = new Days("Szombat");
		Days sunday = new Days("Vasárnap");

		List<Days> freeTime = new ArrayList<Days>();
		freeTime.add(monday);
		freeTime.add(tuesday);
		freeTime.add(wednesday);
		freeTime.add(thursday);
		freeTime.add(friday);
		freeTime.add(saturday);
		freeTime.add(sunday);

		Student student = new Student();
		student.setAddress("Kossuth utca 7.");
		student.setBirthDate(LocalDate.now());
		student.setBirthPlace("Vac");
		student.setCity("Budapest");
		student.setCountry("Magyarorszag");
		student.setCv("ez egy link");
		student.setEMail("1234@gmail.com");
		student.setFirstName("Bela");
		student.setFreeTime(freeTime);
		student.setLastName("Toth");
		student.setMotherName("Virag Ibolya");
		student.setPassword("$2a$10$3Vu4jaZvrCpFkTIK5RetZOQslpdTs1MrBL4jP3SaG2qi98rA5FogG");
		student.setPhoneNumber("06301234567");
		student.setPreferedHourlyWage(BigDecimal.valueOf(100000));
		student.setPreferedWorkPlace(preferedWorkPlace);
		student.setProfilePicture("profilePicture link");
		student.setRank(rankMap);
		student.setSkills(skillsMap);
		student.setTaxId("8684828585");
		student.setZipCode("1234");
		student.setGender(Gender.FÉRFI);
		student.setGdpr(true);
		student.setRegistrationDate(LocalDateTime.now());
		
		CompanyRating cr = new CompanyRating();
		cr.setRating(4);
		cr.setRateDate(LocalDate.of(2018, 7, 12));
		cr.setStudent(student);
		cr.setRatingDescription("Ez egy nagyon jó kis cég. Egy élmény volt eltölteni a szakmai gyakorlatomat, az egyetlen negatív amit fel tudnék sorolni, hogy nincs ingyen kávé.");
		
		List<CompanyRating> crList = new ArrayList<>();
		crList.add(cr);
		

		Company company = new Company();
		company.setAddress("Kossuth utca");
		company.setCity("Baja");
		company.setCompanyName("UPC");
		company.setCountry("Magyarorszag");
		company.setDescription("johely");
		company.setEMail("eMail");
		company.setRegistrationDate(LocalDateTime.now());
		
		List<Job> offeredJobs = new ArrayList<Job>();
		offeredJobs.add(job);
		offeredJobs.add(job2);
		offeredJobs.add(job3);
		offeredJobs.add(job4);
		offeredJobs.add(job5);
		company.setOfferedJobs(offeredJobs);
		company.setPassword("$2a$10$4.pWG0R/af/K3Y/6lyx8beY.sRgZhDEAc.vbK./DIlGyrcP5tpOQ.");
		company.setPhoneNumber("06303030303");
		company.setTaxNumber("12345678-1-21");
		company.setZipCode("1234");

		job.setCompany(company);
		job2.setCompany(company);
		job3.setCompany(company);
		job4.setCompany(company);
		job5.setCompany(company);
		
		
		Company company2 = new Company();
		company2.setAddress("Kossuth utca");
		company2.setCity("Budapest");
		company2.setCompanyName("KFC");
		company2.setCountry("Magyarorszag");
		company2.setDescription("Ez itt a KFC");
		company2.setEMail("kfc@gmail.com");
		company2.setRegistrationDate(LocalDateTime.now());
		
		List<Job> offeredJobs2 = new ArrayList<Job>();
		offeredJobs2.add(job6);
		offeredJobs2.add(job7);
		offeredJobs2.add(job8);
		offeredJobs2.add(job9);
		offeredJobs2.add(job10);
		company2.setOfferedJobs(offeredJobs);
		company2.setPassword("$2a$10$4.pWG0R/af/K3Y/6lyx8beY.sRgZhDEAc.vbK./DIlGyrcP5tpOQ.");
		company2.setPhoneNumber("06303030303");
		company2.setTaxNumber("3456789-1-21");
		company2.setZipCode("1234");
		company2.setRating(crList);

		job6.setCompany(company2);
		job7.setCompany(company2);
		job8.setCompany(company2);
		job9.setCompany(company2);
		job10.setCompany(company2);

		Admin admin = new Admin();
		admin.setAddress("Kossuth utca 8.");
		admin.setCity("Budapest");
		admin.setCountry("Magyarorszag");
		admin.setEMail("admin@gmail.com");
		admin.setPassword("$2a$10$Db4UwM6djrep3Ea.ewe4xOkFH2xhwCnyUqP9NQhSCKHwkGxr/QqeC");
		admin.setForName("Ede");
		admin.setLastName("Kiss");
		admin.setZipCode("1234");
		admin.setPhoneNumber("06303030300");
		admin.setProfilePicture("profilePicture link");
		admin.setRegistrationDate(LocalDateTime.now());

		Contract contract = new Contract();
		contract.setEndDate(LocalDate.of(2019, 6, 30));
		contract.setStartDate(LocalDate.of(2019, 3, 1));
		contract.setContractDate(LocalDate.of(2019, 2, 10));
		contract.setCompany(company);
		contract.setJob(job);
		contract.setStudent(student);
		contract.setState(ContractState.ACTIVE);
		
		Contract contract2 = new Contract();
		contract2.setEndDate(LocalDate.of(2018, 6, 30));
		contract2.setStartDate(LocalDate.of(2018, 3, 1));
		contract2.setContractDate(LocalDate.of(2018, 2, 10));
		contract2.setCompany(company2);
		contract2.setJob(job5);
		contract2.setStudent(student);
		contract2.setState(ContractState.EXPIRED);
		
		Contract contract3 = new Contract();
		contract3.setEndDate(LocalDate.of(2017, 2, 10));
		contract3.setStartDate(LocalDate.of(2016, 11, 1));
		contract3.setContractDate(LocalDate.of(2016, 10, 20));
		contract3.setCompany(company);
		contract3.setJob(job3);
		contract3.setStudent(student);
		contract3.setState(ContractState.DECLINED);
		
		
		Contract contract4 = new Contract();
		contract4.setEndDate(LocalDate.of(2019, 12, 31));
		contract4.setStartDate(LocalDate.of(2019, 7, 1));
		contract4.setContractDate(LocalDate.of(2019, 3, 8));
		contract4.setCompany(company2);
		contract4.setJob(job4);
		contract4.setStudent(student);
		contract4.setState(ContractState.DRAFT);
		
		Contract contract5 = new Contract();
		contract5.setEndDate(LocalDate.of(2019, 11, 30));
		contract5.setStartDate(LocalDate.of(2019, 7, 1));
		contract5.setContractDate(LocalDate.of(2019, 2, 24));
		contract5.setCompany(company);
		contract5.setJob(job10);
		contract5.setStudent(student);
		contract5.setState(ContractState.DRAFT);
		
		News news1 = new News();
		news1.setHeading("Videós önéletrajz");
		news1.setText(

				"Ez a teljesen új, kreatív forma Nyugat-Európában és Amerikában már egyre inkább elterjedt. A videós önéletrajz rövid, maximum 2-3 perces profi kisfilm, amivel a "
						+ "jelentkező bemutatja legfontosabb kompetenciáit, illetve beszél kicsit a szakmai múltjáról is. Lényege, hogy a hagyományos önéletrajzhoz a munkakereső még pluszban "
						+ "csatol egy figyelemfelkeltő kisfilmet is magáról, amiben még inkább meg tudja magát, illetve a saját személyiségét mutatni. További előnyei, hogy még ha virtuálisan is, "
						+ "de 'személyes benyomást' tehetünk arra, aki megnézi, illetve megmutathatjuk a kommunikációs stílusunkat is. Itthon ez még teljesen újdonságnak számít, egyelőre nem elterjedt"
						+ " a használata, de egyszer mindent el kell kezdeni, nem igaz? Összefoglalásként azt mondhatjuk, a jó önéletrajz rövid, megfelelően tördelt, feltétlenül rád, a kompetenciáidra"
						+ " szabott, illetve alkalmazkodik a megpályázott munkakörhöz. Hogy Te melyiket fogod használni a fentebbi minták közül, rád van bízva. A lényeg, hogy figyeld mindig az éppen"
						+ " aktuális trendeket!");

		news1.setPrimary(false);
		news1.setActive(true);

		News news2 = new News();
		news2.setHeading("Gerilla vagy kreatív önéletrajz");
		news2.setText(
				"Formabontó megoldás, ami nem a klasszikus formára épül, teljesen más szerkesztettségű, mint a megszokott CV-knek. Lényege, hogy leginkább a személyes kompetenciákat, "
						+ "valamint a látványt helyezi a fókuszba, amivel esetleg ki lehet tűnni a tömegből. Csakhogy jó tudnod, hogy Magyarországon ez még nem igazán elterjedt és egyelőre mind a "
						+ "munkakeresők, mind pedig a toborzók vonakodnak tőle. Egyáltalán nem mindegy tehát, hogy hová akarsz jelentkezni. Ha mondjuk egy banki pozíciót néztél ki, oda semmi esetre"
						+ " sem ajánlatos ilyet használni. Ahol mégis érdemes lehet megpróbálnod ilyen típussal pályázni azok a grafikusi vagy például a kreatív marketinggel kapcsolatos munkakörök. "
						+ "Ott beletrafálhatsz egy ilyen nem mindennapi önéletrajzzal, hiszen sok olyan információt feltüntethetünk magadról, amelyek egy hagyományosba biztosan nem férnének bele és nem"
						+ " is illenek. ");
		news2.setPrimary(false);
		news2.setActive(true);

		News news3 = new News();
		news3.setHeading("Így készülj az állásinterjúra!");
		news3.setText(

				"A személyes interjú talán a legfontosabb mérföldkő a munkavállalás szempontjából, ahol rövid idő alatt kell 'eladnod magad', így nem árt tudni, hogyan készülhetsz fel rá,"
						+ " illetve mi az, amire mindenképp érdemes odafigyelned. Ha a leendő munkaadódtól pozitív választ kaptál az önéletrajzodra, előtted a lehetőség, hogy személyesen is meggyőzd arról,"
						+ " téged bizony erre a pozícióra teremtettek. Alapvetően három típusát különböztetjük meg az állásinterjúknak: van, amikor a cégen keresztül egy személyzeti tanácsadó interjúztat,"
						+ " előfordulhat továbbá, hogy egy úgynevezett 'headhunter', vagyis fejvadász hív be elbeszélgetésre, illetve klasszikusnak mondhatjuk azt az interjúhelyzetet, melyben egyenesen a cég"
						+ " egyik munkatársával, esetleg magával a menedzserrel találkozol. Az alapos felkészülés minden esetben elengedhetetlen! A visszahívás, vagy viszontválasz azt jelenti, hogy sikeresen"
						+ " bekerültél egy olyan szűkebb körbe, ahol rajtad kívül még több lehetséges pályázó is van (akár többkörös kiválasztással is találkozhatsz), így a cég csak abban az esetben választ ki"
						+ " téged, ha a legmegfelelőbbnek tart a személyes elbeszélgetést követően. ");

		news3.setPrimary(true);
		news3.setActive(true);

		News news4 = new News();
		news4.setHeading("Europass önéletrajz");
		news4.setText(
				"Az Europass önéletrajz az Európai Unióban használt, egységes rugalmas formanyomtatvány, ami ugyan a megszokott önéletrajzi formátumot követi, ám meglehetősen nagy hangsúlyt fektet "
						+ "a munkatapasztalat tartalma, az iskolában szerzett tudás, valamint az egyéni készségek konkrét bemutatására. Célja, hogy a pályázók életét megkönnyítse, hiszen online szerkesztőfelületen"
						+ " készíthetjük el a CV-t. Az előre megadott nyolc fő pont igény szerint törölhetők, bővíthetők, illetve a sorrendjük is változtatható, ezenkívül bármilyen dokumentumot csatolhatunk"
						+ " kedvünkre. Ez a formula a mai napig elég népszerű, főként a pályakezdők körében, azonban az az általános tapasztalat, hogy nem valami hatékony ez a típus, mivel túlságosan sok "
						+ "lényegtelen információt tartalmaz. Emiatt pedig hosszúra nyúlik, amit a cégeknél lévő HR-esek nem szeretnek. Mindent egybevetve tehát nem egy rossz formula ez, de ha teheted, "
						+ "inkább ne ezzel próbálj munkát keresgélni. ");
		news4.setPrimary(false);
		news4.setActive(true);
		
		Question question1 = new Question();
		question1.setQuestion("Mennyi 2 + 2?");
		question1.setSkill(skill2);
		
		Answer answer1 = new Answer();
		answer1.setAnswer("5");
		answer1.setQuestion(question1);
		
		Answer answer2 = new Answer();
		answer2.setAnswer("4");
		answer2.setQuestion(question1);
		
		Answer answer3 = new Answer();
		answer3.setAnswer("8");
		answer3.setQuestion(question1);
		
		CorrectAnswer correctAnswer1 = new CorrectAnswer();
		correctAnswer1.setAnswer(answer2);
		
		Question question2 = new Question();
		question2.setQuestion("Mi 1000 köbgyöke?");
		question2.setSkill(skill2);
		
		Answer answer4 = new Answer();
		answer4.setAnswer("10");
		answer4.setQuestion(question2);
		
		Answer answer5 = new Answer();
		answer5.setAnswer("100");
		answer5.setQuestion(question2);
		
		Answer answer6 = new Answer();
		answer6.setAnswer("50");
		answer6.setQuestion(question2);
		
		Answer answer7 = new Answer();
		answer7.setAnswer("91");
		answer7.setQuestion(question2);
		
		CorrectAnswer correctAnswer2 = new CorrectAnswer();
		correctAnswer2.setAnswer(answer4);

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("StudentWorkPU");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();

		em.persist(news1);
		em.persist(news2);
		em.persist(news3);
		em.persist(news4);

		em.persist(skill);
		em.persist(skill2);
		em.persist(skill3);
		em.persist(skill4);
		em.persist(skill5);
		em.persist(skill6);
		em.persist(skill7);
		
		em.persist(question1);
		em.persist(question2);
		
		em.persist(answer1);
		em.persist(answer2);
		em.persist(answer3);
		em.persist(answer4);
		em.persist(answer5);
		em.persist(answer6);
		em.persist(answer7);
		
		em.persist(correctAnswer1);
		em.persist(correctAnswer2);
		
		em.persist(job);
		em.persist(job2);
		em.persist(job3);
		em.persist(job4);
		em.persist(job5);
		em.persist(job6);
		em.persist(job7);
		em.persist(job8);
		em.persist(job9);
		em.persist(job10);
				
		em.persist(monday);
		em.persist(tuesday);
		em.persist(wednesday);
		em.persist(thursday);
		em.persist(friday);
		em.persist(saturday);
		em.persist(sunday);
		
		em.persist(student);
		em.persist(company);
		em.persist(company2);
		em.persist(contract);
		em.persist(contract2);
		em.persist(contract3);
		em.persist(contract4);
		em.persist(contract5);
		em.persist(admin);

		String countrys = "C:\\tmp\\country.csv";

		RandomAccessFile raf;
		String sor;
		
		try {
			raf = new RandomAccessFile(countrys, "r");

			for (sor = raf.readLine(); sor != null; sor = raf.readLine()) {
				Country country = new Country();
				country.setCountry(sor);
				em.persist(country);
			}
			
			raf.close();
		} catch (IOException e) {
			System.out.println("HIBA");
		}
		
		String citys = "C:\\tmp\\city.csv";

		RandomAccessFile raf2;
		String sor2;
		
		try {
			raf2 = new RandomAccessFile(citys, "r");

			for (sor2 = raf2.readLine(); sor2 != null; sor2 = raf2.readLine()) {
				City city = new City();
				city.setCity(sor2);
				em.persist(city);
			}
			
			raf2.close();
		} catch (IOException e) {
			System.out.println("HIBA");
		}
		
		

		tx.commit();

		em.close();
		emf.close();
*/
	}

}