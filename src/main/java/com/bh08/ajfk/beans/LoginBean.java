package com.bh08.ajfk.beans;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private boolean validLogin;
	private boolean invalidLogin;

	public LoginBean() {

		this.validLogin = false;
		this.invalidLogin = false;

	}

	public void reset() {

		this.validLogin = false;
		this.invalidLogin = false;

	}

	public void clearFlash() {

		setInvalidLogin(false);

	}

}
