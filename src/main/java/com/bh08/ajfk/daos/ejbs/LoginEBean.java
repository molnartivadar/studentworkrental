package com.bh08.ajfk.daos.ejbs;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.bh08.ajfk.models.City;
import com.bh08.ajfk.models.Company;
import com.bh08.ajfk.models.CompanyRating;
import com.bh08.ajfk.models.Contract;
import com.bh08.ajfk.models.ContractState;
import com.bh08.ajfk.models.Country;
import com.bh08.ajfk.models.Job;
import com.bh08.ajfk.models.Job_;
import com.bh08.ajfk.models.Skill;
import com.bh08.ajfk.models.Student;
import com.bh08.ajfk.models.User;
import com.bh08.ajfk.utils.SharedFunctions;

@Stateless
@LocalBean
public class LoginEBean {

	@PersistenceContext
	private EntityManager em;

	public Optional<User> getUserByEmail(String email) {

		TypedQuery<User> user = em.createQuery("SELECT t FROM User t WHERE t.eMail = :email", User.class);
		user.setParameter("email", email);

		try {

			User result = user.getSingleResult();
			return Optional.of(result);

		} catch (NoResultException ex) {

			return Optional.empty();

		}

	}

	public List<City> getAllCity() {

		TypedQuery<City> city = em.createQuery("SELECT c FROM City c ORDER BY c.city", City.class);
		List<City> cityList = city.getResultList();
		return cityList;

	}

	public List<Country> getAllCountry() {

		TypedQuery<Country> country = em.createQuery("SELECT c FROM Country c ORDER BY c.country", Country.class);
		List<Country> countryList = country.getResultList();
		return countryList;

	}

	public List<Skill> getAllSkill() {

		TypedQuery<Skill> skill = em.createQuery("SELECT c FROM Skill c ORDER BY c.skillName", Skill.class);
		List<Skill> skillList = skill.getResultList();
		return skillList;

	}

	public List<Skill> getAllSkillExceptEnglish() {

		TypedQuery<Skill> skill = em
				.createQuery("SELECT c FROM Skill c WHERE c.skillName != 'Angol' ORDER BY c.skillName", Skill.class);
		List<Skill> skillList = skill.getResultList();
		return skillList;

	}

	public Optional<User> getUserById(Long id) {

		User result = em.find(User.class, id);

		if (null == result) {

			return Optional.empty();

		} else {

			return Optional.of(em.find(User.class, id));

		}

	}

	public Optional<Job> getJobById(Long contractId) {

		Job result = em.find(Job.class, contractId);

		if (null == result) {

			return Optional.empty();

		} else {

			return Optional.of(em.find(Job.class, contractId));

		}

	}

	public List<Skill> getSkillsByName(List<String> skills) {

		TypedQuery<Skill> skillList = em.createQuery("SELECT c FROM Skill c WHERE c.skillName in :skills", Skill.class);
		skillList.setParameter("skills", skills);
		List<Skill> searchedJobs = skillList.getResultList();
		return searchedJobs;

	}

	public List<Job> searchJobs(String jobName, BigDecimal hourlyWageFromUser, Integer min, Integer max,
			List<Skill> skillList, List<String> workplaceFromUser) {

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Job> cq = cb.createQuery(Job.class);
		Root<Job> job = cq.from(Job.class);
		cq.select(job);

		Predicate p = null;
		if (!"".equals(jobName)) {

			p = cb.like(cb.upper(job.get(Job_.jobName)), ("%" + jobName + "%").toUpperCase());

		} else {

			p = cb.conjunction();

		}

		Predicate p2 = null;
		if (null != hourlyWageFromUser) {

			p2 = cb.ge(job.get(Job_.hourlyWage), hourlyWageFromUser);

		} else {

			p2 = cb.conjunction();

		}

		Predicate p3 = null;
		if (null != min) {

			p3 = cb.between(job.get(Job_.hoursOfWorkInOneWeek), min, max);

		} else {

			p3 = cb.conjunction();

		}

		Predicate p4 = null;
		if (!skillList.isEmpty()) {

			Expression<List<Skill>> exp = job.get(Job_.skills);
			p4 = exp.in(skillList);

		} else {

			p4 = cb.conjunction();

		}

		Predicate p5 = null;
		if (!workplaceFromUser.isEmpty()) {

			Expression<String> parentExpression = job.get(Job_.workPlace);
			p5 = parentExpression.in(workplaceFromUser);

		} else {

			p5 = cb.conjunction();

		}

		Predicate pFull = cb.and(p, p2, p3, p4, p5);

		cq.where(pFull);
		TypedQuery<Job> q = em.createQuery(cq);
		List<Job> searchedJobList = q.getResultList();
		return searchedJobList;

	}

	public List<Contract> getContractsByStudent(Student student) {

		TypedQuery<Contract> contracts = em.createQuery("SELECT c FROM Contract c WHERE c.student =:student",
				Contract.class);
		contracts.setParameter("student", student);
		List<Contract> contractList = contracts.getResultList();
		return contractList;

	}

	public void setContractState(Long contractId, boolean accept, Student student) {

		Contract contract = em.find(Contract.class, contractId);

		if (student.equals(contract.getStudent())) {

			if (true == accept) {

				contract.setState(ContractState.ACTIVE);

			} else {

				contract.setState(ContractState.DECLINED);

			}

			em.merge(contract);
		}
	}

	public List<Company> getCompanyWithoutRating(Student student) {

		Query q = em.createNativeQuery(
				"SELECT a.* FROM users_company a where a.id in (SELECT company_id FROM contract a "
						+ "where a.student_id = ? and state='EXPIRED') and a.id not in (select b.company_id from companyrating a"
						+ " left join users_company_companyrating b on a.id=b.rating_id where a.student_id = ?)",
				Company.class);
		q.setParameter(1, student.getId());
		q.setParameter(2, student.getId());

		return SharedFunctions.castList(Company.class, q.getResultList());

	}

	public void saveRatingTheDatabase(Student student, Long companyId, Integer ratingNumber, String description) {

		CompanyRating companyRating = new CompanyRating();
		companyRating.setRateDate(LocalDate.now());
		companyRating.setRating(ratingNumber);
		companyRating.setRatingDescription(description);
		companyRating.setStudent(student);

		Company company = em.find(Company.class, companyId);
		List<CompanyRating> crRating = company.getRating();
		crRating.add(companyRating);

		em.merge(company);

	}



}
