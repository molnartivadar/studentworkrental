package com.bh08.ajfk.daos.ejbs.admin;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.bh08.ajfk.models.Admin;
import com.bh08.ajfk.models.User;

@Stateless
@LocalBean
public class AdminRegisterBean {

	@PersistenceContext
	private EntityManager em;

	public void saveAdminInDatabase(Admin admin) {
		em.persist(admin);
	}

	public boolean isRegisteredEmailAddress(String eMail) {
		
		TypedQuery<User> user = em.createQuery("SELECT t FROM User t WHERE t.eMail = :eMail", User.class);
		user.setParameter("eMail", eMail);

		try {
			User result = user.getSingleResult();
			return true;
		} catch (NoResultException ex) {
			return false;
		}
	}
}
