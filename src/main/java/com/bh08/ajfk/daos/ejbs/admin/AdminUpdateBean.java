package com.bh08.ajfk.daos.ejbs.admin;

import java.util.Optional;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.*;

import com.bh08.ajfk.models.Admin;

@Stateless
@LocalBean
public class AdminUpdateBean {

	@PersistenceContext
	private EntityManager em;

	public void updateAdminParameter(String eMail, String column, String param) {
		
		StringBuilder query = new StringBuilder();
		query.append("UPDATE Admin t SET t.").append(column).append(" = :").append(column).append(" WHERE t.eMail = :eMail");
		
		Query admin = em.createQuery(query.toString(), Admin.class);
		admin.setParameter(column, param);
		admin.setParameter("eMail", eMail);
		admin.executeUpdate();
	}

	public Optional<Admin> getUpdatedAdmin(String eMail) {
		
		TypedQuery<Admin> admin = em.createQuery(
				"SELECT t FROM Admin t WHERE t.eMail = :eMail", Admin.class);
		admin.setParameter("eMail", eMail);

		try {
			Admin result = admin.getSingleResult();
			return Optional.of(result);

		} catch (NoResultException ex) {
			return Optional.empty();
		}
		
	}
}
