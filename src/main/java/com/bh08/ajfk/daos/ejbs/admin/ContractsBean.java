package com.bh08.ajfk.daos.ejbs.admin;

import java.time.LocalDate;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.bh08.ajfk.models.Contract;

@Stateless
@LocalBean
public class ContractsBean {

	@PersistenceContext
	private EntityManager em;
	
	public List<Contract> getAllContracts() {
		TypedQuery<Contract> c = em.createQuery("SELECT c FROM Contract c", Contract.class);
		List<Contract> contracts = c.getResultList();
		
		return contracts;
	}

	public List<Contract> getContractsBetweenTwoDates(LocalDate contractDateFrom, LocalDate contractDateTo) {
		TypedQuery<Contract> c = em.createQuery("SELECT c FROM Contract c WHERE c.contractDate BETWEEN :dateFrom AND :dateTo", Contract.class);
		c.setParameter("dateFrom", contractDateFrom);
		c.setParameter("dateTo", contractDateTo);
		List<Contract> contracts = c.getResultList();
		
		return contracts;
	}

	public Contract getContractById(Long id) {
		TypedQuery<Contract> c = em.createQuery("SELECT c FROM Contract c WHERE c.id = :id", Contract.class);
		c.setParameter("id", id);
		Contract contract = c.getSingleResult();
		
		return contract;
	}
}
