package com.bh08.ajfk.daos.ejbs.admin;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.bh08.ajfk.models.News;

@Stateless
@LocalBean
public class NewsAdminBean {
	
	@PersistenceContext
	private EntityManager em;

	public List<News> getNews() {
		
		TypedQuery<News> n = em.createQuery("SELECT n FROM News n", News.class);
		return n.getResultList();
	}

	public void saveNews(boolean isPrimary, String header, String text) {
		
		News news = new News();
		news.setPrimary(isPrimary);
		news.setHeading(header);
		news.setText(text);
		news.setActive(true);
		
		em.persist(news);
		
	}
	
	public void unsetPrimaryNews() {
		
		TypedQuery<News> n = em.createQuery("UPDATE News n SET n.primary = '0' WHERE n.primary = '1'", News.class);
		n.executeUpdate();
	}

	public List<News> getActiveNews() {
		
		TypedQuery<News> n = em.createQuery("SELECT n FROM News n WHERE n.active = '1'", News.class);	
		return n.getResultList();
	}

	public void inactivateNews(News news) {

		TypedQuery<News> n = em.createQuery("UPDATE News n SET n.active = '0' WHERE n.id = :id", News.class);
		n.setParameter("id", news.getId());
		n.executeUpdate();
		
	}

}
