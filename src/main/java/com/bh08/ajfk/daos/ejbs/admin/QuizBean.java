package com.bh08.ajfk.daos.ejbs.admin;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.bh08.ajfk.models.Skill;
import com.bh08.ajfk.models.quiz.Answer;
import com.bh08.ajfk.models.quiz.CorrectAnswer;
import com.bh08.ajfk.models.quiz.Question;
import com.bh08.ajfk.models.quiz.Quiz;

@Stateless
@LocalBean
public class QuizBean {

	@PersistenceContext
	private EntityManager em;

	public List<Skill> getSkillList() {

		TypedQuery<Skill> s = em.createQuery("SELECT s FROM Skill s", Skill.class);
		List<Skill> skills = s.getResultList();

		return skills;
	}

	public List<Quiz> getQuestionsFromDataBase(Skill skill) {
		List<Quiz> result = new ArrayList<Quiz>();

		TypedQuery<Skill> chosenSkill = em.createQuery("SELECT s FROM Skill s WHERE s.skillName = :Skill", Skill.class);
		chosenSkill.setParameter("Skill", skill.getSkillName());
		Skill s = chosenSkill.getSingleResult();

		TypedQuery<Question> countQuery = em.createQuery("SELECT q FROM Question q WHERE q.skill = :Skill",
				Question.class);
		countQuery.setParameter("Skill", s);
		List<Question> qList = countQuery.getResultList();
		int count = qList.size();

		for (int i = 0; i < count; i++) {

			TypedQuery<Question> question = em.createQuery("SELECT q FROM Question q WHERE q.skill = :Skill",
					Question.class);
			question.setParameter("Skill", s);
			question.setFirstResult(i);
			question.setMaxResults(1);

			Question q = question.getSingleResult();

			List<Answer> answers = new ArrayList<Answer>();

			TypedQuery<Answer> answer = em.createQuery("SELECT a FROM Answer a WHERE a.question = :question",
					Answer.class);
			answer.setParameter("question", q);
			answers = answer.getResultList();
			Quiz newQuiz = new Quiz();
			newQuiz.setQuestion(q);
			newQuiz.setAnswers(answers);

			List<CorrectAnswer> correctAnswers = new ArrayList<CorrectAnswer>();

			for (int j = 0; j < answers.size(); j++) {
				TypedQuery<CorrectAnswer> correctAnswer = em
						.createQuery("SELECT c FROM CorrectAnswer c WHERE c.answer = :answer", CorrectAnswer.class);
				correctAnswer.setParameter("answer", answers.get(j));
				try {
					Optional<CorrectAnswer> ca = Optional.of(correctAnswer.getSingleResult());
					if (ca.isPresent()) {
						correctAnswers.add(ca.get());
					}
				} catch (NoResultException e) {

				}
			}
			newQuiz.setCorrectAnswers(correctAnswers);
			result.add(newQuiz);
		}

		return result;

	}

	public void saveNewQuiz(Question question, List<Answer> answers, List<CorrectAnswer> correctAnswers) {
		em.persist(question);
		for (int i = 0; i < answers.size(); i++) {
			Answer answer = answers.get(i);
			em.persist(answer);
		}
		for (int i = 0; i < correctAnswers.size(); i++) {
			CorrectAnswer correctAnswer = correctAnswers.get(i);
			em.persist(correctAnswer);
		}

	}

	public Optional<Skill> getSkillByName(String skillName) {
		TypedQuery<Skill> skill = em.createQuery("SELECT s FROM Skill s WHERE s.skillName = :Skill", Skill.class);
		skill.setParameter("Skill", skillName);
		try {
			Skill s = skill.getSingleResult();
			return Optional.of(s);
		} catch (NoResultException e) {
			return Optional.empty();
		}
	}

	public void saveNewSkill(Skill skill) {
		em.persist(skill);
	}

	public Skill getSkillByQuestionId(Long questionID) {
		TypedQuery<Question> q = em.createQuery("SELECT q FROM Question q WHERE q.id = :questionId", Question.class);
		q.setParameter("questionId", questionID);
		Question question = q.getSingleResult();
		return question.getSkill();
	}

	public void updateQuiz(Question q, List<Answer> answers, List<CorrectAnswer> correctAnswers) {
		
		Query question = em.createQuery("UPDATE Question q SET q.question = :newQuestion WHERE q.id = :questionId", Question.class);
		question.setParameter("newQuestion", q.getQuestion());
		question.setParameter("questionId", q.getId());
		question.executeUpdate();
		
		removeCorrectAnswersByQuestion(q);
		removeAnswersByQuestion(q);
		
		for (int i = 0; i < answers.size(); i++) {
			Answer answer = answers.get(i);
			em.persist(answer);
		}
		for (int i = 0; i < correctAnswers.size(); i++) {
			CorrectAnswer correctAnswer = correctAnswers.get(i);
			em.persist(correctAnswer); 
		}
		 		
	}

	public Question getQuestionById(Long questionID) {
		TypedQuery<Question> q = em.createQuery("SELECT q FROM Question q WHERE q.id = :questionId", Question.class);
		q.setParameter("questionId", questionID);
		Question question = q.getSingleResult();
		return question;
	}

	public void deleteQuiz(Question q) {
		
		removeCorrectAnswersByQuestion(q);
		removeAnswersByQuestion(q);
		removeQuestion(q);
		
	}

	private void removeCorrectAnswersByQuestion(Question q) {
		Query removeCorrectAnswers = em.createQuery("DELETE FROM CorrectAnswer c WHERE c IN (SELECT ca FROM CorrectAnswer ca WHERE ca.answer IN (SELECT a FROM Answer a WHERE a.question = :question))", CorrectAnswer.class);
		removeCorrectAnswers.setParameter("question", q);
		removeCorrectAnswers.executeUpdate();
	}

	private void removeAnswersByQuestion(Question q) {
		Query removeAnswers = em.createQuery("DELETE FROM Answer a WHERE a.question = :question", Answer.class);
		removeAnswers.setParameter("question", q);
		removeAnswers.executeUpdate();
	}

	private void removeQuestion(Question q) {
		Query removeQuestion = em.createQuery("DELETE FROM Question q WHERE q.id = :question", Question.class);
		removeQuestion.setParameter("question", q.getId());
		removeQuestion.executeUpdate();
	}
	
	
}
