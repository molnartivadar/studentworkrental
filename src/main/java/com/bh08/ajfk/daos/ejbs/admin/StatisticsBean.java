package com.bh08.ajfk.daos.ejbs.admin;

import java.time.LocalDate;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.bh08.ajfk.models.Contract;
import com.bh08.ajfk.models.User;

@Stateless
@LocalBean
public class StatisticsBean {
	
	@PersistenceContext
	private EntityManager em;
	
	public List<Contract> getSignedContractsBetweenTwoDates(LocalDate dateFrom, LocalDate dateTo) {
		
		TypedQuery<Contract> c = em.createQuery("SELECT c FROM Contract c WHERE c.contractDate BETWEEN :dateFrom AND :dateTo", Contract.class);
		c.setParameter("dateFrom", dateFrom);
		c.setParameter("dateTo", dateTo);
		
		List<Contract> contracts = c.getResultList();
		
		return contracts;
	}

	public List<Contract> getValidContractsBetweenTwoDates(LocalDate dateFrom, LocalDate dateTo) {
		
		TypedQuery<Contract> c = em.createQuery("SELECT c FROM Contract c WHERE c.startDate <= :dateFrom AND c.endDate >= :dateFrom OR c.startDate <= :dateTo AND c.endDate >= :dateTo OR c.startDate >= :dateFrom AND c.endDate <= :dateTo", Contract.class);
		c.setParameter("dateFrom", dateFrom);
		c.setParameter("dateTo", dateTo);
		
		List<Contract> contracts = c.getResultList();
		
		return contracts;
	}

	public List<User> getRegisteredUsers() {
		
		TypedQuery<User> u = em.createQuery("SELECT u FROM User u", User.class);
		List<User> users = u.getResultList();

		return users;
	}

}
