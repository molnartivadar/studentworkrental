package com.bh08.ajfk.daos.ejbs.admin;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.bh08.ajfk.models.User;

@Stateless
@LocalBean
public class UserListBean {

	@PersistenceContext
	private EntityManager em;

	public List<User> getUsersByType(Class<? extends User> userType) {
		
		TypedQuery<User> u = em.createQuery("SELECT u FROM User u WHERE TYPE(u) = :userType", User.class);
		u.setParameter("userType", userType);
		List<User> users = u.getResultList();

		return users;
	}
	
	
}
