package com.bh08.ajfk.daos.ejbs.company;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.bh08.ajfk.models.Company;
import com.bh08.ajfk.models.Job;
import com.bh08.ajfk.models.Skill;
import com.bh08.ajfk.models.Student;
import com.bh08.ajfk.models.User;

import java.util.ArrayList;
/**
 * Session Bean implementation class CompanyRegisterBean
 */
@Stateless
@LocalBean
public class CompanyRegisterBean {

	@PersistenceContext
	private EntityManager em;

	public Company getCompanyByEmail(String email) {
		TypedQuery<Company> company = em.createQuery("SELECT t FROM Company t WHERE t.eMail = :email", Company.class);
		company.setParameter("email", email);
		Company result = company.getSingleResult();
		return result;
	}

	public Job getSelectedJob(int jobid) {
		TypedQuery<Job> jobs = em.createQuery("SELECT c FROM Job c WHERE c.id = :jobid", Job.class);
		jobs.setParameter("jobid", jobid);
		Job selectedJob = jobs.getSingleResult();
		return selectedJob;
	}	

	public boolean isRegisteredEmailAddress(String email) {

		TypedQuery<Company> company = em.createQuery("SELECT t FROM Company t WHERE t.eMail = :email", Company.class);
		company.setParameter("email", email);

		try {
			Company result = company.getSingleResult();
			return true;
		} catch (NoResultException ex) {
			return false;
		}
	}

	public void saveCompanyInDatabase(Company company, Boolean modifyCompany) {
		if (modifyCompany) {
			em.merge(company);
		} else {
			em.persist(company);
		}
		System.out.println("mentve");
	}

	public void saveJobInDatabase(Job job, Boolean newJob) {
		if (!newJob) {
			em.merge(job);
		} else {
			em.persist(job);
		}
		System.out.println("mentve");

	}
	
}