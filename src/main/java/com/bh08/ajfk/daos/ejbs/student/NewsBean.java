package com.bh08.ajfk.daos.ejbs.student;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.bh08.ajfk.models.Job;
import com.bh08.ajfk.models.News;
import com.bh08.ajfk.models.Student;

@Stateless
@LocalBean
public class NewsBean {

	@PersistenceContext
	private EntityManager em;

	public List<News> getNews() {

		TypedQuery<News> news = em.createQuery("SELECT n FROM News n where n.active = true order by n.primary",
				News.class);
		List<News> newsList = news.getResultList();
		return newsList;

	}

	public List<Student> getAllStudent() {

		TypedQuery<Student> students = em.createQuery("SELECT c FROM Student c", Student.class);
		List<Student> studentList = students.getResultList();
		return studentList;

	}

	public List<Job> getAllJobs() {

		TypedQuery<Job> jobs = em.createQuery("SELECT c FROM Job c where c.isActive = true", Job.class);
		List<Job> jobList = jobs.getResultList();
		return jobList;

	}

}
