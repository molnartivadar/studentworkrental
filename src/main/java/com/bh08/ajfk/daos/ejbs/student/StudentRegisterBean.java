package com.bh08.ajfk.daos.ejbs.student;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.bh08.ajfk.models.Student;
import com.bh08.ajfk.models.User;

@Stateless
@LocalBean
public class StudentRegisterBean {

	@PersistenceContext
	private EntityManager em;

	public boolean isRegisteredEmailAddress(String email) {

		TypedQuery<User> user = em.createQuery("SELECT t FROM User t WHERE t.eMail = :email", User.class);
		user.setParameter("email", email);

		try {

			user.getSingleResult();
			return true;

		} catch (NoResultException ex) {

			return false;

		}
	}

	public void saveStudentInDatabase(Student student) {

		em.persist(student);

	}

	public User uploadProfilePicture(User user) {

		return (User) em.merge(user);

	}

	public Student uploadCv(Student student) {

		return (Student) em.merge(student);

	}

	public Student updateStudentInDatabase(Student student) {

		return (Student) em.merge(student);

	}

}
