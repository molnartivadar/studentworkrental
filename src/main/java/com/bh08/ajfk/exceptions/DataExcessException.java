package com.bh08.ajfk.exceptions;

public class DataExcessException extends Exception {

	private static final long serialVersionUID = 1L;

	public DataExcessException(Throwable e) {
		super("Az adatbázis jelenleg nem elérhető, kérem próbálkozzon később!", e);
	}

}
