package com.bh08.ajfk.exceptions;

public class InvalidDataOperationException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidDataOperationException(String message) {
		super(message);
	}

}
