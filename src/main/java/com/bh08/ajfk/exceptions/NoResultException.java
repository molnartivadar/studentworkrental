package com.bh08.ajfk.exceptions;

public class NoResultException extends Exception {

	private static final long serialVersionUID = 1L;

	public NoResultException(String message) {
		super(message);
	}

}
