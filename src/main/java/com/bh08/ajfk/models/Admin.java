package com.bh08.ajfk.models;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@DiscriminatorValue("A")
@DynamicInsert
@NoArgsConstructor
@Table(name = "USERS_ADMIN")
public class Admin extends User {

	@Column(nullable = false)
	private String forName;

	@Column(nullable = false)
	private String lastName;

	public Admin(String eMail, String password, String zipCode, String country, String city, String address,
			String phoneNumber, String forName, String lastName) {
		super(eMail, password, zipCode, country, city, address, phoneNumber);
		this.forName = forName;
		this.lastName = lastName;
	}

}
