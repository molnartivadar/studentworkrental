package com.bh08.ajfk.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.DynamicInsert;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@DynamicInsert
@NoArgsConstructor
@DiscriminatorValue("C")
@Table(name = "USERS_COMPANY")
public class Company extends User {

	@Column(nullable = false)
	private String companyName;

	@Column(nullable = false, unique = true)
	private String taxNumber;

	@Lob
	@Column(nullable = false)
	private String description;

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Job> offeredJobs;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<CompanyRating> rating;

	public Company(String eMail, String password, String zipCode, String country, String city, String address,
			String phoneNumber, String taxId, String companyName) {

		super(eMail, password, zipCode, country, city, address, phoneNumber);
		this.taxNumber = taxId;
		this.companyName = companyName;
		this.description = "Kérjük töltse ki!";

	}

}
