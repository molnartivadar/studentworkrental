package com.bh08.ajfk.models;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.DynamicInsert;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@DynamicInsert
@NoArgsConstructor
public class CompanyRating {

	@Id
	@GeneratedValue(generator = "companyRatingSequenceGenerator")
	@SequenceGenerator(name = "companyRatingSequenceGenerator", sequenceName = "compRateSeq", initialValue = 1, allocationSize = 1)
	private long id;

	@ManyToOne
	private Student student;

	@Column(nullable = false)
	private int rating;

	@Lob
	private String ratingDescription;

	@Column(nullable = false)
	private LocalDate rateDate;

}
