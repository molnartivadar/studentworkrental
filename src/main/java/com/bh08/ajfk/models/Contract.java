package com.bh08.ajfk.models;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import com.bh08.ajfk.daos.ejbs.converter.LocalDateAttributeConverter;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
public class Contract {

	@Id
	@GeneratedValue(generator = "contractSequenceGenerator")
	@SequenceGenerator(name = "contractSequenceGenerator", sequenceName = "contractSeq", initialValue = 1, allocationSize = 1)
	protected long id;

	@OneToOne
	private Student student;

	@OneToOne
	private Company company;

	@OneToOne
	private Job job;

	@Enumerated(EnumType.STRING)
	private ContractState state;

	@Column(nullable = false)
	private LocalDate startDate;

	@Column(nullable = false)
	private LocalDate endDate;

	@Column(nullable = false)
	@Convert(converter = LocalDateAttributeConverter.class)
	private LocalDate contractDate;

}
