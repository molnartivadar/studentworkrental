package com.bh08.ajfk.models;

public enum ContractState {

	ACTIVE, 
	DRAFT,
	EXPIRED,
	DECLINED

}
