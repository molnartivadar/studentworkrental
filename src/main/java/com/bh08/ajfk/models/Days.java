package com.bh08.ajfk.models;

import java.util.Map;
import java.util.TreeMap;

import javax.persistence.Convert;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import com.bh08.ajfk.utils.BooleanConverter;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Days {

	@Id
	@GeneratedValue(generator = "daysSequenceGenerator")
	@SequenceGenerator(name = "daysSequenceGenerator", sequenceName = "daysSeq", initialValue = 1, allocationSize = 1)
	private long id;

	private String daysName;

	@ElementCollection(fetch = FetchType.EAGER)
	@Convert(converter = BooleanConverter.class/* , attributeName="value" */)
	private Map<Integer, Boolean> hours = new TreeMap<>();

	public Days(String daysName) {

		super();
		this.daysName = daysName;
		for (int i = 0; i < 24; i++) {
			hours.put(Integer.valueOf(i), false);
		}

	}

	public Days(String daysName, Map<Integer, Boolean> hours) {

		super();
		this.daysName = daysName;
		this.hours = hours;

	}

}
