package com.bh08.ajfk.models;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Job {

	@Id
	@GeneratedValue(generator = "jobSequenceGenerator")
	@SequenceGenerator(name = "jobSequenceGenerator", sequenceName = "jobSeq", initialValue = 1, allocationSize = 1)
	private long id;

	@Column(nullable = false)
	private String jobName;

	@Lob
	@Column(nullable = false)
	private String jobDescription;

	@ManyToMany
	private List<Skill> skills;

	@ManyToOne
	private Company company;

	private BigDecimal hourlyWage;

	private Integer hoursOfWorkInOneWeek;

	private String workPlace;

	@Column(nullable = false)
	private boolean isActive;

}
