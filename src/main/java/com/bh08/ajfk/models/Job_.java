package com.bh08.ajfk.models;

import java.math.BigDecimal;

import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Job.class)
public class Job_ {

	public static volatile SingularAttribute<Job, Long> id;

	public static volatile SingularAttribute<Job, String> jobName;

	public static volatile SingularAttribute<Job, String> jobDescription;

	public static volatile ListAttribute<Job, Skill> skills;

	public static volatile SingularAttribute<Job, Company> company;

	public static volatile SingularAttribute<Job, BigDecimal> hourlyWage;

	public static volatile SingularAttribute<Job, Integer> hoursOfWorkInOneWeek;

	public static volatile SingularAttribute<Job, String> workPlace;

	public static volatile SingularAttribute<Job, Boolean> isActive;
}
