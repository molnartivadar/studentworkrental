package com.bh08.ajfk.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Entity
@ToString
public class News {

	@Id
	@GeneratedValue(generator = "newsSequenceGenerator")
	@SequenceGenerator(name = "newsSequenceGenerator", sequenceName = "newSeq", initialValue = 1, allocationSize = 1)
	protected long id;

	private String heading;

	@Lob
	private String text;

	private boolean primary;

	private boolean active;

}
