package com.bh08.ajfk.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Skill implements Comparable<Skill> {

	@Id
	@GeneratedValue(generator = "skillSequenceGenerator")
	@SequenceGenerator(name = "skillSequenceGenerator", sequenceName = "skillSeq", initialValue = 1, allocationSize = 1)
	private long id;

	@Column(unique = true, nullable = false)
	private String skillName;

	@Override
	public String toString() {
		return skillName;
	}

	@Override
	public int compareTo(Skill skill) {
		return this.skillName.compareTo(skill.skillName);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Skill other = (Skill) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
