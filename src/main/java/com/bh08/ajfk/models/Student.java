package com.bh08.ajfk.models;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter(value = AccessLevel.PUBLIC)
@Setter(value = AccessLevel.PUBLIC)
@ToString
@DynamicInsert
@NoArgsConstructor
@DiscriminatorValue("S")
@Table(name = "USERS_STUDENT")
public class Student extends User {

	@Column(nullable = false)
	private String firstName;

	@Column(nullable = false)
	private String lastName;

	@Column(nullable = false)
	private String taxId;

	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	private Gender gender;

	@Column(nullable = false)
	private String motherName;

	@Column(nullable = false)
	private LocalDate birthDate;

	@Column(nullable = false)
	private String birthPlace;

	@ElementCollection(fetch = FetchType.EAGER)
	private Map<Skill, Integer> skills;

	private String cv;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@OrderBy(value = "daysName ASC")
	private List<Days> freeTime;

	private BigDecimal preferedHourlyWage;

	@ElementCollection
	private List<String> preferedWorkPlace;

	@ElementCollection
	private Map<Skill, Double> rank;

	@Column(nullable = false)
	private boolean gdpr;

	public Student(String eMail, String password, String zipCode, String country, String city, String address,
			String phoneNumber, String firstName, String lastName, String taxId, Gender gender, String motherName,
			LocalDate birthDate, String birthPlace, boolean gdpr, List<Days> freeTime) {
		super(eMail, password, zipCode, country, city, address, phoneNumber);
		this.firstName = firstName;
		this.lastName = lastName;
		this.taxId = taxId;
		this.gender = gender;
		this.motherName = motherName;
		this.birthDate = birthDate;
		this.birthPlace = birthPlace;
		this.gdpr = gdpr;
		this.freeTime = freeTime;
	}

}
