package com.bh08.ajfk.models;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicInsert;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter(value = AccessLevel.PUBLIC)
@Setter(value = AccessLevel.PUBLIC)
@NoArgsConstructor
@Entity
@Table(name = "USERS")
@Inheritance(strategy = InheritanceType.JOINED)
@ToString
@DynamicInsert
@DiscriminatorColumn(name = "TYPE", discriminatorType = DiscriminatorType.STRING, length = 20)
public abstract class User {

	@Id
	@GeneratedValue(generator = "userSequenceGenerator")
	@SequenceGenerator(name = "userSequenceGenerator", sequenceName = "userSeq", initialValue = 1, allocationSize = 1)
	protected long id;

	@Column(nullable = false)
	protected String eMail;

	@Column(nullable = false)
	protected String password;

	@Column(nullable = false)
	protected String zipCode;

	@Column(nullable = false)
	protected String country;

	@Column(nullable = false)
	protected String city;

	@Column(nullable = false)
	protected String address;

	@Column(nullable = false)
	protected String phoneNumber;

	@Column(nullable = false)
	protected LocalDateTime registrationDate;

	protected String profilePicture;

	protected BigDecimal balance;

	public User(String eMail, String password, String zipCode, String country, String city, String address,
			String phoneNumber) {

		this.eMail = eMail;
		this.password = password;
		this.zipCode = zipCode;
		this.country = country;
		this.city = city;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.balance = BigDecimal.ZERO;
		this.profilePicture = "profile.png";
		this.registrationDate = LocalDateTime.now();

	}

	@Transient
	public String getDecriminatorValue() {

		return this.getClass().getAnnotation(DiscriminatorValue.class).value();

	}

	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;

	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (id != other.id)
			return false;
		return true;

	}

}
