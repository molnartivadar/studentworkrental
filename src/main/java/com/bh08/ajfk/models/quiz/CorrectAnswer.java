package com.bh08.ajfk.models.quiz;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class CorrectAnswer {

	@Id
	@GeneratedValue(generator = "correctAnswerSequenceGenerator")
	@SequenceGenerator(name = "correctAnswerSequenceGenerator", sequenceName = "correctAnswerSeq", initialValue = 1, allocationSize = 1)
	protected Long id;

	@OneToOne
	private Answer answer;

}
