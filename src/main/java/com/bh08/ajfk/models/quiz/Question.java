package com.bh08.ajfk.models.quiz;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import com.bh08.ajfk.models.Skill;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
public class Question {

	@Id
	@GeneratedValue(generator = "questionSequenceGenerator")
	@SequenceGenerator(name = "questionSequenceGenerator", sequenceName = "questionSeq", initialValue = 1, allocationSize = 1)
	protected Long id;

	@Column(nullable = false)
	private String question;

	@OneToOne
	private Skill skill;

}
