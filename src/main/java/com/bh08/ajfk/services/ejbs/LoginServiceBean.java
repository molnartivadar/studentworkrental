package com.bh08.ajfk.services.ejbs;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.bh08.ajfk.daos.ejbs.LoginEBean;
import com.bh08.ajfk.exceptions.AuthenticationException;
import com.bh08.ajfk.models.City;
import com.bh08.ajfk.models.Company;
import com.bh08.ajfk.models.Contract;
import com.bh08.ajfk.models.Country;
import com.bh08.ajfk.models.Job;
import com.bh08.ajfk.models.Skill;
import com.bh08.ajfk.models.Student;
import com.bh08.ajfk.models.User;
import com.bh08.ajfk.utils.BCrypt;

@Stateless
@LocalBean
public class LoginServiceBean {

	private static final String LOGIN_ERROR_NONEXISTENT_USER = "Nincs ilyen felhasználó az adatbázisban!";
	private static final String LOGIN_ERROR_INVALID = "Hiba a bejelentkezésnél: Hibás email cím vagy felhasználó!";
	
	@EJB
	private LoginEBean loginEBean;

	public User loginUser(String email, String password) throws AuthenticationException {

		Optional<User> result = loginEBean.getUserByEmail(email);

		if (result.isPresent()) {

			User user = result.get();

			if (BCrypt.checkpw(password, user.getPassword())) {
				
				return user;
				
			} else {
				
				throw new AuthenticationException(LOGIN_ERROR_INVALID);
				
			}

		} else {
			
			throw new AuthenticationException(LOGIN_ERROR_INVALID);
			
		}
	}

	public List<City> getAllCity() {
		
		return loginEBean.getAllCity();
		
	}

	public List<Country> getAllCountry() {
		
		return loginEBean.getAllCountry();
		
	}

	public List<Skill> getAllSkill() {
		
		return loginEBean.getAllSkill();
		
	}
	
	public List<Skill> getAllSkillExceptEnglish() {
		
		return loginEBean.getAllSkillExceptEnglish();
		
	}
	
	public User getUserById(String id) throws AuthenticationException {

		try {
			
			Long userId = Long.valueOf(id);

			Optional<User> result = loginEBean.getUserById(userId);

			if (result.isPresent()) {

				return result.get();

			} else {
				
				throw new AuthenticationException(LOGIN_ERROR_NONEXISTENT_USER);
				
			}

		} catch (NumberFormatException ex) {
			
			throw new AuthenticationException(LOGIN_ERROR_NONEXISTENT_USER);
			
		}

	}

	public Job getJobById(String id) throws AuthenticationException, NumberFormatException {

		Long contractId = Long.valueOf(id);

		Optional<Job> result = loginEBean.getJobById(contractId);

		if (result.isPresent()) {

			return result.get();

		} else {
			
			throw new AuthenticationException("Nincs ilyen munka az adatbázisban!");
		}

	}

	public List<Job> searchJobs(String jobName, String hourlyWage, String workTime, String[] skills,
			String[] workplaces) {

		BigDecimal hourlyWageFromUser = null;
		if (!"".equals(hourlyWage)) {
			hourlyWageFromUser = BigDecimal.valueOf(Double.parseDouble(hourlyWage));
		}

		Integer min = null;
		Integer max = null;
		if (!"".equals(workTime)) {
			Integer workTimeFromUser = Integer.parseInt(workTime);
			min = workTimeFromUser - 5;
			max = workTimeFromUser + 5;
		}

		List<Skill> skillList = assembleSkillList(skills);

		List<String> workplaceFromUser = new ArrayList<>();
		if ((null != workplaces)) {

			for (int i = 0; i < workplaces.length; i++) {
				workplaceFromUser.add(workplaces[i]);
			}

		}

		return loginEBean.searchJobs(jobName, hourlyWageFromUser, min, max, skillList, workplaceFromUser);

	}

	private List<Skill> assembleSkillList(String[] skills) {
		List<Skill> skillList = new ArrayList<>();
		if (null != skills) {
			List<String> skillListFromUser = new ArrayList<>();
			for (int i = 0; i < skills.length; i++) {
				skillListFromUser.add(skills[i]);
			}

			skillList = loginEBean.getSkillsByName(skillListFromUser);

		}
		return skillList;
	}

	public List<Contract> getContractsByStudent(Student student) {

		return loginEBean.getContractsByStudent(student);

	}

	public void setContractState(String id, boolean accept, Student student) {

		Long contractId = Long.valueOf(id);
		loginEBean.setContractState(contractId, accept, student);

	}
	
	public List<Company> getCompanyWithoutRating(Student student){
		
		return loginEBean.getCompanyWithoutRating(student);
		
	}
	
	public void saveRatingTheDatabase(Student student,String company,String rating,String description) throws AuthenticationException{
		
		try {
			
			Long companyId = Long.valueOf(company);
			Integer ratingNumber = Integer.parseInt(rating);
			
			loginEBean.saveRatingTheDatabase(student,companyId,ratingNumber,description);
		
		} catch (NumberFormatException ex) {
			throw new AuthenticationException("Hiba a bevitt adatokban!");
		}
		
	}

}
