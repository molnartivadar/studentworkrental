package com.bh08.ajfk.services.ejbs.admin;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.bh08.ajfk.daos.ejbs.admin.AdminRegisterBean;
import com.bh08.ajfk.exceptions.RegisterException;
import com.bh08.ajfk.models.Admin;
import com.bh08.ajfk.utils.BCrypt;
import com.bh08.ajfk.utils.SharedFunctions;

@Stateless
@LocalBean
public class AdminRegisterServiceBean {
	
	@EJB
	private AdminRegisterBean adminRegisterBean;
	
	public Admin saveNewAdmin(String address, String city, String country, String eMail, String forName, String lastName, String password1, String password2, String phoneNumber, String zipCode, BigDecimal balance) throws RegisterException {
		
		if (null == address || null == city || null == country || null == eMail || null == forName || null == lastName 
				|| null == password1 || null == password2 || null == phoneNumber || null == zipCode) {
			throw new RegisterException("A kötelező mezők hiányosak!");
		}
		
		if (!SharedFunctions.isValidEmailAddress(eMail)) {
			throw new RegisterException("Érvénytelen e-mail cím!");
		}
		
		if (adminRegisterBean.isRegisteredEmailAddress(eMail)) {
			throw new RegisterException("Ez az e-mail cím már foglalt!");
		}
		
		if (!SharedFunctions.passwordOk(password1)) {
			throw new RegisterException("A jelszónak legalább 6 karakternek kell lennie!");
		}
		
		if (!password1.equals(password2)) {
			throw new RegisterException("A két jelsző nem egyezik!");
		}
		
		Admin admin = new Admin();
		admin.setAddress(address);
		admin.setCity(city);
		admin.setCountry(country);
		admin.setEMail(eMail);
		admin.setForName(forName);
		admin.setLastName(lastName);
		admin.setPassword(BCrypt.hashpw(password1, BCrypt.gensalt()));
		admin.setPhoneNumber(phoneNumber);
		admin.setZipCode(zipCode);
		admin.setBalance(new BigDecimal(0));
		admin.setProfilePicture("profile.png");
		admin.setRegistrationDate(LocalDateTime.now());
		
		adminRegisterBean.saveAdminInDatabase(admin);
		
		return admin;
	}

}
