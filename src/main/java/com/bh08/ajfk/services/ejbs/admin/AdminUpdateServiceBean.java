package com.bh08.ajfk.services.ejbs.admin;

import java.util.Optional;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.bh08.ajfk.daos.ejbs.admin.AdminUpdateBean;
import com.bh08.ajfk.exceptions.RegisterException;
import com.bh08.ajfk.models.Admin;
import com.bh08.ajfk.utils.SharedFunctions;

@Stateless
@LocalBean
public class AdminUpdateServiceBean {

	@EJB
	private AdminUpdateBean adminUpdateBean;

	public void updateAdminData(String eMail, String column, String param) throws RegisterException {

		adminUpdateBean.updateAdminParameter(eMail, column, param);

	}

	public void updateAdminPassword(String eMail, String password1, String password2) throws RegisterException {

			if (!SharedFunctions.passwordOk(password1)) {
				throw new RegisterException("A jelszónak legalább 6 karakternek kell lennie!");
			}

			if (!password1.equals(password2)) {
				throw new RegisterException("A két jelszó nem egyezik!");
			}

			adminUpdateBean.updateAdminParameter(eMail, "password", password1);
	}

	public Admin getUpdatedAdmin(String eMail) {

		Optional<Admin> result = adminUpdateBean.getUpdatedAdmin(eMail);

		if (result.isPresent()) {
			Admin admin = result.get();
			return admin;
		}

		return null;
	}

	public void updateAdmin(Admin admin, String eMail, String password1, String password2, String lastName, String forName,
			String country, String zipCode, String city, String address, String phoneNumber, String profilePicture) {

		try {

			if (password1.length() > 0 && password1.equals(password2)) {
				updateAdminPassword(eMail, password1, password2);
			}

			if (!admin.getLastName().equals(lastName)) {
				updateAdminData(eMail, "lastName", lastName);
			}

			if (!admin.getForName().equals(forName)) {
				updateAdminData(eMail, "forName", forName);
			}

			if (null == country) {
				country = admin.getCountry();
			}

			if (!admin.getCountry().equals(country)) {
				updateAdminData(eMail, "country", country);
			}

			if (!admin.getZipCode().equals(zipCode)) {
				updateAdminData(eMail, "zipCode", zipCode);
			}

			if (!admin.getCity().equals(city)) {
				updateAdminData(eMail, "city", city);
			}

			if (!admin.getAddress().equals(address)) {
				updateAdminData(eMail, "address", address);
			}

			if (!admin.getPhoneNumber().equals(phoneNumber)) {
				updateAdminData(eMail, "phoneNumber", phoneNumber);
			}

			if (null != profilePicture && profilePicture.length() != 0) {
				updateAdminData(eMail, "profilePicture", profilePicture);
			}

		} catch (RegisterException e) {

		}

	}

}
