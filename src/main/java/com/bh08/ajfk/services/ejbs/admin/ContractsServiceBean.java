package com.bh08.ajfk.services.ejbs.admin;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.bh08.ajfk.daos.ejbs.admin.ContractsBean;
import com.bh08.ajfk.models.Contract;
import com.bh08.ajfk.models.ContractState;

@Stateless
@LocalBean
public class ContractsServiceBean {

	@EJB
	private ContractsBean contractsBean;

	public List<Contract> getAllContracts(String studentName, String companyName, String jobName, String contractState,
			LocalDate contractDateFrom, LocalDate contractDateTo) {
		
		ContractState state = null;
		if (!"ALL".equals(contractState)) {
			state = ContractState.valueOf(contractState);
		}

		List<Contract> contracts = contractsBean.getContractsBetweenTwoDates(contractDateFrom, contractDateTo);
		List<Contract> result = new ArrayList<Contract>();
		

		// TODO: move filtering logic to the database
		for (int i = 0; i < contracts.size(); i++) {
			String studentFullName = contracts.get(i).getStudent().getLastName() + " "
					+ contracts.get(i).getStudent().getFirstName();
			if (studentFullName.toLowerCase().contains(studentName.toLowerCase())
					&& contracts.get(i).getCompany().getCompanyName().toLowerCase().contains(companyName.toLowerCase())
					&& contracts.get(i).getJob().getJobName().toLowerCase().contains(jobName.toLowerCase())
					&& state == null || contracts.get(i).getState().equals(state)) {
				result.add(contracts.get(i));
			}
		}

		return result;
	}

	public Contract getContractById(String contractId) {

		Long id = Long.valueOf(contractId);
		Contract contract = contractsBean.getContractById(id);
		
		return contract;
	}
}
