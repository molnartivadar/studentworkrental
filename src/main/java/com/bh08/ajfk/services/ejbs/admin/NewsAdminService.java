package com.bh08.ajfk.services.ejbs.admin;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.bh08.ajfk.daos.ejbs.admin.NewsAdminBean;
import com.bh08.ajfk.models.News;

@Stateless
@LocalBean
public class NewsAdminService {

	@EJB
	private NewsAdminBean newsAdminBean;

	public List<News> getNews() {

		List<News> sortedNews = newsAdminBean.getNews().stream().sorted(Comparator.comparing(News::getId).reversed())
				.collect(Collectors.toList());

		return sortedNews;
	}

	public void saveNews(String isPrimaryString, String header, String text) {

		boolean isPrimary = false;
		if ("isPrimary".equals(isPrimaryString)) {

			isPrimary = true;
			newsAdminBean.unsetPrimaryNews();
		}

		while (newsAdminBean.getActiveNews().size() >= 4) {

			setInactiveTheOldestNotPrimaryNews();
		}

		newsAdminBean.saveNews(isPrimary, header, text);

	}

	private void setInactiveTheOldestNotPrimaryNews() {

		List<News> sortedActiveNews = newsAdminBean.getActiveNews().stream().sorted(Comparator.comparing(News::getId))
				.collect(Collectors.toList());

		for (int i = 0; i < sortedActiveNews.size(); i++) {

			News news = sortedActiveNews.get(i);

			if (!news.isPrimary()) {

				news.setActive(false);
				newsAdminBean.inactivateNews(news);
				i = sortedActiveNews.size();
			}
		}
	}

}
