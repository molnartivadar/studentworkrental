package com.bh08.ajfk.services.ejbs.admin;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.bh08.ajfk.daos.ejbs.admin.QuizBean;
import com.bh08.ajfk.daos.ejbs.student.StudentRegisterBean;
import com.bh08.ajfk.exceptions.InvalidDataOperationException;
import com.bh08.ajfk.exceptions.NoResultException;
import com.bh08.ajfk.models.Skill;
import com.bh08.ajfk.models.Student;
import com.bh08.ajfk.models.quiz.Answer;
import com.bh08.ajfk.models.quiz.CorrectAnswer;
import com.bh08.ajfk.models.quiz.Question;
import com.bh08.ajfk.models.quiz.Quiz;

@Stateless
@LocalBean
public class QuizServiceBean {

	@EJB
	private QuizBean quizBean;
	
	@EJB
	private StudentRegisterBean studentRegisterBean;

	public List<Skill> getSkillList() {
		List<Skill> skills = new ArrayList<Skill>();
		skills = quizBean.getSkillList();
		return skills;
	}

	public List<Quiz> getQuizList(Skill skill) {
		return quizBean.getQuestionsFromDataBase(skill);
	}

	public void saveNewQuiz(String skillName, String question, List<String> a, List<String> ca)
			throws NoResultException {

		Optional<Skill> optionalSkill = quizBean.getSkillByName(skillName);

		if (optionalSkill.isPresent()) {

			Skill skill = optionalSkill.get();
			Question q = new Question();
			q.setQuestion(question);
			q.setSkill(skill);

			List<Answer> answers = new ArrayList<Answer>();
			List<CorrectAnswer> correctAnswers = new ArrayList<CorrectAnswer>();
			for (int i = 0; i < a.size(); i++) {
				if (!"".equals(a.get(i))) {
					Answer answer = new Answer();
					answer.setAnswer(a.get(i));
					answer.setQuestion(q);
					answers.add(answer);
					if (null != ca.get(i)) {
						CorrectAnswer correctAnswer = new CorrectAnswer();
						correctAnswer.setAnswer(answer);
						correctAnswers.add(correctAnswer);
					}
				}
			}
			quizBean.saveNewQuiz(q, answers, correctAnswers);
		} else {
			throw new NoResultException("A kapott skill nem szerepel az adatbázisban: " + skillName);
		}

	}

	public void saveNewQuizFromCsv(File file) throws NoResultException {

		// TODO: move to config file
		String uploadPath = "C:\\tmp\\upload\\" + file.getName();
		BufferedReader br = null;
		
		try {
			br = new BufferedReader(new FileReader(uploadPath));
			String line;
			while ((line = br.readLine()) != null && !line.isEmpty()) {
				String[] fields = removeApostrophes(line.split(";"));
				Optional<Skill> optionalSkill = quizBean.getSkillByName(fields[0]);

				if (optionalSkill.isPresent()) {

					Skill skill = optionalSkill.get();
					Question question = new Question();
					question.setQuestion(fields[1]);
					question.setSkill(skill);

					List<Answer> answers = new ArrayList<Answer>();
					List<CorrectAnswer> correctAnswers = new ArrayList<CorrectAnswer>();

					for (int i = 2; i < fields.length; i++) {
						Answer answer = new Answer();
						CorrectAnswer correctAnswer = new CorrectAnswer();

						if (!"x".equalsIgnoreCase(fields[i])) {
							answer.setAnswer(fields[i]);
							answer.setQuestion(question);
							answers.add(answer);
							if ((i + 1 < fields.length) && "x".equalsIgnoreCase(fields[i + 1])) {
								correctAnswer.setAnswer(answer);
								correctAnswers.add(correctAnswer);
								i++;
							}
						}
					}
					quizBean.saveNewQuiz(question, answers, correctAnswers);
				} else {
					throw new NoResultException("A kapott skill nem szerepel az adatbázisban: " + fields[0]);
				}
			}

		} catch (FileNotFoundException e) {
			System.out.println("File not found: " + e.getMessage());
		} catch (IOException e) {
			System.out.println("IO exception: " + e.getMessage());
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	private String[] removeApostrophes(String[] array) {

		array[0] = array[0].substring(1, array[0].length());
		int n = array.length;
		array[n - 1] = array[n - 1].substring(0, array[n - 1].length() - 1);

		return array;

	}

	public Skill saveNewSkill(String skillName) throws InvalidDataOperationException {

		if (null == skillName || "".equals(skillName)) {
			throw new InvalidDataOperationException("Érvénytelen, vagy üres adat!");
		}

		List<Skill> skills = getSkillList();

		for (int i = 0; i < skills.size(); i++) {
			if (skillName.equalsIgnoreCase(skills.get(i).getSkillName())) {
				throw new InvalidDataOperationException(skillName + " már létezik a rendszerben!");
			}
		}

		Skill skill = new Skill();
		skill.setSkillName(skillName);
		quizBean.saveNewSkill(skill);

		return skill;
	}

	public Skill getSkillByName(String skillName) throws InvalidDataOperationException{

		Optional<Skill> optionalSkill = quizBean.getSkillByName(skillName);

		Skill skill = null;
		
		if (optionalSkill.isPresent()) {
			 skill = optionalSkill.get();
		}else {
			throw new InvalidDataOperationException("Nincs ilyen skill az adatbázisban");
		}
		
		return skill;
	}
	
	public List<Quiz> getRandomQuizBySkillAndNumberOfQuestions(Skill skill,int numberOfQuestion){
		List<Quiz> allQuiz = quizBean.getQuestionsFromDataBase(skill);
		Collections.shuffle(allQuiz);
		
		List<Quiz> resultQuizList = new ArrayList<Quiz>();
		
		for (int i = 0; i < numberOfQuestion; i++) {
			resultQuizList.add(allQuiz.get(i));
		}
		
		return resultQuizList;
		
		
	}
	
	public void saveEnglishTest(Student student,int quizPoints) throws InvalidDataOperationException{
		
		Skill skill = getSkillByName("Angol");
		Map<Skill,Integer> skillList = student.getSkills();
		int skillPoints = (int) Math.round(quizPoints/2.0);
		if(skillPoints==0) {
			skillPoints=1;
		}
		skillList.put(skill, skillPoints);
		student.setSkills(skillList);
		studentRegisterBean.updateStudentInDatabase(student);
		
	}

	public Skill getSkillByQuestionId(Long questionID) {
		Skill skill = quizBean.getSkillByQuestionId(questionID);
		return skill;
	}

	public void updateQuiz(Long questionId, Skill skill, String question, List<String> a, List<String> ca) {
		
		Question q = new Question();
		q.setId(questionId);
		q.setQuestion(question);
		q.setSkill(skill);
		
		List<Answer> answers = new ArrayList<Answer>();
		List<CorrectAnswer> correctAnswers = new ArrayList<CorrectAnswer>();
		for (int i = 0; i < a.size(); i++) {
			if (!"".equals(a.get(i))) {
				Answer answer = new Answer();
				answer.setAnswer(a.get(i));
				answer.setQuestion(q);
				answers.add(answer);
				if (null != ca.get(i)) {
					CorrectAnswer correctAnswer = new CorrectAnswer();
					correctAnswer.setAnswer(answer);
					correctAnswers.add(correctAnswer);
				}
			}
		}
		
		quizBean.updateQuiz(q, answers, correctAnswers);
		
	}

	public void deleteQuiz(Long deleteQuestionId) {
		Question q = quizBean.getQuestionById(deleteQuestionId);
		quizBean.deleteQuiz(q);
		
	}

}
