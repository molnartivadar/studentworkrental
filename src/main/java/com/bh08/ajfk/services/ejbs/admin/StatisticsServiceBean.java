package com.bh08.ajfk.services.ejbs.admin;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.json.JSONArray;
import org.json.JSONObject;

import com.bh08.ajfk.daos.ejbs.admin.StatisticsBean;
import com.bh08.ajfk.models.Contract;
import com.bh08.ajfk.models.User;

@Stateless
@LocalBean
public class StatisticsServiceBean {

	@EJB
	private StatisticsBean statisticsBean;
	
	private final double SUCCESSFEE = 0.1;

	public JSONArray getNumberOfSignedContractsBetweenTwoDates(LocalDate dateFrom, LocalDate dateTo) {

		List<Contract> contracts = statisticsBean.getSignedContractsBetweenTwoDates(dateFrom, dateTo);

		JSONArray result = new JSONArray();
		JSONObject obj = new JSONObject();

		for (LocalDate date = dateFrom; date.isBefore(dateTo.plusDays(1)); date = date.plusDays(1)) {
			for (int i = 0; i < contracts.size(); i++) {
				if (date.getYear() == contracts.get(i).getContractDate().getYear()
						&& date.getDayOfYear() == contracts.get(i).getContractDate().getDayOfYear()) {

					if (obj.has(date.toString())) {
						obj.increment(date.toString());
					} else {
						obj.put(date.toString(), 1);
					}
				} else {
					if (!obj.has(date.toString())) {
						obj.put(date.toString(), 0);
					}
				}

			}
		}
		
		result.put(obj);
		return result;
	}

	public JSONArray getIncomesFromSignedContractsBetweenTwoDates(LocalDate dateFrom, LocalDate dateTo) {

		List<Contract> contracts = statisticsBean.getValidContractsBetweenTwoDates(dateFrom, dateTo);
		
		JSONArray result = new JSONArray();
		JSONObject obj = new JSONObject();

		for (LocalDate date = dateFrom; date.isBefore(dateTo.plusDays(1)); date = date.plusDays(1)) {
			for (int i = 0; i < contracts.size(); i++) {
				if (date.getYear() >= contracts.get(i).getStartDate().getYear()
						&& date.getDayOfYear() >= contracts.get(i).getStartDate().getDayOfYear()
						&& date.getYear() <= contracts.get(i).getEndDate().getYear()
						&& date.getDayOfYear() <= contracts.get(i).getEndDate().getDayOfYear()) {

					if (obj.has(date.toString())) {
						BigDecimal oldValue = obj.getBigDecimal(date.toString());
						obj.remove(date.toString());
						BigDecimal newValue = oldValue.add(getDailyIncomeFromContract(contracts.get(i)));
						obj.put(date.toString(), newValue);
					} else {
						obj.put(date.toString(), getDailyIncomeFromContract(contracts.get(i)));
					}
				} else {
					if (!obj.has(date.toString())) {
						obj.put(date.toString(), BigDecimal.valueOf(0));
					}
				}

			}
		}
		
		result.put(obj);
		return result;
	}
	
	private BigDecimal getDailyIncomeFromContract (Contract contract) {
		BigDecimal hourlyWage = contract.getJob().getHourlyWage();
		BigDecimal avgWorkHoursPerDay = BigDecimal.valueOf(SUCCESSFEE * contract.getJob().getHoursOfWorkInOneWeek() / 7);
		BigDecimal result = hourlyWage.multiply(avgWorkHoursPerDay);
		return result;
	}

	public Map<String, Integer> getRegisteredUsersGroupByType() {
		List<User> registeredUsers = statisticsBean.getRegisteredUsers();
		
		Map<String, Integer> result = new HashMap<String, Integer>();
		
		for (int i = 0; i < registeredUsers.size(); i++) {
			if (result.containsKey(registeredUsers.get(i).getDecriminatorValue())) {
				result.replace(registeredUsers.get(i).getDecriminatorValue(), result.get(registeredUsers.get(i).getDecriminatorValue()) + 1);
			} else {
				result.put(registeredUsers.get(i).getDecriminatorValue(), 1);
			}
		}
		return result;
	}

}
