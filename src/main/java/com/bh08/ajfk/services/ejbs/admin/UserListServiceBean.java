package com.bh08.ajfk.services.ejbs.admin;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.bh08.ajfk.daos.ejbs.admin.UserListBean;
import com.bh08.ajfk.models.User;

@Stateless
@LocalBean
public class UserListServiceBean {

	@EJB
	private UserListBean userListBean;

	public List<User> getUserList(String userType) {

		List<User> users = new ArrayList<User>();
		
		Class<? extends User> user;
		
		switch (userType) {
		case "Admin":
			user = com.bh08.ajfk.models.Admin.class;
			users = userListBean.getUsersByType(user);
			break;
		case "Company":
			user = com.bh08.ajfk.models.Company.class;
			users = userListBean.getUsersByType(user);
			break;
		case "Student":
			user = com.bh08.ajfk.models.Student.class;
			users = userListBean.getUsersByType(user);
			break;
		}
		
		return users;

	}

}
