package com.bh08.ajfk.services.ejbs.company;

import java.math.BigDecimal;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.bh08.ajfk.daos.ejbs.company.CompanyRegisterBean;
import com.bh08.ajfk.daos.ejbs.student.StudentRegisterBean;
import com.bh08.ajfk.exceptions.RegisterException;
import com.bh08.ajfk.models.Company;
import com.bh08.ajfk.models.Gender;
import com.bh08.ajfk.models.Job;
import com.bh08.ajfk.models.Skill;
import com.bh08.ajfk.models.Student;
import com.bh08.ajfk.utils.BCrypt;
import com.bh08.ajfk.utils.SharedFunctions;

import java.util.ArrayList;
import java.util.List;

@Stateless
@LocalBean
public class CompanyRegisterServiceBean {

	@EJB
	private CompanyRegisterBean companyRegisterBean;

	public void saveCompany(String email, String password, String password2, String zipCode, String country,
			String city, String address, String phone, String companyName, String taxNumber, String description,
			Boolean modifyCompany) throws RegisterException {

		if (modifyCompany) {
			password2 = password;
		}

		if (null == email || null == password || null == password2 || null == country || null == taxNumber
				|| null == phone || null == zipCode || null == city || null == address || null == companyName) {
			throw new RegisterException("Nem töltötte ki az összes adatot!");
		}
		if (!SharedFunctions.isValidEmailAddress(email)) {
			throw new RegisterException("Érvénytelen email cím!");
		}
		if (companyRegisterBean.isRegisteredEmailAddress(email) && !modifyCompany) {
			throw new RegisterException("Ez az email cím már foglalt!");
		}
		if (!SharedFunctions.passwordOk(password)) {
			throw new RegisterException("Nem elég hosszú a jelszó!");
		}
		if (!password.equals(password2)) {
			throw new RegisterException("Nem egyezik a két jelszó!");
		}
		String encryptedPassword = BCrypt.hashpw(password, BCrypt.gensalt());

		if (modifyCompany) {

			Company company = companyRegisterBean.getCompanyByEmail(email);
			company.setEMail(email);
			company.setZipCode(zipCode);
			company.setCountry(country);
			company.setCity(city);
			company.setAddress(address);
			company.setPhoneNumber(phone);
			company.setCompanyName(companyName);
			company.setTaxNumber(taxNumber);
			company.setDescription(description);

			companyRegisterBean.saveCompanyInDatabase(company, modifyCompany);

		} else {
			Company company = new Company(email, encryptedPassword, zipCode, country, city, address, phone, taxNumber,
					companyName);

			companyRegisterBean.saveCompanyInDatabase(company, modifyCompany);
		}

	}

	public void saveJob(String jobName, String hourlyWage, String workPlace, String hoursOfWorkInOneWeek,
			String jobDescription, List<Skill> skills, Company company, Boolean newJob, int jobID) throws RegisterException {

		if (null == jobName || null == hourlyWage || null == workPlace || null == hoursOfWorkInOneWeek
				|| null == jobDescription) {
			throw new RegisterException("Nem töltötte ki az összes adatot!");
		}
		Job job;
		if (newJob) {
			job = new Job();
			
		} else {
			job = companyRegisterBean.getSelectedJob(jobID);
		}
		job.setJobName(jobName);
		job.setHourlyWage(BigDecimal.valueOf(Integer.parseInt(hourlyWage)));
		job.setWorkPlace(workPlace);
		job.setHoursOfWorkInOneWeek(Integer.parseInt(hoursOfWorkInOneWeek));
		job.setJobDescription(jobDescription);
		job.setActive(true);
		job.setSkills(skills);
		job.setCompany(company);
		companyRegisterBean.saveJobInDatabase(job, newJob);
		

	}
}
