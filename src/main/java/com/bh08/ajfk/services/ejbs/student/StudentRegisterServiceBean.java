package com.bh08.ajfk.services.ejbs.student;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.bh08.ajfk.daos.ejbs.LoginEBean;
import com.bh08.ajfk.daos.ejbs.student.StudentRegisterBean;
import com.bh08.ajfk.exceptions.RegisterException;
import com.bh08.ajfk.models.Days;
import com.bh08.ajfk.models.Gender;
import com.bh08.ajfk.models.Skill;
import com.bh08.ajfk.models.Student;
import com.bh08.ajfk.models.User;
import com.bh08.ajfk.utils.BCrypt;
import com.bh08.ajfk.utils.SharedFunctions;

@Stateless
@LocalBean
public class StudentRegisterServiceBean {

	@EJB
	private StudentRegisterBean studentRegisterBean;

	@EJB
	private LoginEBean loginEBean;

	public void saveStudent(String email, String password, String password2, String lastName, String firstName,
			String gender, String motherName, String birthPlace, String birthDate, String taxNumber, String phone,
			String country, String zipCode, String city, String address, String gdpr) throws RegisterException {

		isAllDataNotFilled(email, password, password2, lastName, firstName, gender, motherName, birthPlace, birthDate,
				taxNumber, phone, country, zipCode, city, address, gdpr);

		isValidEmail(email);

		isRegisteredEmail(email);

		isStrongPassword(password);

		isPasswordMatches(password, password2);

		isGenderTypeCorrect(gender);

		isValidPhoneNumber(phone);

		LocalDate birthDate2 = LocalDate.of(Integer.parseInt(birthDate.substring(0, 4)),
				Integer.parseInt(birthDate.substring(5, 7)), Integer.parseInt(birthDate.substring(8)));

		Period period = Period.between(birthDate2, LocalDate.now());

		isEnoughOldPerson(period);

		isFilledGdpr(gdpr);

		List<Days> freeTime = createFreeTimeList();

		String encryptedPassword = BCrypt.hashpw(password, BCrypt.gensalt());

		Student student = new Student(email, encryptedPassword, zipCode, country, city, address, phone, firstName,
				lastName, taxNumber, Gender.valueOf(gender), motherName, birthDate2, birthPlace, true, freeTime);

		studentRegisterBean.saveStudentInDatabase(student);

	}

	public User uploadProfilePicture(User user) {

		return studentRegisterBean.uploadProfilePicture(user);

	}

	public Student uploadCv(Student student) {

		return studentRegisterBean.uploadCv(student);

	}

	public Student updateStudent(Student student, String password, String password2, String lastName, String firstName,
			String gender, String taxNumber, String phone, String country, String zipCode, String city, String address,
			String hourlyWage, String[] cityList, List<Days> freeTime) throws RegisterException {

		isUpdatedDataNotFilled(lastName, firstName, gender, taxNumber, phone, country, zipCode, city, address);

		isGenderTypeCorrect(gender);

		saveUpdatedPassword(student, password, password2);

		isValidPhoneNumber(phone);

		saveHourlyWage(student, hourlyWage);

		savePrefferedCitites(student, cityList);

		student.setLastName(lastName);
		student.setFirstName(firstName);
		student.setGender(Gender.valueOf(gender));
		student.setTaxId(taxNumber);
		student.setPhoneNumber(phone);
		student.setCountry(country);
		student.setZipCode(zipCode);
		student.setCity(city);
		student.setAddress(address);
		student.setFreeTime(freeTime);

		student = studentRegisterBean.updateStudentInDatabase(student);

		return student;

	}

	public Student updateStudentSkill(Student student, String skillKey, String skillValue) throws RegisterException {

		try {
			
			Integer skillValueInt = Integer.valueOf(Integer.parseInt(skillValue));

			Map<Skill, Integer> studentSkill = student.getSkills();

			Skill selectedSkill = getTheSelectedSkillFromStudent(skillKey, studentSkill);

			updateTheSelecetedSkill(skillKey, skillValueInt, studentSkill, selectedSkill);

			student.setSkills(studentSkill);

			student = studentRegisterBean.updateStudentInDatabase(student);

			return student;
			
		} catch (NumberFormatException ex) {
			
			throw new RegisterException("Nem számot adott meg fizetésnek!");
		
		}

	}

	private void updateTheSelecetedSkill(String skillKey, Integer skillValueInt, Map<Skill, Integer> studentSkill,
			Skill selectedSkill) {
		
		if (null == selectedSkill) {

			List<Skill> skills = loginEBean.getAllSkill();
			for (Skill skill : skills) {
				if (skill.getSkillName().equals(skillKey)) {
					selectedSkill = skill;
				}
			}
			studentSkill.put(selectedSkill, skillValueInt);

		} else {
			studentSkill.remove(selectedSkill);
			studentSkill.put(selectedSkill, skillValueInt);
		}
		
	}

	private Skill getTheSelectedSkillFromStudent(String skillKey, Map<Skill, Integer> studentSkill) {
		Skill selectedSkill = null;
		
		for (Map.Entry<Skill, Integer> skillKeyInMap : studentSkill.entrySet()) {
			if (skillKeyInMap.getKey().getSkillName().equals(skillKey)) {
				selectedSkill = skillKeyInMap.getKey();
			}
		}
		return selectedSkill;
		
	}

	private List<Days> createFreeTimeList() {

		List<Days> freeTime = new ArrayList<>();

		freeTime.add(new Days("Hétfő"));
		freeTime.add(new Days("Kedd"));
		freeTime.add(new Days("Szerda"));
		freeTime.add(new Days("Csütörtök"));
		freeTime.add(new Days("Péntek"));
		freeTime.add(new Days("Szombat"));
		freeTime.add(new Days("Vasárnap"));

		return freeTime;
	}

	private boolean isDataNotFilled(String email, String password, String password2, String lastName, String firstName,
			String gender, String motherName, String birthPlace, String birthDate, String taxNumber, String phone,
			String country, String zipCode, String city, String address, String gdpr) {

		return "".equals(email) || "".equals(password) || "".equals(password2) || "".equals(lastName)
				|| "".equals(firstName) || "".equals(gender) || "".equals(motherName) || "".equals(birthPlace)
				|| "".equals(birthDate) || "".equals(taxNumber) || "".equals(phone) || "".equals(country)
				|| "".equals(zipCode) || "".equals(city) || "".equals(address) || "".equals(gdpr);

	}

	private void isFilledGdpr(String gdpr) throws RegisterException {

		if (!"true".equals(gdpr)) {

			throw new RegisterException("Nem fogadta el a GDPR nyilatkozatot!");

		}

	}

	private void isEnoughOldPerson(Period period) throws RegisterException {

		if (period.getYears() < 14) {

			throw new RegisterException("Ön túl fiatal a regisztráláshoz!");

		}

	}

	private void isGenderTypeCorrect(String gender) throws RegisterException {

		if (!gender.equals(Gender.FÉRFI.toString()) && !gender.equals(Gender.NŐ.toString())) {

			throw new RegisterException("Nem jól adta meg a nemét!");

		}

	}

	private void isPasswordMatches(String password, String password2) throws RegisterException {

		if (!password.equals(password2)) {

			throw new RegisterException("Nem egyezik a két jelszó!");

		}

	}

	private void isStrongPassword(String password) throws RegisterException {

		if (!SharedFunctions.passwordOk(password)) {

			throw new RegisterException("Nem elég hosszú a jelszó!");

		}

	}

	private void isRegisteredEmail(String email) throws RegisterException {

		if (studentRegisterBean.isRegisteredEmailAddress(email)) {

			throw new RegisterException("Ez az email cím már foglalt!");

		}

	}

	private void isAllDataNotFilled(String email, String password, String password2, String lastName, String firstName,
			String gender, String motherName, String birthPlace, String birthDate, String taxNumber, String phone,
			String country, String zipCode, String city, String address, String gdpr) throws RegisterException {

		if (isDataNotFilled(email, password, password2, lastName, firstName, gender, motherName, birthPlace, birthDate,
				taxNumber, phone, country, zipCode, city, address, gdpr)) {

			throw new RegisterException("Nem töltötte ki az összes adatot!");

		}

	}

	private void isValidEmail(String email) throws RegisterException {

		if (!SharedFunctions.isValidEmailAddress(email)) {

			throw new RegisterException("Érvénytelen email cím!");

		}

	}

	private void savePrefferedCitites(Student student, String[] cityList) {

		if (null != cityList) {

			List<String> prefferedWorkPlace = new ArrayList<>();
			for (String cities : cityList) {
				prefferedWorkPlace.add(cities);
			}
			student.setPreferedWorkPlace(prefferedWorkPlace);
		}

	}

	private void saveHourlyWage(Student student, String hourlyWage) throws RegisterException {

		if (!"".equals(hourlyWage)) {
			try {
				Double hourlyWage2 = Double.parseDouble(hourlyWage);
				BigDecimal prefferedHourlyWage = BigDecimal.valueOf(hourlyWage2);
				student.setPreferedHourlyWage(prefferedHourlyWage);
			} catch (NumberFormatException ex) {
				throw new RegisterException("Nem számot adott meg fizetésnek!");
			}
		}

	}

	private void saveUpdatedPassword(Student student, String password, String password2) throws RegisterException {

		if (!"".equals(password) || !"".equals(password2)) {

			isStrongPassword(password);

			isPasswordMatches(password, password2);

			String encryptedPassword = BCrypt.hashpw(password, BCrypt.gensalt());
			student.setPassword(encryptedPassword);

		}

	}

	private void isUpdatedDataNotFilled(String lastName, String firstName, String gender, String taxNumber,
			String phone, String country, String zipCode, String city, String address) throws RegisterException {

		if ("".equals(lastName) || "".equals(firstName) || "".equals(gender) || "".equals(taxNumber) || "".equals(phone)
				|| "".equals(country) || "".equals(zipCode) || "".equals(city) || "".equals(address)) {
			throw new RegisterException("Nem töltötte ki az összes adatot!");
		}

	}

	private void isValidPhoneNumber(String phone) throws RegisterException {

		if (!phone.matches("[0-9]+")) {
			throw new RegisterException("Nem számot adott meg telefonszámnak!");
		}

	}

}
