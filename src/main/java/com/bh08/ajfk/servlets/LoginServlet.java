package com.bh08.ajfk.servlets;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bh08.ajfk.beans.LoginBean;
import com.bh08.ajfk.exceptions.AuthenticationException;
import com.bh08.ajfk.exceptions.InvalidDataOperationException;
import com.bh08.ajfk.models.Admin;
import com.bh08.ajfk.models.Company;
import com.bh08.ajfk.models.Gender;
import com.bh08.ajfk.models.Skill;
import com.bh08.ajfk.models.Student;
import com.bh08.ajfk.models.User;
import com.bh08.ajfk.models.quiz.Quiz;
import com.bh08.ajfk.services.ejbs.LoginServiceBean;
import com.bh08.ajfk.services.ejbs.admin.QuizServiceBean;
import com.bh08.ajfk.utils.SharedFunctions;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@EJB
	private LoginServiceBean loginServiceBean;

	@EJB
	private QuizServiceBean quizServiceBean;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		LoginBean loginBean = (LoginBean) session.getAttribute("loginBean");

		if (null == loginBean) {
			loginBean = new LoginBean();
			session.setAttribute("loginBean", loginBean);
			session.setAttribute("successLogin", false);
		}

		super.service(request, response);

	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		LoginBean loginBean = (LoginBean) session.getAttribute("loginBean");

		if (!loginBean.isInvalidLogin() && "true".equals(request.getParameter("reset"))) {
			loginBean.reset();
			session.removeAttribute("user");
			session.removeAttribute("picture");
		}

		request.getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
		loginBean.clearFlash();
		session.setAttribute("successLogin", false);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		LoginBean loginBean = (LoginBean) session.getAttribute("loginBean");

		if ("login".equalsIgnoreCase(request.getParameter("login-form"))) {

			String email = request.getParameter("email");
			String password = request.getParameter("password");

			try {

				User user = loginServiceBean.loginUser(email, password);
				String userType = user.getDecriminatorValue();

				if ("S".equals(userType)) {

					Student student = (Student) user;
					session.setAttribute("user", student);

					Map<Skill, Integer> studentSkills = student.getSkills();
					try {

						Skill english = quizServiceBean.getSkillByName("Angol");

						if (studentSkills.containsKey(english)) {

							loginBean.setValidLogin(true);
							response.sendRedirect(request.getContextPath() + "/NewsServlet");

						} else {

							List<Quiz> quizList = quizServiceBean.getRandomQuizBySkillAndNumberOfQuestions(english, 10);
							System.out.println(quizList);
							session.setAttribute("englishQuiz", quizList);
							response.sendRedirect(request.getContextPath() + "/QuizServlet");

						}

					} catch (InvalidDataOperationException ex) {
						ex.printStackTrace();
					}

				} else if ("C".equals(userType)) {

					Company company = (Company) user;
					session.setAttribute("user", company);
					loginBean.setValidLogin(true);
					response.sendRedirect(request.getContextPath() + "/CompanyMainServlet");

				} else if ("A".equals(userType)) {

					Admin admin = (Admin) user;
					session.setAttribute("user", admin);
					loginBean.setValidLogin(true);
					SharedFunctions.loadProfilePicture(request, response, session, admin, "/WEB-INF/admin/mydata.jsp");

				}

			} catch (AuthenticationException e) {

				loginBean.setInvalidLogin(true);
				response.sendRedirect(response.encodeRedirectURL(request.getRequestURI()));

			}

		} else if ("register_student".equalsIgnoreCase(request.getParameter("login-form"))) {

			List<Gender> genderList = Arrays.asList(Gender.values());
			session.setAttribute("countryList", loginServiceBean.getAllCountry());
			session.setAttribute("genderList", genderList);
			response.sendRedirect(request.getContextPath() + "/RegisterServlet");

		} else if ("register_company".equalsIgnoreCase(request.getParameter("login-form"))) {

			request.getRequestDispatcher("WEB-INF/company/regform_c.html").forward(request, response);

		}

	}

}
