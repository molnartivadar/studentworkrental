package com.bh08.ajfk.servlets;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.bh08.ajfk.exceptions.RegisterException;
import com.bh08.ajfk.models.Student;
import com.bh08.ajfk.models.User;
import com.bh08.ajfk.services.ejbs.admin.QuizServiceBean;
import com.bh08.ajfk.services.ejbs.student.StudentRegisterServiceBean;

@WebServlet("/UploadFileServlet")
public class UploadFileServlet extends HttpServlet {

	private static final String ERROR_INVALID_FILEFORMAT = "Hibás fájlformátum!";

	@EJB
	private StudentRegisterServiceBean studentRegisterServiceBean;
	
	@EJB
	private QuizServiceBean quizServiceBean;

	private static final long serialVersionUID = 1L;
	private ServletFileUpload uploader = null;

	@Override
	public void init() throws ServletException {
		
		DiskFileItemFactory fileFactory = new DiskFileItemFactory();
		File filesDir = (File) getServletContext().getAttribute("FILES_DIR_FILE");
		fileFactory.setRepository(filesDir);
		this.uploader = new ServletFileUpload(fileFactory);
		
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		if ("skills".equalsIgnoreCase(request.getParameter("skill-button"))) {

			try {
				
				String skillKey = request.getParameter("skill_list");
				String skillValue = request.getParameter("skill_point");
				Student student = (Student) session.getAttribute("user");
				student = studentRegisterServiceBean.updateStudentSkill(student, skillKey, skillValue);
				session.setAttribute("user", student);
				response.sendRedirect(response.encodeRedirectURL(request.getHeader("referer")));
				
			} catch (RegisterException ex) {
				
				session.setAttribute("successSkillUpload", false);
				response.sendRedirect(response.encodeRedirectURL(request.getHeader("referer")));
				
			}
			
		} else {

			uploadFile(request, response, session);

		}
	}

	private void uploadFile(HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws ServletException {

		if (!ServletFileUpload.isMultipartContent(request)) {
			throw new ServletException("Content type is not multipart/form-data");
		}

		try {
			
			List<FileItem> fileItemsList = uploader.parseRequest(request);
			Iterator<FileItem> fileItemsIterator = fileItemsList.iterator();
			
			while (fileItemsIterator.hasNext()) {
				
				FileItem fileItem = fileItemsIterator.next();

				File file = new File(request.getServletContext().getAttribute("FILES_DIR") + File.separator
						+ getDateTime() + fileItem.getName());

				fileItem.write(file);

				if (!"".equals(fileItem.getName())) {
					
					if (fileItem.getFieldName().equals("profilePic")) {

						if ("image/jpeg".equals(fileItem.getContentType())
								|| "image/png".equals(fileItem.getContentType())) {

							User user = (User) session.getAttribute("user");
							user.setProfilePicture(getDateTime() + fileItem.getName());
							user = studentRegisterServiceBean.uploadProfilePicture(user);
							session.setAttribute("user", user);
							session.setAttribute("successUpload", true);
							session.setAttribute("goodFormat", true);

						} else {
							session.setAttribute("goodFormat", false);
						}

					} else if (fileItem.getFieldName().equals("CV")) {

						if ("application/pdf".equals(fileItem.getContentType())) {

							Student student = (Student) session.getAttribute("user");
							student.setCv(getDateTime() + fileItem.getName());
							student = studentRegisterServiceBean.uploadCv(student);
							session.setAttribute("user", student);
							session.setAttribute("successUpload", true);
							session.setAttribute("goodFormat", true);

						} else {
							
							session.setAttribute("goodFormat", false);
							
						}
					} else if (fileItem.getFieldName().equals("CSV")) {
						
						if ("application/vnd.ms-excel".equals(fileItem.getContentType())) {
							
							quizServiceBean.saveNewQuizFromCsv(file);
							request.setAttribute("message", "File " + fileItem.getName() + " has uploaded successfully!");
							
						} else {
							
							request.setAttribute("errorMessage", ERROR_INVALID_FILEFORMAT);
							
						}
						
					}

				} else {
					session.setAttribute("successUpload", false);
				}

			}
		} catch (FileUploadException e) {
			
			// TODO kezdeni ezzel valamit
			
		} catch (Exception e) {
			
			// TODO kezdeni ezzel valamit
			
		} finally {
			
			try {
				
				response.sendRedirect(response.encodeRedirectURL(request.getHeader("referer")));
				
			} catch (IOException e) {
				
				// TODO kezdeni ezzel valamit
				
			}
		}
	}

	private final static String getDateTime() {
		
		DateFormat df = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
		df.setTimeZone(TimeZone.getTimeZone("CET"));
		return df.format(new Date());
		
	}

}