package com.bh08.ajfk.servlets.admin;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bh08.ajfk.models.Contract;
import com.bh08.ajfk.services.ejbs.admin.ContractsServiceBean;

@WebServlet("/ContractsAdminServlet")
public class ContractsAdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ContractsServiceBean contractsServiceBean;

    public ContractsAdminServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String studentName = request.getParameter("studentName");
		String companyName = request.getParameter("companyName");
		String jobName = request.getParameter("jobName");
		String contractState = request.getParameter("contractState");
		String cdf = request.getParameter("contractDateFrom");
		String cdt = request.getParameter("contractDateTo");
		
		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate dateFrom = LocalDate.parse(cdf,format);
		LocalDate dateTo = LocalDate.parse(cdt,format);
		
		List<Contract> contracts = contractsServiceBean.getAllContracts(studentName, companyName, jobName, contractState, dateFrom, dateTo);
		
		request.setAttribute("contracts", contracts);
		
		request.setAttribute("studentName", studentName);
		request.setAttribute("companyName", companyName);
		request.setAttribute("jobName", jobName);
		request.setAttribute("contractState", contractState);
		request.setAttribute("contractDateFrom", dateFrom);
		request.setAttribute("contractDateTo", dateTo);
		
		request.getRequestDispatcher("WEB-INF/admin/contracts.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
