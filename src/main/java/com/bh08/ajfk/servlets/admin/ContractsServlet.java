package com.bh08.ajfk.servlets.admin;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bh08.ajfk.models.Contract;
import com.bh08.ajfk.services.ejbs.admin.ContractsServiceBean;

@WebServlet("/ContractsServlet")
public class ContractsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ContractsServiceBean contractsServiceBean;
       
    public ContractsServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Contract contract = contractsServiceBean.getContractById(request.getParameter("id"));
		
		request.setAttribute("selectedContract", contract);
		request.getRequestDispatcher("/WEB-INF/admin/contractProfile.jsp").forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
