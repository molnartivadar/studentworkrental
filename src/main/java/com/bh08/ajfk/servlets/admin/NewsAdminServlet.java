package com.bh08.ajfk.servlets.admin;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bh08.ajfk.models.News;
import com.bh08.ajfk.services.ejbs.admin.NewsAdminService;

@WebServlet("/NewsAdminServlet")
public class NewsAdminServlet extends HttpServlet {
	
	private static final String MESSAGE_SUCCESSFULLY_UPLOAD = "Az új hír feltöltése sikeres!";
	private static final String ERROR_MISSING_DATA = "A cím és a leírás mező kitöltése kötelező!";

	private static final long serialVersionUID = 1L;
	
	@EJB
	private NewsAdminService newsAdminService;

    public NewsAdminServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<News> news = newsAdminService.getNews();
		request.setAttribute("news", news);		
				
		request.getRequestDispatcher("WEB-INF/admin/news.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String isPrimary = request.getParameter("isPrimary");
		String header = request.getParameter("newNewsHeader");
		String text = request.getParameter("newNewsText");

		if ("".equals(header) || "".equals(text)) {
			
			request.setAttribute("errorMessage", ERROR_MISSING_DATA);
			
		} else {
			
			newsAdminService.saveNews(isPrimary, header, text);
			request.setAttribute("message", MESSAGE_SUCCESSFULLY_UPLOAD);
			
		}
		
		request.getRequestDispatcher("WEB-INF/admin/news.jsp").forward(request, response);
	}

}
