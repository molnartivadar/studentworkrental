package com.bh08.ajfk.servlets.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;

import com.bh08.ajfk.exceptions.InvalidDataOperationException;
import com.bh08.ajfk.exceptions.NoResultException;
import com.bh08.ajfk.models.Skill;
import com.bh08.ajfk.models.quiz.Quiz;
import com.bh08.ajfk.services.ejbs.admin.QuizServiceBean;

/**
 * Servlet implementation class QuizAdminServlet
 */
@WebServlet("/QuizAdminServlet")
public class QuizAdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private QuizServiceBean quizServiceBean;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public QuizAdminServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		String actButton = request.getParameter("btnQuiz");
		String deleteQuestionId = request.getParameter("delete");

		if (null != deleteQuestionId) {
			Long questionID = Long.valueOf(deleteQuestionId);
			Skill skill = quizServiceBean.getSkillByQuestionId(questionID);
			
			quizServiceBean.deleteQuiz(questionID);
			
			List<Quiz> quiz = quizServiceBean.getQuizList(skill);
			request.setAttribute("quiz", quiz);			
			
		} else if ("List".equalsIgnoreCase(actButton)) {
			Skill skill = new Skill();
			skill.setSkillName(request.getParameter("skillCategory"));
			List<Quiz> quiz = quizServiceBean.getQuizList(skill);
			
			request.setAttribute("quiz", quiz);

		} else if ("Save".equalsIgnoreCase(actButton)) {
			String skillName = request.getParameter("quizSkill");
			String question = request.getParameter("newQuestion");
			String answer1 = request.getParameter("answer1");
			String answer2 = request.getParameter("answer2");
			String answer3 = request.getParameter("answer3");
			String answer4 = request.getParameter("answer4");
			String correctAnswer1 = request.getParameter("correctAnswer1");
			String correctAnswer2 = request.getParameter("correctAnswer2");
			String correctAnswer3 = request.getParameter("correctAnswer3");
			String correctAnswer4 = request.getParameter("correctAnswer4");

			List<String> answers = new ArrayList<String>();
			answers.add(answer1);
			answers.add(answer2);
			answers.add(answer3);
			answers.add(answer4);

			List<String> correctAnswers = new ArrayList<String>();
			correctAnswers.add(correctAnswer1);
			correctAnswers.add(correctAnswer2);
			correctAnswers.add(correctAnswer3);
			correctAnswers.add(correctAnswer4);

			try {
				quizServiceBean.saveNewQuiz(skillName, question, answers, correctAnswers);
				request.setAttribute("message", "A kvíz feltöltése sikeres!");
			} catch (NoResultException e) {
				request.setAttribute("errorMessage", e.getMessage());
			}

		} else if ("Felvétel".equalsIgnoreCase(actButton)) {
			
			String skillName = request.getParameter("newSkill");
			System.out.println(skillName);
			
			try {
				Skill skill = quizServiceBean.saveNewSkill(skillName);
				request.setAttribute("skillUploadSuccess", "Új skill-típus felvéve " + skill.getSkillName() + " néven!");
				List<Skill> skills = quizServiceBean.getSkillList();
				session.setAttribute("skills", skills);
			} catch (InvalidDataOperationException e) {
				request.setAttribute("skillUploadFail", e.getMessage());
			}
			
		} else if ("Update".equalsIgnoreCase(actButton)) {
			Long questionID = Long.valueOf(request.getParameter("editedQuizId"));
			Skill skill = quizServiceBean.getSkillByQuestionId(questionID);
			
			String question = request.getParameter("editedQuizQuestion");
			String answer1 = request.getParameter("editedQuizAnswer1");
			String answer2 = request.getParameter("editedQuizAnswer2");
			String answer3 = request.getParameter("editedQuizAnswer3");
			String answer4 = request.getParameter("editedQuizAnswer4");
			String correctAnswer1 = request.getParameter("editedCorrectAnswer1");
			String correctAnswer2 = request.getParameter("editedCorrectAnswer2");
			String correctAnswer3 = request.getParameter("editedCorrectAnswer3");
			String correctAnswer4 = request.getParameter("editedCorrectAnswer4");
			
			List<String> answers = new ArrayList<String>();
			answers.add(answer1);
			answers.add(answer2);
			answers.add(answer3);
			answers.add(answer4);

			List<String> correctAnswers = new ArrayList<String>();
			correctAnswers.add(correctAnswer1);
			correctAnswers.add(correctAnswer2);
			correctAnswers.add(correctAnswer3);
			correctAnswers.add(correctAnswer4);
			
			quizServiceBean.updateQuiz(questionID, skill, question, answers, correctAnswers);
			
			List<Quiz> quiz = quizServiceBean.getQuizList(skill);
			request.setAttribute("quiz", quiz);
		}

		request.getRequestDispatcher("WEB-INF/admin/quiz.jsp").forward(request, response);

	}

}
