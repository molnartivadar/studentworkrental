package com.bh08.ajfk.servlets.admin;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;

import com.bh08.ajfk.services.ejbs.admin.StatisticsServiceBean;

@WebServlet("/StatisticsServlet")
public class StatisticsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB
	private StatisticsServiceBean statisticsServiceBean;

    public StatisticsServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if (request.getParameter("contractDateSubmit") != null) {
		
			String dfs = request.getParameter("contractDateFrom");
			String dts = request.getParameter("contractDateTo");
			
			DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			LocalDate dateFrom = LocalDate.parse(dfs,format);
			LocalDate dateTo = LocalDate.parse(dts,format);
			
			JSONArray contracts = statisticsServiceBean.getNumberOfSignedContractsBetweenTwoDates(dateFrom, dateTo);
			
			request.setAttribute("contracts", contracts);
			request.setAttribute("contractDateFrom", dateFrom);
			request.setAttribute("contractDateTo", dateTo);
			request.setAttribute("incomeDateFrom", LocalDate.now().minusDays(30));
			request.setAttribute("incomeDateTo", LocalDate.now());
		
		} else if (request.getParameter("incomeSubmit") != null) {
			
			String dfs = request.getParameter("incomeDateFrom");
			String dts = request.getParameter("incomeDateTo");
			
			DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			LocalDate dateFrom = LocalDate.parse(dfs,format);
			LocalDate dateTo = LocalDate.parse(dts,format);
			
			JSONArray incomes = statisticsServiceBean.getIncomesFromSignedContractsBetweenTwoDates(dateFrom, dateTo);
			
			request.setAttribute("incomes", incomes);
			request.setAttribute("incomeDateFrom", dateFrom);
			request.setAttribute("incomeDateTo", dateTo);
			request.setAttribute("contractDateFrom", LocalDate.now().minusDays(30));
			request.setAttribute("contractDateTo", LocalDate.now());
			
		} else if (request.getParameter("registeredUsers") != null) {
			
			Map<String, Integer> registeredUsers = statisticsServiceBean.getRegisteredUsersGroupByType();
			
			request.setAttribute("registeredUsers", registeredUsers);
			request.setAttribute("incomeDateFrom", LocalDate.now().minusDays(30));
			request.setAttribute("incomeDateTo", LocalDate.now());
			request.setAttribute("contractDateFrom", LocalDate.now().minusDays(30));
			request.setAttribute("contractDateTo", LocalDate.now());
		}
		
		request.getRequestDispatcher("WEB-INF/admin/statistics.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
