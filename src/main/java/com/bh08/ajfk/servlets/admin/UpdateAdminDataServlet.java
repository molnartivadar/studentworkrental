package com.bh08.ajfk.servlets.admin;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bh08.ajfk.models.Admin;
import com.bh08.ajfk.services.ejbs.admin.AdminUpdateServiceBean;
import com.bh08.ajfk.utils.SharedFunctions;

@WebServlet("/UpdateAdminDataServlet")
public class UpdateAdminDataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private AdminUpdateServiceBean adminUpdateServiceBean;

	public UpdateAdminDataServlet() {
		super();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		Admin admin = (Admin) session.getAttribute("user");
		
		String eMail = admin.getEMail();
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");
		String lastName = request.getParameter("lastname");
		String forName = request.getParameter("forname");
		String country = request.getParameter("country");
		String zipCode = request.getParameter("zipcode");
		String city = request.getParameter("city");
		String address = request.getParameter("address");
		String phoneNumber = request.getParameter("phonenumber");
		String profilePicture = (String) request.getParameter("profilepicture");
		
		adminUpdateServiceBean.updateAdmin(admin, eMail, password1, password2, lastName, forName, country, zipCode, city, address, phoneNumber, profilePicture);
		
		admin = adminUpdateServiceBean.getUpdatedAdmin(eMail);
		
		session.removeAttribute("user");
		session.setAttribute("user", admin);

		SharedFunctions.loadProfilePicture(request, response, session, admin, "/WEB-INF/admin/mydata.jsp");
	}

}
