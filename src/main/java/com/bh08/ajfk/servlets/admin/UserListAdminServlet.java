package com.bh08.ajfk.servlets.admin;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bh08.ajfk.models.User;
import com.bh08.ajfk.services.ejbs.admin.UserListServiceBean;

@WebServlet("/UserListAdminServlet")
public class UserListAdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB
	private UserListServiceBean userListServiceBean;
       
    public UserListAdminServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String userType = request.getParameter("userCategory");
		List<User> users = userListServiceBean.getUserList(userType);
		if (null == users) {
			request.setAttribute("errorMessage", "Hiba a lekérdezés során!");
		} else {
			request.setAttribute("userType", userType);
			request.setAttribute("users", users);
			request.setAttribute("message", "A keresési feltételeknek (" + userType + ") " + users.size() + " felhasználó felel meg.");
		}
		
		request.getRequestDispatcher("WEB-INF/admin/users.jsp").forward(request, response);
		
	}

}
