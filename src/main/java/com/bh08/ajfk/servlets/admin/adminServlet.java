package com.bh08.ajfk.servlets.admin;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bh08.ajfk.models.Admin;
import com.bh08.ajfk.models.Skill;
import com.bh08.ajfk.services.ejbs.LoginServiceBean;
import com.bh08.ajfk.services.ejbs.admin.QuizServiceBean;
import com.bh08.ajfk.utils.SharedFunctions;

public class adminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB
	private QuizServiceBean quizServiceBean;
	
	@EJB
	private LoginServiceBean loginServiceBean;

	public adminServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		Admin admin = (Admin) session.getAttribute("user");
		
		LocalDate dateTo = LocalDate.now();
		LocalDate dateFrom = dateTo.minusDays(30);
		
		session.setAttribute("countryList", loginServiceBean.getAllCountry());

		if (null == admin) {
			response.sendRedirect(request.getContextPath() + "/LoginServlet");
		} else {

			String nextPage = request.getParameter("name");

			switch (nextPage) {

			case "mydata":
				SharedFunctions.loadProfilePicture(request, response, session, admin, "/WEB-INF/admin/mydata.jsp");
				break;
			case "newadmin":
				request.getRequestDispatcher("WEB-INF/admin/newadmin.jsp").forward(request, response);
				break;
			case "users":
				request.getRequestDispatcher("WEB-INF/admin/users.jsp").forward(request, response);
				break;
			case "quiz":
				List<Skill> skills = quizServiceBean.getSkillList();
				session.setAttribute("skills", skills);
				request.getRequestDispatcher("WEB-INF/admin/quiz.jsp").forward(request, response);
				break;
			case "contracts":
				request.setAttribute("contractDateFrom", dateFrom);
				request.setAttribute("contractDateTo", dateTo);
				request.getRequestDispatcher("WEB-INF/admin/contracts.jsp").forward(request, response);
				break;
			case "statistics":
				request.setAttribute("contractDateFrom", dateFrom);
				request.setAttribute("contractDateTo", dateTo);
				request.setAttribute("incomeDateFrom", dateFrom);
				request.setAttribute("incomeDateTo", dateTo);
				request.getRequestDispatcher("WEB-INF/admin/statistics.jsp").forward(request, response);
				break;
			case "news":
				request.getRequestDispatcher("WEB-INF/admin/news.jsp").forward(request, response);
				break;
			default:
				break;
			}
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
