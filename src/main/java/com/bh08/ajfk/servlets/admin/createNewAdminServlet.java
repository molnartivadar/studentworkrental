package com.bh08.ajfk.servlets.admin;

import java.io.IOException;
import java.math.BigDecimal;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bh08.ajfk.exceptions.RegisterException;
import com.bh08.ajfk.models.Admin;
import com.bh08.ajfk.services.ejbs.admin.AdminRegisterServiceBean;

@WebServlet("/createNewAdminServlet")
public class createNewAdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB
	private AdminRegisterServiceBean adminRegisterServiceBean;
       
    public createNewAdminServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		String address = request.getParameter("address");
		String city = request.getParameter("city");
		String country = request.getParameter("country");
		String eMail = request.getParameter("email");
		String forName = request.getParameter("forname");
		String lastName = request.getParameter("lastname");
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");
		String phoneNumber = request.getParameter("phonenumber");
		String zipCode = request.getParameter("zipcode");
		
		try {
			
			Admin newAdmin = adminRegisterServiceBean.saveNewAdmin(address, city, country, eMail, forName, lastName, password1, password2, phoneNumber, zipCode, new BigDecimal(0));
			session.setAttribute("newAdmin", newAdmin);
			
		} catch (RegisterException e) {
			String error = e.getMessage();
			request.setAttribute("error", error);
		}
		request.getRequestDispatcher("WEB-INF/admin/newadmin.jsp").forward(request, response);
		
	}

}
