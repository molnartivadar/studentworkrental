package com.bh08.ajfk.servlets.company;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bh08.ajfk.daos.ejbs.company.CompanyRegisterBean;
import com.bh08.ajfk.models.Company;

/**
 * Servlet implementation class CompanyOffers
 */
@WebServlet("/CompanyOffers")
public class CompanyOffers extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private CompanyRegisterBean companyRegisterBean;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CompanyOffers() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		String eMail = (String) session.getAttribute("email");
		System.out.println(eMail);
		//Company company = companyRegisterBean.getCompanyByEmail(eMail);
		//session.setAttribute("user", company);
		request.getRequestDispatcher("WEB-INF/company/CompanyOffers.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
