package com.bh08.ajfk.servlets.company;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;

import com.bh08.ajfk.exceptions.RegisterException;
import com.bh08.ajfk.services.ejbs.company.CompanyRegisterServiceBean;
import com.bh08.ajfk.services.ejbs.student.StudentRegisterServiceBean;


@WebServlet("/CompanyRegisterServlet")
public class CompanyRegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@EJB
	private CompanyRegisterServiceBean companyRegisterServiceBean;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("WEB-INF/company/regform_c.html").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String zipCode = request.getParameter("zipcode");
		String country = request.getParameter("country");
		String city = request.getParameter("city");
		String adress = request.getParameter("adress");
		String phone = request.getParameter("phone");
		String companyName = request.getParameter("companyname");
		String taxNumber = request.getParameter("taxid");
		String description = request.getParameter("description");
		
		Boolean modifyCompany = "true".equals(request.getParameter("modifycompany"));
		
			System.out.println(email+password+password2+zipCode+country+city+adress+phone+companyName+taxNumber+modifyCompany);
			
		try {
			System.out.println("ment");
			companyRegisterServiceBean.saveCompany(email, password, password2, zipCode, country, city, adress, phone, companyName, taxNumber, description, modifyCompany);
			System.out.println("mentve");
			
			if (modifyCompany) {
				session.setAttribute("modifiedcompany", true);
				session.removeAttribute("successLogin");
				request.getRequestDispatcher("WEB-INF/company/modifySuccess.jsp").forward(request, response);
				//response.sendRedirect("");
			}else {
			session.setAttribute("successLogin", true);
			//response.sendRedirect("WEB-INF/company/modifySuccess.jsp");
			response.sendRedirect("/StudentWorkRental/LoginServlet");
			}
			System.out.println("ez még van");
			
				
		} catch (RegisterException ex) {
			System.out.println(ex.getMessage());
			request.getRequestDispatcher("WEB-INF/company/regform_c.html").forward(request, response);	
		}

	}

}
