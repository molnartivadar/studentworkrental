package com.bh08.ajfk.servlets.company;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bh08.ajfk.daos.ejbs.company.CompanyRegisterBean;
import com.bh08.ajfk.models.Job;
import com.bh08.ajfk.models.Skill;
import com.bh08.ajfk.services.ejbs.admin.QuizServiceBean;
import com.bh08.ajfk.services.ejbs.company.CompanyRegisterServiceBean;

/**
 * Servlet implementation class JobMakerServlet
 */
@WebServlet("/JobMakerServlet")
public class JobMakerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private CompanyRegisterBean companyRegisterBean;
	
	@EJB
	private QuizServiceBean quizServiceBean;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public JobMakerServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("jobmaker elérve");
		// HttpSession session = request.getSession();
		Boolean newJob = "true".equals(request.getParameter("newjob"));
		request.setAttribute("newjob", newJob);
		System.out.println(newJob);
		
		HttpSession session = request.getSession(true);
		List<Skill> allSkills = quizServiceBean.getSkillList();
		session.setAttribute("skills", allSkills);
		
		if (newJob) {
			System.out.println("újmeló ág");
			Job selectedJob = new Job();
			selectedJob.setJobName("kérjük kitölteni");
			selectedJob.setJobDescription("kérjük kitölteni");
			session.setAttribute("newjob", "true");
			request.setAttribute("selectedjob", selectedJob);
			
		} else {
			System.out.println("módosított melo ág");
			String jobID = request.getParameter("jobid");
			int id = Integer.parseInt(jobID);
			session.setAttribute("jobid", jobID);
			Job selectedJob = companyRegisterBean.getSelectedJob(id);
			System.out.println(selectedJob.getJobName() + ", " + selectedJob.getWorkPlace());
			request.setAttribute("jobid", jobID);
			request.setAttribute("selectedjob", selectedJob);
		}

		request.getRequestDispatcher("WEB-INF/company/JobMaker.jsp").forward(request, response);
		// doGet(request, response);
	}

}
