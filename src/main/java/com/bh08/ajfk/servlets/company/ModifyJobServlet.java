package com.bh08.ajfk.servlets.company;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bh08.ajfk.exceptions.RegisterException;
import com.bh08.ajfk.models.Company;
import com.bh08.ajfk.models.Skill;
import com.bh08.ajfk.services.ejbs.admin.QuizServiceBean;
import com.bh08.ajfk.services.ejbs.company.CompanyRegisterServiceBean;

/**
 * Servlet implementation class ModifyJobServlet
 */
@WebServlet("/ModifyJobServlet")
public class ModifyJobServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private CompanyRegisterServiceBean companyRegisterServiceBean;

	@EJB
	private QuizServiceBean quizServiceBean;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ModifyJobServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		Company company = (Company) session.getAttribute("user");

		String jobName = request.getParameter("jobname");
		String hourlyWage = request.getParameter("hourlywage");
		String workPlace = request.getParameter("workplace");
		String hoursOfWorkInOneWeek = request.getParameter("hoursofworkinoneweek");
		String jobDescription = request.getParameter("jobdescription");

		List<Skill> skillsToSave = new ArrayList<>();
		List<Skill> allSkills = quizServiceBean.getSkillList();

		String[] selectedSkills = request.getParameterValues("skillid");
		List<Long> selectedSkillsID = new ArrayList<Long>();

		if (selectedSkills != null) {
			for (String s : selectedSkills) {
				// for (int i = 0; i < selectedSkills.length; i++) {
				selectedSkillsID.add(Long.parseLong(s));
			}

			for (Long l : selectedSkillsID) {
				for (Skill skill : allSkills) {
					if (skill.getId() == l) {
						System.out.println(skill.getSkillName()); // Törlendő
						skillsToSave.add(skill);
					}
				}
			}
		}
		
		Boolean newJob = "true".equals(request.getParameter("newjob"));

		if (newJob) {
			System.out.println("új munka");
			try {
				companyRegisterServiceBean.saveJob(jobName, hourlyWage, workPlace, hoursOfWorkInOneWeek, jobDescription,
						skillsToSave, company, newJob, 0);
			} catch (RegisterException e) {
				e.printStackTrace();
			}

		} else {
			int jobID = Integer.parseInt((String) session.getAttribute("jobid"));
			System.out.println("módosított munka");
			try {
				companyRegisterServiceBean.saveJob(jobName, hourlyWage, workPlace, hoursOfWorkInOneWeek, jobDescription,
						skillsToSave, company, newJob, jobID);
			} catch (RegisterException e) {
				e.printStackTrace();
			}
		}
		session.removeAttribute("jobid");
		request.getRequestDispatcher("WEB-INF/company/modifySuccess.jsp").forward(request, response);
	
	}
}
