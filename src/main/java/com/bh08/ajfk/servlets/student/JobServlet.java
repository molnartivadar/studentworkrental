package com.bh08.ajfk.servlets.student;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bh08.ajfk.exceptions.AuthenticationException;
import com.bh08.ajfk.models.Job;
import com.bh08.ajfk.services.ejbs.LoginServiceBean;

@WebServlet("/JobServlet")
public class JobServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private LoginServiceBean loginServiceBean;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {

			Job job = loginServiceBean.getJobById(request.getParameter("id"));
			request.setAttribute("selectedJob", job);
			request.getRequestDispatcher("/WEB-INF/student/jsp/Job.jsp").forward(request, response);

		} catch (NumberFormatException | AuthenticationException e) {

			request.setAttribute("existJob", false);
			request.getRequestDispatcher("/WEB-INF/profileNotExist.jsp").forward(request, response);

		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);

	}

}
