package com.bh08.ajfk.servlets.student;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bh08.ajfk.models.Contract;
import com.bh08.ajfk.models.Student;
import com.bh08.ajfk.services.ejbs.LoginServiceBean;

@WebServlet("/MyContractServlet")
public class MyContractServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private LoginServiceBean loginServiceBean;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		List<Contract> contracts = loginServiceBean.getContractsByStudent((Student) session.getAttribute("user"));
		request.setAttribute("contractList", contracts);

		request.getRequestDispatcher("/WEB-INF/student/jsp/myContract.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		if (null != request.getParameter("yes-button")) {

			loginServiceBean.setContractState(request.getParameter("yes-button"), true,
					(Student) session.getAttribute("user"));

		} else if (null != request.getParameter("no-button")) {

			loginServiceBean.setContractState(request.getParameter("no-button"), false,
					(Student) session.getAttribute("user"));

		}

		response.sendRedirect(response.encodeRedirectURL(request.getRequestURI()));

	}

}
