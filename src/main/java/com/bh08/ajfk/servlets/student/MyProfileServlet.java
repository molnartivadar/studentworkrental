package com.bh08.ajfk.servlets.student;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bh08.ajfk.exceptions.AuthenticationException;
import com.bh08.ajfk.models.Admin;
import com.bh08.ajfk.models.Company;
import com.bh08.ajfk.models.CompanyRating;
import com.bh08.ajfk.models.Student;
import com.bh08.ajfk.models.User;
import com.bh08.ajfk.services.ejbs.LoginServiceBean;
import com.bh08.ajfk.utils.SharedFunctions;

@WebServlet("/MyProfileServlet")
public class MyProfileServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private LoginServiceBean loginServiceBean;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		try {

			User user = loginServiceBean.getUserById(request.getParameter("id"));
			String userType = user.getDecriminatorValue();

			if ("S".equals(userType)) {

				Student student = (Student) user;
				request.setAttribute("selectedUser", student);
				SharedFunctions.loadProfilePicture(request, response, session, student,
						"/WEB-INF/student/jsp/studentProfile.jsp");

			} else if ("C".equals(userType)) {

				Company company = (Company) user;
				List<CompanyRating> rating = company.getRating();

				double averageRating = 0;
				int countRating = 0;
				int fiveRating = 0;
				int fourRating = 0;
				int threeRating = 0;
				int twoRating = 0;
				int oneRating = 0;

				for (int i = 0; i < rating.size(); i++) {

					averageRating += rating.get(i).getRating();
					countRating++;

					switch (rating.get(i).getRating()) {
					case 1:
						oneRating++;
						break;
					case 2:
						twoRating++;
						break;
					case 3:
						threeRating++;
						break;
					case 4:
						fourRating++;
						break;
					case 5:
						fiveRating++;
						break;
					}

				}

				averageRating = averageRating / countRating;

				request.setAttribute("oneRating", oneRating);
				request.setAttribute("twoRating", twoRating);
				request.setAttribute("threeRating", threeRating);
				request.setAttribute("fourRating", fourRating);
				request.setAttribute("fiveRating", fiveRating);

				request.setAttribute("averageRating", averageRating);
				request.setAttribute("selectedUser", company);

				SharedFunctions.loadProfilePicture(request, response, session, company,
						"/WEB-INF/company/companyProfile.jsp");

			} else if ("A".equals(userType)) {

				Admin admin = (Admin) user;
				request.setAttribute("selectedUser", admin);
				SharedFunctions.loadProfilePicture(request, response, session, admin,
						"/WEB-INF/admin/adminProfile.jsp");

			} else {

				request.setAttribute("existUser", false);
				request.getRequestDispatcher("/WEB-INF/profileNotExist.jsp").forward(request, response);

			}

		} catch (AuthenticationException e) {

			request.setAttribute("existUser", false);
			request.getRequestDispatcher("/WEB-INF/profileNotExist.jsp").forward(request, response);

		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);

	}

}
