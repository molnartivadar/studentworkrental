package com.bh08.ajfk.servlets.student;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bh08.ajfk.daos.ejbs.student.NewsBean;
import com.bh08.ajfk.models.Job;
import com.bh08.ajfk.models.News;
import com.bh08.ajfk.models.Skill;
import com.bh08.ajfk.models.Student;

@WebServlet("/NewsServlet")
public class NewsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private NewsBean newsBean;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		super.service(request, response);

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		Student student = (Student) session.getAttribute("user");

		if (null == student) {
			response.sendRedirect(request.getContextPath() + "/LoginServlet");
		} else {

			Set<Job> sevenJobs = new HashSet<>();
			List<Job> jobs = newsBean.getAllJobs();

			for (Job job : jobs) {

				for (Skill skill : job.getSkills()) {

					for (Skill skill2 : student.getSkills().keySet()) {

						if (skill2.equals(skill)) {
							sevenJobs.add(job);

						}
					}
				}
			}

			List<Job> sevenJobsList = new ArrayList<>(sevenJobs);
			Collections.shuffle(sevenJobsList);

			for (int i = 7; i < sevenJobsList.size(); i++) {

				sevenJobsList.remove(i);

			}

			List<News> newsList = newsBean.getNews();
			session.setAttribute("news", newsList);
			request.setAttribute("studentList", newsBean.getAllStudent());
			request.setAttribute("jobList", sevenJobsList);
			request.getRequestDispatcher("WEB-INF/student/jsp/student.jsp").forward(request, response);

		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
