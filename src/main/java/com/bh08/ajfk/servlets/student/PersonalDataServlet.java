package com.bh08.ajfk.servlets.student;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;

import com.bh08.ajfk.exceptions.RegisterException;
import com.bh08.ajfk.models.Days;
import com.bh08.ajfk.models.Gender;
import com.bh08.ajfk.models.Student;
import com.bh08.ajfk.models.User;
import com.bh08.ajfk.services.ejbs.LoginServiceBean;
import com.bh08.ajfk.services.ejbs.student.StudentRegisterServiceBean;
import com.bh08.ajfk.utils.SharedFunctions;

@WebServlet("/PersonalDataServlet")
public class PersonalDataServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@EJB
	private LoginServiceBean loginServiceBean;

	@EJB
	private StudentRegisterServiceBean studentRegisterServiceBean;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		Student user2 = (Student) session.getAttribute("user");

		List<Gender> genderList = Arrays.asList(Gender.values());
		session.setAttribute("countryList", loginServiceBean.getAllCountry());
		session.setAttribute("cityList", loginServiceBean.getAllCity());
		session.setAttribute("skillList", loginServiceBean.getAllSkillExceptEnglish());
		session.setAttribute("genderList", genderList);

		SharedFunctions.loadProfilePicture(request, response, session, user, "/WEB-INF/student/jsp/personalData.jsp");
		session.removeAttribute("successUpload");
		session.removeAttribute("goodFormat");
		session.removeAttribute("successSkillUpload");

		System.out.println(user2);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		Student student = (Student) session.getAttribute("user");

		String password = request.getParameter("user_password");
		String password2 = request.getParameter("confirm_password");
		String lastName = request.getParameter("last_name");
		String firstName = request.getParameter("first_name");
		String gender = request.getParameter("gender");
		String taxNumber = request.getParameter("tax_num");
		String phone = request.getParameter("contact_no");
		String country = request.getParameter("country");
		String zipCode = request.getParameter("zip_code");
		String city = request.getParameter("city");
		String address = request.getParameter("address");
		String hourlyWage = request.getParameter("hourly_wage");
		String[] cityList = request.getParameterValues("city_list");

		List<Days> freeTime = student.getFreeTime();
		String[] days = { "Hétfő", "Kedd", "Szerda", "Csütörtök", "Péntek", "Szombat", "Vasárnap" };

		for (int i = 0; i < 7; i++) {

			Map<Integer, Boolean> monday = new TreeMap<>();

			for (int j = 0; j < 24; j++) {

				if (null == (request.getParameter(days[i] + j))) {

					monday.put(j, false);

				} else {

					monday.put(j, true);

				}
			}

			for (int k = 0; k < 7; k++) {

				if (freeTime.get(k).getDaysName().equals(days[i])) {
					freeTime.get(k).setHours(monday);
				}

			}
		}

		String jsonString;
		try {

			student = studentRegisterServiceBean.updateStudent(student, password, password2, lastName, firstName,
					gender, taxNumber, phone, country, zipCode, city, address, hourlyWage, cityList, freeTime);
			session.setAttribute("user", student);
			jsonString = new JSONObject().put("redirectString", request.getContextPath() + "/PersonalDataServlet")
					.toString();

		} catch (RegisterException ex) {

			jsonString = new JSONObject().put("redirectString", ex.getMessage()).toString();

		}

		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonString);
		out.flush();

	}

}
