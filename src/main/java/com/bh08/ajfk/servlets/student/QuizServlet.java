package com.bh08.ajfk.servlets.student;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bh08.ajfk.exceptions.InvalidDataOperationException;
import com.bh08.ajfk.models.Student;
import com.bh08.ajfk.models.quiz.CorrectAnswer;
import com.bh08.ajfk.models.quiz.Quiz;
import com.bh08.ajfk.services.ejbs.admin.QuizServiceBean;

@WebServlet("/QuizServlet")
public class QuizServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@EJB
	private QuizServiceBean quizServiceBean;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.getRequestDispatcher("WEB-INF/student/jsp/registerQuiz.jsp").forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		List<Quiz> quizList = (List<Quiz>) session.getAttribute("englishQuiz");

		int quizPoints = 0;
		for (int i = 0; i < quizList.size(); i++) {

			String[] answerId = request.getParameterValues(quizList.get(i).getQuestion().getId().toString());
			List<CorrectAnswer> crAnswer = quizList.get(i).getCorrectAnswers();
			boolean isGood = true;

			if (null != answerId) {
				int correctAnswer = 0;
				for (int j = 0; j < answerId.length; j++) {

					for (int k = 0; k < crAnswer.size(); k++) {

						if (crAnswer.get(k).getAnswer().getId().toString().equals(answerId[j])) {
							correctAnswer++;
						}
					}

				}
				if (crAnswer.size() != correctAnswer) {
					isGood = false;
				}
			} else {
				isGood = false;
			}

			if (isGood) {
				quizPoints++;
			}

		}

		Student student = (Student) session.getAttribute("user");

		try {

			quizServiceBean.saveEnglishTest(student, quizPoints);
			session.removeAttribute("englishQuiz");
			response.sendRedirect("/StudentWorkRental/NewsServlet");

		} catch (InvalidDataOperationException ex) {

			// TODO kezdeni vele valamit

		}

	}

}
