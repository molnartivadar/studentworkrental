package com.bh08.ajfk.servlets.student;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bh08.ajfk.exceptions.AuthenticationException;
import com.bh08.ajfk.models.Company;
import com.bh08.ajfk.models.Student;
import com.bh08.ajfk.services.ejbs.LoginServiceBean;

@WebServlet("/RateCompanyServlet")
public class RateCompanyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private LoginServiceBean loginServiceBean;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		List<Company> companyList = loginServiceBean.getCompanyWithoutRating((Student) session.getAttribute("user"));
		request.setAttribute("companyList", companyList);
		request.getRequestDispatcher("/WEB-INF/student/jsp/rateCompany.jsp").forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		Student student = (Student) session.getAttribute("user");

		String company = request.getParameter("companies");
		String rating = request.getParameter("companyrate");
		String description = request.getParameter("ratingDescriprion");

		try {

			loginServiceBean.saveRatingTheDatabase(student, company, rating, description);

		} catch (AuthenticationException ex) {

			// TODO kezdeni vele valamit

		}

		doGet(request, response);

	}

}
