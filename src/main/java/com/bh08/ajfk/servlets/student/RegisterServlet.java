package com.bh08.ajfk.servlets.student;

import java.io.IOException;
import java.io.PrintWriter;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;

import com.bh08.ajfk.exceptions.RegisterException;
import com.bh08.ajfk.services.ejbs.student.StudentRegisterServiceBean;

@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private StudentRegisterServiceBean studentRegisterServiceBean;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.getRequestDispatcher("WEB-INF/student/jsp/studentRegistration.jsp").forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		String email = request.getParameter("email");
		String password = request.getParameter("user_password");
		String password2 = request.getParameter("confirm_password");
		String lastName = request.getParameter("last_name");
		String firstName = request.getParameter("first_name");
		String gender = request.getParameter("gender");
		String motherName = request.getParameter("mother_name");
		String birthPlace = request.getParameter("birth_place");
		String birthDate = request.getParameter("birth_date");
		String taxNumber = request.getParameter("tax_num");
		String phone = request.getParameter("contact_no");
		String zipCode = request.getParameter("zip_code");
		String country = request.getParameter("country");
		String city = request.getParameter("city");
		String address = request.getParameter("address");
		String gdpr = request.getParameter("gdpr");

		String jsonString;
		try {

			studentRegisterServiceBean.saveStudent(email, password, password2, lastName, firstName, gender, motherName,
					birthPlace, birthDate, taxNumber, phone, country, zipCode, city, address, gdpr);
			session.setAttribute("successLogin", true);
			jsonString = new JSONObject().put("redirectString", request.getContextPath() + "/LoginServlet").toString();

		} catch (RegisterException ex) {

			jsonString = new JSONObject().put("redirectString", ex.getMessage()).toString();

		}

		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonString);
		out.flush();

	}

}
