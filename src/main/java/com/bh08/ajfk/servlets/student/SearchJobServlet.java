package com.bh08.ajfk.servlets.student;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;

import com.bh08.ajfk.daos.ejbs.student.NewsBean;
import com.bh08.ajfk.models.Job;
import com.bh08.ajfk.services.ejbs.LoginServiceBean;

@WebServlet("/SearchJobServlet")
public class SearchJobServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@EJB
	private LoginServiceBean loginServiceBean;

	@EJB
	private NewsBean newsBean;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		request.setAttribute("cityList", loginServiceBean.getAllCity());
		request.setAttribute("skillList", loginServiceBean.getAllSkill());
		request.getRequestDispatcher("/WEB-INF/student/jsp/searchJob.jsp").forward(request, response);
		session.removeAttribute("jobList");

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		String jobName = request.getParameter("jobname");
		String hourlyWage = request.getParameter("hourlywage");
		String workTime = request.getParameter("worktime");
		String[] skillList = request.getParameterValues("skills");
		String[] workplaces = request.getParameterValues("workplaces");

		String jsonString;

		List<Job> searchedJob = loginServiceBean.searchJobs(jobName, hourlyWage, workTime, skillList, workplaces);
		session.setAttribute("jobList", searchedJob);
		jsonString = new JSONObject().put("redirectString", request.getContextPath() + "/SearchJobServlet").toString();

		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonString);
		out.flush();

		doGet(request, response);
	}

}
