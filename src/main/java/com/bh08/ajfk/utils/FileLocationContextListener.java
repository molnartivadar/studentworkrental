package com.bh08.ajfk.utils;

import java.io.File;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class FileLocationContextListener implements ServletContextListener {

	public void contextInitialized(ServletContextEvent servletContextEvent) {

		ServletContext ctx = servletContextEvent.getServletContext();
		String relativePath = new File("/tmp/upload").getAbsolutePath();
		File file = new File(relativePath);
		
		if (!file.exists()) {
			
			file.mkdirs();
			
		}
			
		System.out.println("File Directory created to be used for storing files");
		ctx.setAttribute("FILES_DIR_FILE", file);
		ctx.setAttribute("FILES_DIR", relativePath);

	}

	public void contextDestroyed(ServletContextEvent servletContextEvent) {

	}

}
