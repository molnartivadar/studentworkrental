package com.bh08.ajfk.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;

import com.bh08.ajfk.models.User;

public class SharedFunctions {
	
	// TODO: read from config file (eg. java.util.Properties)
	private static final String PATH_PROFILE_IMAGES = "C:\\tmp\\upload\\";

	public static boolean isValidEmailAddress(String email) {
		
		String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
		java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
		java.util.regex.Matcher m = p.matcher(email);
		return m.matches();
		
	}

	public static boolean passwordOk(String password) {
		
		if (password == null) {
			
			return false;
			
		}
		
		if (password.length() < 6) {
			
			return false;
			
		}

		return true;
	}
	
	public static void loadProfilePicture(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			User user, String redirectUrl) throws ServletException, IOException {
		
		File file = new File(PATH_PROFILE_IMAGES + user.getProfilePicture());
		 
        try {            
        	
            FileInputStream imageInFile = new FileInputStream(file);
            byte imageData[] = new byte[(int) file.length()];
            imageInFile.read(imageData);
 
            String imageDataString = Base64.encodeBase64String(imageData);
            request.setAttribute("picture", imageDataString);
 
            imageInFile.close();
 
            System.out.println("Image Successfully Manipulated!");
            
        } catch (FileNotFoundException e) {
        	
            System.out.println("Image not found" + e);
            
        } catch (IOException ioe) {
        	
            System.out.println("Exception while reading the Image " + ioe);
            
        }finally { 		
        	
        	request.getRequestDispatcher(redirectUrl).forward(request, response);
        	
        }
        
	}
	
	public static <T> List<T> castList(Class<? extends T> clazz, Collection<?> c) {

		List<T> r = new ArrayList<T>(c.size());
		for (Object o : c)
			r.add(clazz.cast(o));
		return r;

	}
		
}
