<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>
	
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="/StudentWorkRental/css/admin/admin.css" />
		<title>Admin profil</title>
	</head>


	<body>
		<br /><br /><br />
		<div align="center">
			<table>
			<tr>
				<td class="adminProfil">
					<img src="data:image/jpeg;base64,${picture}" width='200px' height='200px' class='profilpicture' >
				</td>
				<td>
					<ul><h2 class="adminName">${selectedUser.lastName} ${selectedUser.forName}</h2></ul>
					<ul>Város: <b>${selectedUser.city}, ${selectedUser.country}</b></ul>
					<ul>Cím: <b>${selectedUser.address}</b></ul>
					<ul>e-mail: <b>${selectedUser.EMail}</b></ul>
					<ul>Telefonszám: <b>${selectedUser.phoneNumber}</b></ul>
					<ul><i>Regisztráció dátuma: ${selectedUser.registrationDate}</i></ul>
				</td>
			</tr>
			</table>
		</div>
	
	
	</body>
</html>