<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>
	
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="/StudentWorkRental/css/admin/admin.css" />
		<title>Contract</title>
	</head>


	<body>
		<br /><br /><br />
		<div align="center">
			<table class="contractTable">
				<col width="65%">
				<col width="35%">
				<tr>
					<th colspan="2">Szerződés adatai</th>
				</tr>
				<tr>
					<td>Szerződés státusza</td>
					<td>
						<c:if test="${selectedContract.state == 'ACTIVE' }">Aktív</c:if>
						<c:if test="${selectedContract.state == 'DRAFT' }">Piszkozat</c:if>
						<c:if test="${selectedContract.state == 'DECLINED' }">Visszautasított</c:if>
						<c:if test="${selectedContract.state == 'EXPIRED' }">Lejárt</c:if>
					</td>
				</tr>
				<tr>
					<td>Szerződéskötés dátuma</td>
					<td>${selectedContract.contractDate }</td>
				</tr>
				<tr>
					<td>Szerződés érvényességének kezdete</td>
					<td>${selectedContract.startDate }</td>
				</tr>
				<tr>
					<td>Szerződés érvényességének vége</td>
					<td>${selectedContract.endDate }</td>
				</tr>
			</table>
			<br />
			<table class="contractTable">
			<col width="50%">
			<col width="50%">
			<tr>
				<th>Tanuló adatai</th>
				<th>Cég adatok</th>
			</tr>
			<tr>
				<td>Név: ${selectedContract.student.lastName } ${selectedContract.student.firstName } </td>
				<td>Cégnév: ${selectedContract.company.companyName }</td>
			</tr>
			<tr>
				<td>Város: ${selectedContract.student.city }</td>
				<td>Város: ${selectedContract.company.city }</td>
			</tr>
			<tr>
				<td>e-Mail: ${selectedContract.student.EMail }</td>
				<td>e-Mail: ${selectedContract.company.EMail }</td>
			</tr>
			<tr>
				<td>Telefonszám: ${selectedContract.student.phoneNumber }</td>
				<td>Telefonszám: ${selectedContract.company.phoneNumber }</td>
			</tr>
			<tr>
				<td>Születési dátum: ${selectedContract.student.birthDate }</td>
				<td>Adószám: ${selectedContract.company.taxNumber }</td>
			</tr>			
			</table>
			<br />
			<table class="contractTable" >
				<col width="35%">
				<col width="65%">
				<th colspan="2">Munka leírása</th>
				<tr>
					<td>Munka megnevezése:</td>
					<td>${selectedContract.job.jobName }</td>
				</tr>
				<tr>
					<td>Leírás:</td>
					<td>${selectedContract.job.jobDescription}</td>
				</tr>
				<tr>
					<td>Szükséges készségek:</td>
					<td>
						<c:forEach var="skills" items="${selectedContract.job.skills}">
								${skills}; 
						</c:forEach>
					</td>
				</tr>
				<tr>
					<td>Óradíj:</td>
					<td>${selectedContract.job.hourlyWage } Forint</td>
				</tr>
				<tr>
					<td>Heti óraszám:</td>
					<td>${selectedContract.job.hoursOfWorkInOneWeek }</td>
				</tr>
				<tr>
					<td>Munkavégzés helye:</td>
					<td>${selectedContract.job.workPlace }</td>
				</tr>
			</table>
		</div>
	
	
	</body>
</html>