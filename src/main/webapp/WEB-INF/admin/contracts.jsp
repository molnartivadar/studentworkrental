<%@page import="oracle.sql.JAVA_STRUCT"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="/StudentWorkRental/css/admin/admin.css" />
<script type="text/javascript" charset="utf-8" src="/StudentWorkRental/javascript/admin/admin.js"></script>
<meta charset="UTF-8">
<title>Adminisztrációs felület - szerződések</title>
</head>
<body onload="showNavMenu()">
	
	<form method="GET">
		<header>
			<div class="container" id="navMenu"></div>
		</header>
	</form>
	<br />
	<br />
	<br />
	<br />
	<div align='center'>
		<h3><b class="bmenu">Szerződések</b></h3>
	</div>
	<div align='center'>
		<form method="GET" action="/StudentWorkRental/ContractsAdminServlet">
			Tanuló neve: <input type='text' class='contractSearchTextfield' name='studentName' id='studentName' value="${studentName}" >
			Cég neve: <input type='text' class='contractSearchTextfield' name='companyName' id='companyName' value="${companyName}" >
			Munka megnevezése: <input type='text' class='contractSearchTextfield' name='jobName' id='jobName' value="${jobName}" >
			<br /><br />
			Szerződés státusza: <select name='contractState' id='contractState' class='dropdown'>
				<option value="ALL">Összes</option>
 				<option value="ACTIVE">Aktív</option>
  				<option value="DRAFT">Piszkozat</option>
  				<option value="EXPIRED">Lejárt</option>
  				<option value="DECLINED">Visszautasított</option>
			</select>
			Szerződés kötés dátuma: 
			<input type="date" name="contractDateFrom" id="contractDateFrom" class="datefieldform" value="${contractDateFrom}" onchange="showContractSubmitButton()" />-tól
			<input type="date" name="contractDateTo" id="contractDateTo" class="datefieldform" value="${contractDateTo}" onchange="showContractSubmitButton()" />-ig
			<br /><br />
			<input type='submit' class='btn' id='getContracts' name='getContracts' value='Lekérdez' /> <br />
		</form>
	</div>
	<div class="errorMessage" align="center">${errorMessage}</div>
	<div class="message" align="center">${message}</div>
	<br />
	<div align="center">
    <table class="userTable">
      <thead>
        <tr>
          <th class="borderOnRight">Szerződés ID</th>
          <th class="borderOnRight">Tanuló neve</th>
          <th class="borderOnRight">Cég neve</th>
          <th class="borderOnRight">Munka megnevezése</th>
          <th class="borderOnRight">Szerződés státusza</th>
          <th class="borderOnRight">Szerződéskötés dátuma</th>
          <th class="borderOnRight">Szerződés kezdete</th>
          <th class="borderOnRight">Szerződés vége</th>
          <th>Megtekint</th>
        </tr>
      </thead>
      <tbody>
      	<c:forEach var="contracts" items="${contracts}">
			<tr>
  	        	<td class="borderOnRight">${contracts.id}</td>
  	        	<td class="borderOnRight">${contracts.student.lastName} ${contracts.student.firstName}</td>
  	        	<td class="borderOnRight">${contracts.company.companyName}</td>
  	        	<td class="borderOnRight">${contracts.job.jobName}</td>
  	        	<td class="borderOnRight">${contracts.state}</td>
  	        	<td class="borderOnRight">${contracts.contractDate}</td>
  	        	<td class="borderOnRight">${contracts.startDate}</td>
  	        	<td class="borderOnRight">${contracts.endDate}</td>
  	        	<td>
  	        		<a href="/StudentWorkRental/ContractsServlet?id=${contracts.id}" target="_blank">
						<img src='/StudentWorkRental/static/eye.png' id='deletequizimg' width='20px' height='20px' >
					</a>
				</td>
			</tr>																			                				
		</c:forEach>
      </tbody>
    </table>
  </div>
</body>
</html>