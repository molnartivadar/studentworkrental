<%@page import="com.bh08.ajfk.models.Admin"%>
<%@page import="oracle.sql.JAVA_STRUCT"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>

<head>
	<link rel="stylesheet" href="/StudentWorkRental/css/admin/admin.css" />
	<script type="text/javascript" charset="utf-8" src = "/StudentWorkRental/javascript/admin/admin.js"></script>
	<meta charset="UTF-8">
	<title>Adminisztrációs felület - adataim</title>
</head>

<body onload="showNavMenu()">
	
	<form method="GET">
		<header>
			<div class="container" id="navMenu"></div>
		</header>
	</form>
	<br />
	<br />
	<br />
	<br />
	<form method="POST" action="/StudentWorkRental/UpdateAdminDataServlet" >
	<div class="left-side">
		<%
			Admin admin = (Admin) session.getAttribute("user");
		%>
		<div align="center"><img src="data:image/jpeg;base64,${picture}" width='150px' height='150px' class='profilpicture' ></div>
		
		<table class='myDataTable'><col width='35%'><col width='65%'>
			<tr>
				<th colspan='2' class="bmenu"><h3>Adataim</h3></th>
			</tr>
		<tr>
			<td>E-mail cím: </td>
			<td><%=admin.getEMail() %> </td>
		</tr>
		<tr>
			<td>Jelszó: </td>
			<td><input type='password' id='password1' name='password1' class='textFieldForm' onkeyup='showUpdateButton()' onchange='checkPassword()' /></td>
		</tr>
		<tr>
			<td>Jelszó újra: </td>
			<td><input type='password' id='password2' name='password2' class='textFieldForm' onkeyup='showUpdateButton()' onchange='checkPassword()' /></td>
		</tr>
		<tr>
			<td>Vezetéknév: </td>
			<td><input type='text' id='lastname' name='lastname' class='textFieldForm' value=<%=admin.getLastName() %> onkeyup='showUpdateButton()' /></td>
		</tr>
		<tr>
			<td>Keresztnév: </td>
			<td><input type='text' id='forname' name='forname' class='textFieldForm' value=<%=admin.getForName() %> onkeyup='showUpdateButton()' /></td>
		</tr>
		<tr>
			<td>Ország: </td>
			<td>
				<select name='country' class='countryDropdown' onchange='showUpdateButton()' >
					<option value=<%=admin.getCountry() %> selected disabled hidden><%=admin.getCountry() %></option>
      				<c:forEach var='CountryList' items='${countryList}'>
      					<option value='${CountryList.country}'>${CountryList.country}</option>
      				</c:forEach>
      			</select>
      		</td>
      	</tr>
		<tr>
			<td>Irányítószám: </td>
			<td><input type='text' id='zipcode' name='zipcode' class='textFieldForm' value=<%=admin.getZipCode() %> onkeyup='showUpdateButton()' /></td>
		</tr>
		<tr>
			<td>Város: </td>
			<td><input type='text' id='city' name='city' class='textFieldForm' value=<%=admin.getCity() %> onkeyup='showUpdateButton()' /></td>
		</tr>
		<tr>
			<td>Közterület neve, házszám: </td>
			<td><input type='text' id='address' name='address' class='textFieldForm' value="<%=admin.getAddress() %>" onkeyup='showUpdateButton()' /></td>
		</tr>
		<tr>
			<td>Telefonszám: </td>
			<td><input type='text' id='phonenumber' name='phonenumber' class='textFieldForm' value=<%=admin.getPhoneNumber() %> onkeyup='showUpdateButton()' /></td>
		</tr>
		</table>
		<br />
		<div align="center">
			<button class="btn" id="update" name="update" type="update" style="visibility:hidden;" onclick="">Adatok módosítása</button>
		</div>
	</div>
	</form>
	<form method="post" action="/StudentWorkRental/UploadFileServlet" enctype="multipart/form-data" id="profile_picture_upload">
		<table class='myDataTable'><col width='35%'><col width='65%'>
			<tr>
				<td>Profilkép feltöltése:</td>
				<td><input type="file" id="fileupload" name="profilePic" accept="image/x-png,image/gif,image/jpeg" />
				<input type="submit" class='btn' value="Upload" />
				</td>
			</tr>
		</table>
	</form>
</body>
</html>