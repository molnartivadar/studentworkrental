<%@page import="com.bh08.ajfk.models.Admin"%>
<%@page import="oracle.sql.JAVA_STRUCT"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>

<head>
	<link rel="stylesheet" href="/StudentWorkRental/css/admin/admin.css" />
	<script type="text/javascript" charset="utf-8" src = "/StudentWorkRental/javascript/admin/admin.js"></script>
	<meta charset="UTF-8">
	<title>Adminisztrációs felület - új adminisztrátor</title>
</head>

<body onload="showNavMenu()">
	
	<form method="GET">
		<header>
			<div class="container" id="navMenu"></div>
		</header>
	</form>
	<br />
	<br />
	<br />
	<br />
	<div>
	<div align="center">
		<%
			Admin newAdmin = (Admin) session.getAttribute("newAdmin");
			String errorMessage = (String) request.getAttribute("error");
			
			if (null != errorMessage) {
				out.println("<h3><b>" + errorMessage + "</b></h3>");
			} else if (null != newAdmin) {
				out.println("<h3>Új adminisztrátor sikeresen felvéve:</h3><h4>Név: " + newAdmin.getLastName() + " "
						+ newAdmin.getForName() + "<br />E-mail cím: " + newAdmin.getEMail() + "</h4>");
				session.removeAttribute("newAdmin");
			}
		%>
	</div>

		<form method="POST" action="/StudentWorkRental/createNewAdminServlet">
			<table class='myDataTable'><col width='35%'><col width='65%'>
				<tr>
					<th colspan='2' class="bmenu"><h3>Új admininsztrátor adatainak megadása</h3></th>
				</tr>
				<tr>
					<td><label for="lastname">Vezetéknév: </label></td>
					<td><input type="text" id="lastname" name="lastname"
						class='textFieldForm' 
						onkeyup="showSubmitButton(); showResetButton()" /> <label
						class="requiredfield"> *</label></td>
				</tr>
				<tr>
					<td><label for="forname">Keresztnév: </label></td>
					<td><input type="text" id="forname" name="forname"
						class="textFieldForm"
						onkeyup="showSubmitButton(); showResetButton()" /> <label
						class="requiredfield"> *</label></td>
				</tr>
				<tr>
					<!-- TODO: e-mail form check -->
					<td><label for="email">E-mail: </label></td>
					<td><input type="text" id="email" name="email"
						class="textFieldForm"
						onkeyup="showSubmitButton(); showResetButton()" /> <label
						class="requiredfield"> *</label></td>
				</tr>
				<tr>
					<!-- TODO: password form check -->
					<td><label for="password">Jelszó: </label></td>
					<td><input type="password" id="password1" name="password1"
						class="textFieldForm"
						onkeyup="showSubmitButton(); showResetButton()"
						onchange="checkPassword()" /> <label class="requiredfield">
							*</label></td>
				</tr>
				<tr>
					<td><label for="password">Jelszó újra: </label></td>
					<td><input type="password" id="password2" name="password2"
						class="textFieldForm"
						onkeyup="showSubmitButton(); showResetButton()"
						onchange="checkPassword()" /> <label class="requiredfield">
							*</label></td>
				</tr>
				<tr>
					<td><label for="country">Ország: </label></td>
					<td>
						<select name="country" id="country" class="countryDropdown" onchange="showSubmitButton(); showResetButton()" >
					    	<option value="" selected disabled hidden>Válassz</option>
					    	<c:forEach var="CountryList" items="${countryList}">
	                			<option value="${CountryList.country}" >${CountryList.country}</option>
	            			</c:forEach>
					    </select>
						<label class="requiredfield"> *</label>
					</td>
				</tr>
				<tr>
					<td><label for="zipcode">Irányítószám: </label></td>
					<td><input type="text" id="zipcode" name="zipcode"
						class="textFieldForm" maxlength="4"
						onkeyup="showSubmitButton(); showResetButton()" /> <label
						class="requiredfield"> *</label></td>
				</tr>
				<tr>
					<td><label for="city">Település: </label></td>
					<td><input type="text" id="city" name="city"
						class="textFieldForm"
						onkeyup="showSubmitButton(); showResetButton()" /> <label
						class="requiredfield"> *</label></td>
				</tr>
				<tr>
					<td><label for="address">Cím: </label></td>
					<td><input type="text" id="address" name="address"
						class="textFieldForm"
						onkeyup="showSubmitButton(); showResetButton()" /> <label
						class="requiredfield"> *</label></td>
				</tr>
				<tr>
					<td><label for="phonenumber">Telefonszám: </label></td>
					<td><input type="text" id="phonenumber" name="phonenumber"
						class="textFieldForm"
						onkeyup="showSubmitButton(); showResetButton()" /> <label
						class="requiredfield"> *</label></td>
				</tr>
				<tr>
					<td><button class="btn" id="reset" name="reset" type="reset"
							style="visibility: hidden;" onclick="onClickResetButton()">Törlés</button></td>
					<td><button class="btn" class="btn" id="submit" name="submit"
							type="submit" style="visibility: hidden;">Új admin
							felvétele</button></td>
				</tr>
			</table>
		</form>
	</div>
</body>
</html>