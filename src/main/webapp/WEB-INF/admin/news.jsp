<%@page import="oracle.sql.JAVA_STRUCT"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="/StudentWorkRental/css/admin/admin.css" />
<script type="text/javascript" charset="utf-8" src="/StudentWorkRental/javascript/admin/admin.js"></script>
<meta charset="UTF-8">
<title>Adminisztrációs felület - hírek</title>
</head>
<body onload="showNavMenu()">
	
	<form method="GET">
		<header>
			<div class="container" id="navMenu"></div>
		</header>
	</form>
	<br />	<br />	<br />	<br />
	
	<div align="center" class="errorMessage">${errorMessage}</div>
	<div align="center" class="message">${message}</div>
	
	<form method="POST" action="/StudentWorkRental/NewsAdminServlet">
		<div align='center'>
			<h3><b class="bmenu">Új hír felvétele</b></h3>
			<label for="isPrimary">Kiemelt hír: </label>
			<input type="checkbox" name="isPrimary" id="isPrimary" value="isPrimary" />
			Cím: <input type='text' id='newNewsHeader' name='newNewsHeader' placeholder="Cím..." />
			<br /><br />
			<textarea rows="10" cols="100" name="newNewsText" id="newNewsText" class="textarea" placeholder="Leírás..."></textarea>
			<br /><br />
			<input type='submit' class='btn' id='setNews' name='SetNews' value='Feltölt' />
		</div>
	</form>
	<br />
	<div class='vertical-line'></div>
	<br />
	<div align='center'>
		<h3><b class="bmenu">Hírek listázása</b></h3>
	</div>
	<div align='center'>
		<form method="GET" action="/StudentWorkRental/NewsAdminServlet">
			<input type='submit' class='btn' id='getNews' name='getNews' value='Lekérdez' />
			<br /><br />
			<table class="newsTable">
				<tr>
					<th class="borderOnRight">ID</th>
					<th class="borderOnRight">Aktív</th>
					<th class="borderOnRight">Kiemelt</th>
					<th class="borderOnRight">Cím</th>
					<th>Leírás</th>
				</tr>
				<c:forEach var="news" items="${news}">
					<tr>
						<td class="borderOnRight">${news.id}</td>
						<td class="borderOnRight">
							<c:choose>
								<c:when test="${news.active}">
							    	<img src='/StudentWorkRental/static/ok.png' width='20px' height='20px' />
								</c:when>
								<c:otherwise>
							    	<img src='/StudentWorkRental/static/delete.png' width='20px' height='20px' />
								</c:otherwise>
							</c:choose>
						</td>
		  	        	<td class="borderOnRight">
							<c:choose>
								<c:when test="${news.primary}">
							    	<img src='/StudentWorkRental/static/ok.png' width='20px' height='20px' />
								</c:when>
								<c:otherwise>
							    	<img src='/StudentWorkRental/static/delete.png' width='20px' height='20px' />
								</c:otherwise>
							</c:choose>
						</td>
			        	<td class="borderOnRight">${news.heading}</td>
			        	<td>${news.text}</td>
					</tr>																			                				
				</c:forEach>
			</table>
		</form>
		</div>
	<br />
	<div class='vertical-line'></div>
	<br />
</body>
</html>