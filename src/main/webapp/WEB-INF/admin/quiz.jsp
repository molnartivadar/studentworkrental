<%@page import="com.bh08.ajfk.models.quiz.Quiz"%>
<%@page import="com.bh08.ajfk.models.quiz.Answer"%>
<%@page import="java.util.List"%>
<%@page import="com.bh08.ajfk.models.quiz.Question"%>
<%@page import="java.util.Map"%>
<%@page import="oracle.sql.JAVA_STRUCT"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="/StudentWorkRental/css/admin/admin.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script type="text/javascript" charset="utf-8" src="/StudentWorkRental/javascript/admin/admin.js"></script>
<meta charset="UTF-8">
<title>Adminisztrációs felület - kvíz</title>
</head>
<body onload="showNavMenu()">
	
	<form method="GET">
		<header>
			<div class="container" id="navMenu"></div>
		</header>
	</form>
	<br />
	<br />
	<br />
	<br />
	<div align='center'>
		<h3><b class="bmenu">Kvízek listázása</b></h3>
	</div>

	<div class="errorMessage">${errorMessage}</div>
	<div class="message">${message}</div>

	<div align='center'>
		<form method="POST" name="quizAdmin" action="/StudentWorkRental/QuizAdminServlet">
			Válassz egy kategóriát: <select name='skillCategory' id='skillCategory' class='dropdown'>
				<c:forEach var='skillCategory' items='${skills}'>
					<option value='${skillCategory.skillName}'>${skillCategory.skillName}</option>
				</c:forEach>
			</select> <input type='submit' class='btn' id='getQuiz' name='btnQuiz' value='List' /> <br />

		<br />
		<table class='quiztable' id='quiztable'>
			<thead>
				<tr>
					<th>ID</th>
					<th>Kérdés</th>
					<th>Válasz 1</th>
					<th>Válasz 2</th>
					<th>Válasz 3</th>
					<th>Válasz 4</th>
					<th>Helyes válasz(ok)</th>
					<th>Szerkesztés</th>
				</tr>
			</thead>
			<tbody>
			<% int rowCount = 1; %>
				<c:forEach var="quiz" items="${quiz}">
					<% int answerCount = 0; %>
					<tr>
						<td>${quiz.question.id}</td>
						<td>${quiz.question.question}</td>
						<c:forEach var="answers" items="${quiz.answers}">
							<td>${answers.answer}</td>
							<% answerCount++; %>
						</c:forEach>
						<% 
							for (int i = answerCount; i < 4; i++) {
									out.println("<td></td>");
							}
						%>
						<td>
							<c:forEach var="correctAnswers" items="${quiz.correctAnswers}">
								${correctAnswers.answer.answer};
							</c:forEach>
						</td>
						<td>
							<img src='/StudentWorkRental/static/edit.png' id=<%=rowCount++ %> width='20px' height='20px' onclick='editQuiz(this.id)' />
							<button class="deletebtn" name="delete" value='${quiz.question.id}' ><i class="fa fa-trash"></i></button>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		</form>
		<br />
		<form method="POST" action="/StudentWorkRental/QuizAdminServlet">
			<div id='editQuiz'></div>
		</form>	
		<br />
		<div class='vertical-line'></div>
		
	</div>

	<div align='center'>
		<h3><b class="bmenu">Kvíz feltöltése</b></h3> Válassz egy kategóriát: 
		<select name='newQuizSkill' class='dropdown' id='newQuizSkill'>
			<c:forEach var='skillCategory' items='${skills}'>
				<option value='${skillCategory.skillName}'>${skillCategory.skillName}</option>
			</c:forEach>
		</select>
		<button type='input' class='btn' id='setQuiz' name='setquiz' onclick='newQuiz()'>Új kvíz</button>
	</div>

	<form method="POST" action="/StudentWorkRental/QuizAdminServlet">
		<div id='newQuiz' align='center'></div>
	</form>
	<br />
	<div class='vertical-line'></div>
	<div align='center'>
		<h3><b class="bmenu">Kvíz feltöltése CSV fájlból</b></h3>
		<form method="post" action="/StudentWorkRental/UploadFileServlet" enctype="multipart/form-data">
			Válassz egy fájlt: <input type="file" if="fileUpload" name="CSV" accept=".csv" />
			<input type="submit" class='btn' value="Upload" />
		</form>
	</div>
	<br />
		<div class='vertical-line'></div>
	<form method="POST" action="/StudentWorkRental/QuizAdminServlet">	
		<div align='center'>
			<h3><b class="bmenu">Új skill felvétele</b></h3>
			<input type='text' id='newSkill' name='newSkill' />
			<input type='submit' class='btn' id='newSkillSubmit' name='btnQuiz' value='Felvétel' />
		</div>
	</form>
	<div class="errorMessage" align='center'>${skillUploadFail}</div>
	<div class="message" align='center'>${skillUploadSuccess}</div>
	<%
		request.removeAttribute("skillUploadFail");
		request.removeAttribute("skillUploadSuccess");
	%>
	<br />
	<div class='vertical-line'></div>
</body>
</html>