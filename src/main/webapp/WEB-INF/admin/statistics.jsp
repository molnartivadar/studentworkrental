<%@page import="java.util.Map"%>
<%@page import="java.time.LocalDate"%>
<%@page import="oracle.sql.JAVA_STRUCT"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>

<html>
<head>

	<link rel="stylesheet" href="/StudentWorkRental/css/admin/admin.css" />
	<link href="webjars/visjs/4.21.0/vis-timeline-graph2d.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
	
	<script type="text/javascript" charset="utf-8" src="/StudentWorkRental/javascript/admin/admin.js"></script>
	<script src="webjars/visjs/4.21.0/vis.js"></script>
	<script src="webjars/chartjs/2.7.3/Chart.bundle.js"></script>
	<script src="webjars/chartjs/2.7.3/Chart.js"></script>
	
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	
	<title>Adminisztrációs felület - statisztikák</title>

</head>

<body onload="showNavMenu()">

	<form method="GET">
		<header>
			<div class="container" id="navMenu"></div>
		</header>
	</form>
	<br />
	<br />
	<br />
	<br />
	<form method="GET" action="/StudentWorkRental/StatisticsServlet">
		<div align="center">
			<b class="bmenu" align="center"><h3>Regisztrált felhasználók</h3></b>
			<input type="submit" class="btn" name="registeredUsers" id="registeredUsers" style="visibility: visible;" value="Lekérdez" />
		</div>
	</form>
	<br />
	
	<%
		Map<String, Integer> users = (Map<String, Integer>) request.getAttribute("registeredUsers");
		if (null != users) {
			int students = users.get("S");
			int companies = users.get("C");
			
			out.println("<div id='users'><table class='registeredUserTable'><tr><td>Regisztrált tanulók száma:</td><td>" + students + "</td></tr>");
			out.println("<tr><td>Regisztrált cégek száma:</td><td>" + companies + "</td></tr></table></div>");
		}
	%>
	
	<br />
	<div class='vertical-line'></div>
	<br />
	<div class="left-side">
		<b class="bmenu" align="center"><h3>Megkötött szerződések</h3></b>
	</div>
	<br />
	<form method="GET" action="/StudentWorkRental/StatisticsServlet">
		<div align="center">
			<%
				LocalDate contractDateFrom = (LocalDate) request.getAttribute("contractDateFrom");
				LocalDate contractDateTo = (LocalDate) request.getAttribute("contractDateTo");
			%>

			Dátumtól: <input type="date" name="contractDateFrom" id="contractDateFrom" class="datefieldform" value=<%=contractDateFrom%> onchange="showContractSubmitButton()" />
			Dátumig: <input type="date" name="contractDateTo" id="contractDateTo" class="datefieldform" value=<%=contractDateTo%> onchange="showContractSubmitButton()" />
			<input type="submit" class="btn" name="contractDateSubmit" id="contractDateSubmit" style="visibility: visible;" value="Lekérdez" />
		</div>
	</form>
	<br />

	<div id="concludedContracts" class="chart"></div>
	<script type="text/javascript">
		var container = document.getElementById('concludedContracts');
		var contracts = ${contracts};

		var items = [];

		for ( var obj in contracts) {
			if (contracts.hasOwnProperty(obj)) {
				for ( var prop in contracts[obj]) {
					if (contracts[obj].hasOwnProperty(prop)) {
						var object = {x : prop, y : contracts[obj][prop]};
						items.push(object);
					}
				}
			}
		}

		var day1 = document.getElementById('contractDateFrom').value;
		var day2 = document.getElementById('contractDateTo').value;

		var dataset = new vis.DataSet(items);
		var options = {
			width : '70%',
			height : '400px',
			zoomable : false,
			start : day1,
			end : day2,
		};
		var graph2d = new vis.Graph2d(container, dataset, options);
	</script>
	
	<br />
	<div class='vertical-line'></div>
	<br />
	
	<div class="left-side">
		<b class="bmenu" align="center"><h3>Várható bevétel a szerződések után</h3></b>
	</div>
	<form method="GET" action="/StudentWorkRental/StatisticsServlet">
		<div align=center>
			<%
				LocalDate incomeDateFrom = (LocalDate) request.getAttribute("incomeDateFrom");
				LocalDate incomeDateTo = (LocalDate) request.getAttribute("incomeDateTo");
			%>

			Dátumtól: <input type="date" name="incomeDateFrom" id="incomeDateFrom" class="datefieldform" value=<%=incomeDateFrom%> onchange="showIncomeSubmitButton()" />
			Dátumig: <input type="date" name="incomeDateTo" id="incomeDateTo" class="datefieldform" value=<%=incomeDateTo%> onchange="showIncomeSubmitButton()" />
			
			<input type="submit" class="btn" name="incomeSubmit" id="incomeSubmit" style="visibility: visible;" value="Lekérdez" />
			
		</div>
	</form>
	<br />
	
	<div id='income' class='chart'></div>
	
		<script type="text/javascript">
		var container = document.getElementById('income');
		var incomes = ${incomes};

		var items = [];

		for ( var obj in incomes) {
			if (incomes.hasOwnProperty(obj)) {
				console.log(prop + ':' + incomes[obj][prop]);
				for ( var prop in incomes[obj]) {
					if (incomes[obj].hasOwnProperty(prop)) {
						var object = {x : prop, y : incomes[obj][prop]};
						items.push(object);
					}
				}
			}
		}

		var day1 = document.getElementById('incomeDateFrom').value;
		var day2 = document.getElementById('incomeDateTo').value;

		var dataset = new vis.DataSet(items);
		var options = {
			width : '70%',
			height : '400px',
			zoomable : false,
			start : day1,
			end : day2,
		};
		var graph2d = new vis.Graph2d(container, dataset, options);
	</script>
	
	<br />
	<div class='vertical-line'></div>
	<br />
</body>
</html>