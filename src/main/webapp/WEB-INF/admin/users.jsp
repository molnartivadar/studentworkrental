<%@page import="com.bh08.ajfk.models.User"%>
<%@page import="java.util.List"%>
<%@page import="oracle.sql.JAVA_STRUCT"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="/StudentWorkRental/css/admin/admin.css" />
<script type="text/javascript" charset="utf-8" src="/StudentWorkRental/javascript/admin/admin.js"></script>
<meta charset="UTF-8">
<title>Felhasználók</title>
</head>
<body onload="showNavMenu()">
	
	<form method="GET">
		<header>
			<div class="container" id="navMenu"></div>
		</header>
	</form>
	<br /><br /><br /><br />
	<div align='center'>
		<h3><b class="bmenu">Felhasználók listázása</b></h3>
	</div>
	<div align='center'>
		<form method="POST" action="/StudentWorkRental/UserListAdminServlet">
			Válassz egy felhasználói csoportot: <select name='userCategory' id='userCategory' class='dropdown'>
				<option value="Admin">Adminisztátorok</option>
 				<option value="Company">Cégek</option>
  				<option value="Student">Tanulók</option>
			</select>
			<input type='submit' class='btn' id='getUsers' name='getUsers' value='Lekérdez' /> <br />
		</form>
	</div>
	<div class="errorMessage" align="center">${errorMessage}</div>
	<div class="message" align="center">${message}</div>
	<br />
	<div align="center">
    <table class="userTable">
      <thead>
        <tr>
          <th class="borderOnRight">ID</th>
          <th class="borderOnRight">Név</th>
          <th class="borderOnRight">e-Mail</th>
          <th class="borderOnRight">Telefonszám</th>
          <th class="borderOnRight">Város</th>
          <th class="borderOnRight">Regisztráció időpontja</th>
          <th>Megtekint</th>
        </tr>
      </thead>
      <tbody>
      	<c:forEach var="users" items="${users}">
			<tr>
				<td class="borderOnRight">${users.id}</td>
				<td class="borderOnRight">
					<c:if test="${userType == 'Admin'}"> ${users.lastName } ${users.forName } </c:if>
					<c:if test="${userType == 'Company'}"> ${users.companyName } </c:if>
					<c:if test="${userType == 'Student'}"> ${users.lastName } ${users.firstName } </c:if>
				</td>
				<td class="borderOnRight">${users.EMail}</td>
  	        	<td class="borderOnRight">${users.phoneNumber}</td>
	        	<td class="borderOnRight">${users.city}</td>
	        	<td class="borderOnRight">${users.registrationDate}</td>
	        	<td>
  	        		<a href="/StudentWorkRental/MyProfileServlet?id=${users.id}" target="_blank">
						<img src='/StudentWorkRental/static/eye.png' id='viewUser' width='20px' height='20px' >
					</a>
				</td>
			</tr>																			                				
		</c:forEach>
      </tbody>
    </table>
  </div>
	
</body>
</html>