<%@page import="com.bh08.ajfk.models.Company"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

	<style>
a.button {
	-webkit-appearance: button;
	-moz-appearance: button;
	appearance: button;
	text-decoration: none;
	border-style: groove;
	color: green;
}

td.bordered {
	border-style: groove;
	color: blue;
}
</style>

	<%
		Company company = (Company) session.getAttribute("user");
	%>

	<h3>
		Welcome <b> <%
 	out.println(company.getCompanyName() + " !");
 %> <br />
	</h3>

	<table>
		<tr>
			<td><a href="/StudentWorkRental/MyProfileServlet?id=${user.id}"
				class="button">Nézet</a></td>
			<td><a href="/StudentWorkRental/CompanyOffers" class="button">
					Ajánlatok </a></td>
			<td><a href="/StudentWorkRental/CompanyProfile" class="button">
					Munkavállalók </a></td>
			<td><a href="/StudentWorkRental/CompanyProfile" class="button">
					Értékelések </a></td>
			<td><a href="/StudentWorkRental/LoginServlet?reset=true"
				class="button"> Kilépés </a></td>

		</tr>
		<tr>
			<td width="30"></td>
		</tr>
	</table>



	<table style="width: 500px" title="adatok">
		<col width="150px">
		<col width="245px">
		<col width="40px">
		<col width="120px">

		<tr>
			<td height="15"></td>
		</tr>

		<form id="modify" action="CompanyRegisterServlet" method="post">

			<tr>
				<td class="bordered">e-mail</td>
				<td class="bordered">
					<%
						out.println(company.getEMail());
					%>
				</td>

			</tr>

			<tr>
				<td class="bordered">Password</td>
				<td class="bordered"><input name="password" type="password"
					value="****"></td>
			</tr>

			<tr>
				<td colspan="2">Please enter password before clicking submit!</td>
			</tr>

			<tr>
				<td class="bordered">Company Name</td>
				<td class="bordered"><input name="companyname" type="text"
					size="50" value="${user.companyName}"></td>
			</tr>

			<tr>
				<td class="bordered">Country</td>
				<td class="bordered"><input name="country" type="text"
					value="${user.country}"></td>
			</tr>

			<tr>
				<td class="bordered">ZIP Code</td>
				<td class="bordered"><input name="zipcode" type="text"
					value="${user.zipCode}"></td>
			</tr>

			<tr>
				<td class="bordered">City</td>
				<td class="bordered"><input name="city" type="text"
					value="${user.city}"></td>
			</tr>

			<tr>
				<td class="bordered">Address</td>
				<td class="bordered"><input name="adress" type="text"
					value="${user.address}"></td>
			</tr>

			<tr>
				<td class="bordered">Phone</td>
				<td class="bordered"><input name="phone" type="text"
					value="${user.phoneNumber}"></td>
			</tr>

			<tr>
				<td class="bordered">TaxID</td>
				<td class="bordered"><input name="taxid" type="text"
					value="${user.taxNumber}"></td>
			</tr>

			<tr>
				<td class="bordered">Description</td>
				<td class="bordered" name="description" id="description"><input
					type="textarea" rows="5" name="description" size="50"
					value="${user.description}"></td>
			</tr>

			<tr>
				<td></td>


				<td><input type="hidden" name="email" value="${user.EMail}">
					<input type="hidden" name="modifycompany" value="true"> <input
					type="submit" value="Modify data"></td>
			</tr>
	</table>
	</form>

	<table>
		<tr>
			<form class="" action="UploadFileServlet" method="post"
				enctype="multipart/form-data" id="profile_picture_upload">

				<td>Profilkép (JPEG/PNG)</td>
				<td><input type="file" id="fileupload" name="profilePic"
					accept="image/x-png,image/gif,image/jpeg"></td>
				<td><button type="submit" class="uploadfile">Feltöltés</button></td>
		</tr>
		<tr>
			<td></td>
			<td><c:if test="${!empty user.profilePicture}">
					<h6>${user.profilePicture}</h6>
				</c:if></td>
		</tr>
		</form>

	</table>

</body>
</html>