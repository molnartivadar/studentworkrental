<%@page import="com.bh08.ajfk.models.Company"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Insert title here</title>
</head>
<body>
	<%
		Company company = (Company) session.getAttribute("user");
		//int numberOfJobs = company.getOfferedJobs().size();
		//	out.print(company.getCompanyName() + " " + "ajánlatai:");
		pageContext.setAttribute("offers", company.getOfferedJobs());
		
	%>

	<h3>${user.companyName} ajánlatai:</h3>

	<form action="/StudentWorkRental/JobMakerServlet" method="POST">
		<table>
			<th>Work</th>
			<th>HUF/h</th>
			<th>City</th>
			<c:forEach items="${offers}" var="current">
				<tr>
					<td><c:out value="${current.jobName}" /></td>
					<td><c:out value="${current.hourlyWage}" /></td>
					<td><c:out value="${current.workPlace}" /></td>
					<td><input type="radio"  name="jobid" value="${current.id}" /></td>
					<!-- name="${offers}" -->
				</tr>
			</c:forEach>
		</table>
		<input type="hidden" name="newjob" value="false"> <input
			type="submit" value="Kiválasztott ajánlat szerkesztése"
			name="submitOrderID" />
	</form>
	<h3>Új ajánlat felvitele:</h3>
	<form action="/StudentWorkRental/JobMakerServlet" method="POST">
		<input type="hidden" name="newjob" value="true">
		<input type="submit" value="új felvitel"
			name="new" />
	</form>
</body>
</html>