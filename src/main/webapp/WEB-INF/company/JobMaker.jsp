<%@page import="com.bh08.ajfk.models.Company"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>

<body>
	<h3>Ajánlat szerkesztése</h3>
	<br />
	<%
		/*if ("newjob"=="true"){
			out.println("Új ajánlat létrehozása");
		}else{
			out.println((String) request.getAttribute("jobid") + ". számú ajánlat szerkesztése:");
		}
		out.println();
		*/
	%>
	<br />

	<c:if test="${null eq jobid}">
		<p>nincs jobID</p>
	</c:if>
	<c:out value="${selectedjob.jobName}" />


	<form id="modify" action="ModifyJobServlet" method="post">
		<table>
			<tr>
				<td class="bordered">Ajánlat neve</td>
				<td class="bordered"><input name="jobname" type="text"
					size="50" value="${selectedjob.jobName}"></td>
			</tr>

			<tr>
				<td class="bordered">Óradíj</td>
				<td class="bordered"><input name="hourlywage" type="text"
					value="${selectedjob.hourlyWage}"></td>
			</tr>

			<tr>
				<td class="bordered">Hely</td>
				<td class="bordered"><input name="workplace" type="text"
					value="${selectedjob.workPlace}"></td>
			</tr>

			<tr>
				<td class="bordered">Heti Óraszám</td>
				<td class="bordered"><input name="hoursofworkinoneweek"
					type="text" value="${selectedjob.hoursOfWorkInOneWeek}"></td>
			</tr>

			<tr>
				<td class="bordered">Munka leírása</td>
				<td class="bordered" name="jobdescription" id="jobdescription"><input
					type="textarea" rows="5" name="jobdescription" size="50"
					value="${selectedjob.jobDescription}"></td>
			</tr>

	
			</tr><td>Szükséges képességek:</td></tr>
			
			<c:forEach items="${skills}" var="current">
			<tr>	
			<td><c:out value="${current.skillName}" /></td>
			<td><input type="checkbox" name="skillid" value="${current.id}" /></td>
		</tr>
			</c:forEach>
	


			<tr>
				<td></td>

				<td>
					<!-- 
				<input type="hidden" name="email" value="${user.EMail}">
				
					<input type="hidden" name="newjob" value="true">
					 
					 <input type="hidden" name="newjob" value='<c:if test=""></c:if>'>-->


					
					<c:if test="${null eq jobid}">
						<input type="hidden" name="newjob" value="true">
					</c:if> <c:out value="${selectedjob.jobName}" /> <input type="submit"
					value="Mentés">
				</td>
			</tr>
		</table>
	</form>

</body>
</html>
