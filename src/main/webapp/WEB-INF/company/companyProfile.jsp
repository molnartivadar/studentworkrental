<%@page import="com.bh08.ajfk.models.Company"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
	
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Cég profil</title>
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<link rel="stylesheet" href="/StudentWorkRental/css/student/studentProfile.css" />
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
	</head>

	<body>
	
		<div class="container">
		  <div class="row">
		    <div class="col-md-6 img">	    	
		    	<img src="data:image/png;base64,${picture}" id="profile-img" class="profile-img-card"/>  
		    </div>
		    <div class="col-md-6 details main">
		      <blockquote>
		        <h4>${selectedUser.companyName}</h4>
		        <small><cite title="Source Title">${selectedUser.country}, ${selectedUser.city} <i class="icon-map-marker"></i></cite></small>
		      </blockquote>
		      <p>
		        <u>Email:</u> ${selectedUser.EMail} <br>
		        <u>Telefon:</u> ${selectedUser.phoneNumber} <br>       
		      </p>
		    </div>
		  </div>
		  
		  
		<div class="row">
					<b><u>Leírás:</u></b> <br>
					<p>	${selectedUser.description} </p>	         	
        </div>	
        
        <div class="row">
		    <b><u>Munkáink:</u></b>
		    <table width="80%" class="table jobs">
		      <thead class="thead-light">
		        <tr>
		          <th scope="col">Munka</th>
		          <th scope="col">Munka leírás</th>
		          <th scope="col">Szükséges képesítések</th>
		          <th scope="col">Heti munkaidő</th>
		          <th scope="col">Fizetés</th>
		          <th scope="col">Munkavégzés helye</th>
		        </tr>
		      </thead>
		      <tbody>
		           	<c:forEach var="jobList" items="${selectedUser.offeredJobs}">
						<tr>
						  <td><a href="/StudentWorkRental/JobServlet?id=${jobList.id}" target="_blank">${jobList.jobName}</a></td>
				          <td>${jobList.jobDescription}</td>
				          <td>
				          	<c:forEach var="skillList" items="${jobList.skills}" varStatus="loop">
					             	<c:out value = "${skillList}"/><c:if test="${!loop.last}">,</c:if> 
					        </c:forEach>
				          </td>
				          <td>${jobList.hoursOfWorkInOneWeek} óra</td>
				          <td><fmt:formatNumber value="${jobList.hourlyWage}" maxFractionDigits="0" /> Ft/Óra</td>
				          <td>${jobList.workPlace}</td>
						</tr>																			                				
					</c:forEach>
		      </tbody>
		    </table>
		  </div>
               
        	<b><u>Értékelés:</u></b>
        
        	<div class="row">
        		
				<div class="col-sm-3">
					
					<div class="rating-block">
						<h4>Átlagos értékelés</h4>
						<h2 class="bold padding-bottom-7">${averageRating}<small>/ 5</small></h2>
					</div>
					
				</div>
			
				<div class="col-sm-3">
					<h4>Értékelések megoszlása</h4>
					<div class="pull-left">
						<div class="pull-left" style="width:35px; line-height:1;">
							<div style="height:9px; margin:5px 0;">5 <span class="glyphicon glyphicon-star"></span></div>
						</div>
						<div class="pull-left" style="width:180px;">
							<div class="progress" style="height:9px; margin:8px 0;">
							  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="5" style="width: 1000%">
								<span class="sr-only">80% Complete (danger)</span>
							  </div>
							</div>
						</div>
						<div class="pull-right" style="margin-left:10px;">${fiveRating}</div>
					</div>
					<div class="pull-left">
						<div class="pull-left" style="width:35px; line-height:1;">
							<div style="height:9px; margin:5px 0;">4 <span class="glyphicon glyphicon-star"></span></div>
						</div>
						<div class="pull-left" style="width:180px;">
							<div class="progress" style="height:9px; margin:8px 0;">
							  <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="4" aria-valuemin="0" aria-valuemax="5" style="width: 80%">
								<span class="sr-only">80% Complete (danger)</span>
							  </div>
							</div>
						</div>
						<div class="pull-right" style="margin-left:10px;">${fourRating}</div>
					</div>
					<div class="pull-left">
						<div class="pull-left" style="width:35px; line-height:1;">
							<div style="height:9px; margin:5px 0;">3 <span class="glyphicon glyphicon-star"></span></div>
						</div>
						<div class="pull-left" style="width:180px;">
							<div class="progress" style="height:9px; margin:8px 0;">
							  <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="5" style="width: 60%">
								<span class="sr-only">80% Complete (danger)</span>
							  </div>
							</div>
						</div>
						<div class="pull-right" style="margin-left:10px;">${threeRating}</div>
					</div>
					<div class="pull-left">
						<div class="pull-left" style="width:35px; line-height:1;">
							<div style="height:9px; margin:5px 0;">2 <span class="glyphicon glyphicon-star"></span></div>
						</div>
						<div class="pull-left" style="width:180px;">
							<div class="progress" style="height:9px; margin:8px 0;">
							  <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="5" style="width: 40%">
								<span class="sr-only">80% Complete (danger)</span>
							  </div>
							</div>
						</div>
						<div class="pull-right" style="margin-left:10px;">${twoRating}</div>
					</div>
					<div class="pull-left">
						<div class="pull-left" style="width:35px; line-height:1;">
							<div style="height:9px; margin:5px 0;">1 <span class="glyphicon glyphicon-star"></span></div>
						</div>
						<div class="pull-left" style="width:180px;">
							<div class="progress" style="height:9px; margin:8px 0;">
							  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="1" aria-valuemin="0" aria-valuemax="5" style="width: 20%">
								<span class="sr-only">80% Complete (danger)</span>
							  </div>
							</div>
						</div>
						<div class="pull-right" style="margin-left:10px;">${oneRating}</div>
					</div>
				</div>			
			</div>		
			
		</div>
		
	</body>

</html>