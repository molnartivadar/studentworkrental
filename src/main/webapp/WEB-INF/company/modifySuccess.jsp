<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<head>
<meta charset="windows-1255">
<title>Insert title here</title>
</head>
<body>
	<c:if test="${!empty modifiedcompany}">

		<div class="alert alert-success" role="alert">Adat módosítva.</div>
		<button onclick="goBack()">Vissza</button>

		<script>
			function goBack() {
				window.history.back();
			}
		</script>
	</c:if>
</body>
</html>