<%@page import="com.bh08.ajfk.beans.LoginBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>

<!DOCTYPE html>
<html>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Diákmunka a legkönnyebben</title>

<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
<link href="/StudentWorkRental/css/login.css" rel="stylesheet">
<script
	src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

</head>

<body>

	<c:if test="${successLogin}">

		<div class="alert alert-success" role="alert">Gratulálunk
			sikeres regisztráció, jelentkezzen be!</div>

	</c:if>

	<c:if test="${!empty modifiedcompany}">

		<div class="alert alert-success" role="alert">Adat módosítva.</div>
		<button onclick="goBack()">Go Back</button>

		<script>
			function goBack() {
				window.history.back();
			}
		</script>
	</c:if>

	<c:if test="${loginBean.invalidLogin}">

		<div class="alert alert-danger" role="alert">Hibás email/jelszó!
			Kérjük adja meg újra!</div>

	</c:if>

	<c:if test="${!loginBean.validLogin}">

		<div class="container">

			<div class="card card-container">

				<img id="profile-img" class="profile-img-card"
					src="/StudentWorkRental/static/AJFK_logo_bw.png" />
				<p id="profile-name" class="profile-name-card"></p>

				<form method="POST" class="form-signin">

					<span id="reauth-email" class="reauth-email"></span> 
					
					<input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email cím" required autofocus> 
						
					<input	type="password" id="inputPassword" name="password"	class="form-control" placeholder="Jelszó" required>
												
					<button class="btn btn-lg btn-primary btn-block btn-signin"	type="submit" name="login-form" value="login">Belépés</button>

				</form>

				<form method="POST">

					<button class="btn btn-lg btn-primary btn-block btn-signin"
						name="login-form" value="register_student">Regisztráció
						diákként</button>
					<button class="btn btn-lg btn-primary btn-block btn-signin"
						name="login-form" value="register_company">Regisztráció
						cégként</button>

				</form>

			</div>

		</div>

	</c:if>

</body>

</html>