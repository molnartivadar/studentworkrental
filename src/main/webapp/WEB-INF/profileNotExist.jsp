<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>
	
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">	
			
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
	
	</head>

	<body>
	
		<c:if test="${!empty existUser}">
 			
			<c:if test="${!existUser}">
			
				<div class="alert alert-danger" role="alert">
					Nincs ilyen felhasználó regisztrálva!
				</div>
			
			</c:if>
			
		</c:if>
		
		<c:if test="${!empty existJob}">
 			
			<c:if test="${!existJob}">
			
				<div class="alert alert-danger" role="alert">
					Nincs ilyen munka az adatbázisban!
				</div>
			
			</c:if>
			
		</c:if>
	
	</body>


</html>