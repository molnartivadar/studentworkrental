<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>${selectedJob.jobName}</title>
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>	

	
	</head>
	
	
	<body>
	
		<div class="container">
			
			<h1>${selectedJob.jobName}</h1>
			
			<br><br>
		
			<h3><u>Leírás:</u></h3>
			<p>${selectedJob.jobDescription}</p>
			
			<h3><u>Fizetés:</u></h3>
			<p>${selectedJob.hourlyWage} Ft/óra</p>
			
			<h3><u>Heti munkaidő:</u></h3>
			<p>${selectedJob.hoursOfWorkInOneWeek} óra</p>
			
			<h3><u>Munkavégzés helye:</u></h3>
			<p>${selectedJob.workPlace}</p>
		
			<h3><u>Szükséges skillek:</u></h3>
			<c:forEach var="skillList" items="${selectedJob.skills}" varStatus="loop">
		    	<c:out value = "${skillList}"/><c:if test="${!loop.last}">,</c:if> 
		    </c:forEach>
		    
		    <h3>Munkát kínáló cég:</h3>
		    <a href="/StudentWorkRental/MyProfileServlet?id=${selectedJob.company.id}" target="_blank">${selectedJob.company.companyName}</a>
		
		</div>
	
	
	</body>
	
</html>