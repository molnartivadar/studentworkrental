<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Szerződéseim</title>
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<link rel="stylesheet" href="/StudentWorkRental/css/student/myContract.css" />
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>	

		<script type="text/javascript" charset="utf-8" src = "/StudentWorkRental/javascript/student/studentNavigation.js"></script>
	
	</head>
	
	
	<body>
	
		<div id="nav"></div>
		
		<div class="container">  			     	
			<table class="table table-bordered table-striped table-highlight table-responsive">
				<caption><h3><u>Élő szerződéseim:</u></h3></caption>
				<tr>
					<th>Munka</th>
					<th>Kezdő dátum</th>
					<th>Vég dátum</th>
					<th>Szerződés dátum</th>
					<th>Cég</th>
				</tr>
				
				
					<c:forEach var="contractList" items="${contractList}">
						<c:if test="${contractList.state == 'ACTIVE'}">
							<tr>
							
								<td ><a href="/StudentWorkRental/JobServlet?id=${contractList.job.id}" target="_blank">${contractList.job.jobName}</a></td>
								<td >${contractList.startDate}</td>
								<td >${contractList.endDate}</td>
								<td >${contractList.contractDate}</td>
								<td ><a href="/StudentWorkRental/MyProfileServlet?id=${contractList.company.id}" target="_blank">${contractList.company.companyName}</a></td>
	
							</tr>	      
						</c:if>          				
	           		</c:forEach>
					
				
			</table>
		</div>
		
		<div class="container">  			     	
			<table class="table table-bordered table-striped table-highlight table-responsive">
				<caption><h3><u>Lejárt szerződéseim:</u></h3></caption>
				<tr>
					<th>Munka</th>
					<th>Kezdő dátum</th>
					<th>Vég dátum</th>
					<th>Szerződés dátum</th>
					<th>Cég</th>
				</tr>
				
				
					<c:forEach var="contractList" items="${contractList}">
						<c:if test="${contractList.state == 'EXPIRED'}">
							<tr>
							
								<td ><a href="/StudentWorkRental/JobServlet?id=${contractList.job.id}" target="_blank">${contractList.job.jobName}</a></td>
								<td >${contractList.startDate}</td>
								<td >${contractList.endDate}</td>
								<td >${contractList.contractDate}</td>
								<td ><a href="/StudentWorkRental/MyProfileServlet?id=${contractList.company.id}" target="_blank">${contractList.company.companyName}</a></td>
	
							</tr>	      
						</c:if>          				
	           		</c:forEach>
					
				
			</table>
		</div>
		
		<div class="container">  			     	
			<table class="table table-bordered table-striped table-highlight table-responsive">
				<caption><h3><u>Visszautasított szerződéseim:</u></h3></caption>
				<tr>
					<th>Munka</th>
					<th>Kezdő dátum</th>
					<th>Vég dátum</th>
					<th>Szerződés dátum</th>
					<th>Cég</th>
				</tr>
				
				
					<c:forEach var="contractList" items="${contractList}">
						<c:if test="${contractList.state == 'DECLINED'}">
							<tr>
							
								<td ><a href="/StudentWorkRental/JobServlet?id=${contractList.job.id}" target="_blank">${contractList.job.jobName}</a></td>
								<td >${contractList.startDate}</td>
								<td >${contractList.endDate}</td>
								<td >${contractList.contractDate}</td>
								<td ><a href="/StudentWorkRental/MyProfileServlet?id=${contractList.company.id}" target="_blank">${contractList.company.companyName}</a></td>
	
							</tr>	      
						</c:if>          				
	           		</c:forEach>
					
				
			</table>
		</div>
		
		<br>
		
		<div class="container">
			<hr class="style5">
		</div>
		
		<br>
		
		<div class="container">  			     	
			<table class="table table-bordered table-striped table-highlight table-responsive">
				<caption><h3><u>Ajánlataim:</u></h3></caption>
				<tr>
					<th>Munka</th>
					<th>Kezdő dátum</th>
					<th>Vég dátum</th>
					<th>Szerződés dátum</th>
					<th>Cég</th>
					<th></th>
				</tr>
				
				
					<c:forEach var="contractList" items="${contractList}">
						<c:if test="${contractList.state == 'DRAFT'}">
						<form action="/StudentWorkRental/MyContractServlet" method="post"  id="contract_form">
							<tr>
								
									<td ><a href="/StudentWorkRental/JobServlet?id=${contractList.job.id}" target="_blank">${contractList.job.jobName}</a></td>
									<td >${contractList.startDate}</td>
									<td >${contractList.endDate}</td>
									<td >${contractList.contractDate}</td>
									<td ><a href="/StudentWorkRental/MyProfileServlet?id=${contractList.company.id}" target="_blank">${contractList.company.companyName}</a></td>
									<td id="buttons">
										
										<button type="submit" class="btn btn-labeled btn-success" value="${contractList.id}" name="yes-button">
							                <i class="glyphicon glyphicon-ok"></i>
							            </button>
							            
							            <button type="submit" class="btn btn-labeled btn-danger" value="${contractList.id}" name="no-button">
	                						<i class="glyphicon glyphicon-remove"></i>
	                					</button>
										
									</td>
								
							</tr>
						</form>	      
						</c:if>          				
	           		</c:forEach>
					
				
			</table>
		</div>
		
	</body>
	
	
</html>