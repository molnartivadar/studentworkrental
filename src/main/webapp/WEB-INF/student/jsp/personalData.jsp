<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>

	<head>
	
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Személyes adatok</title>
		
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
		<link rel="stylesheet" href="/StudentWorkRental/css/student/studentRegister.css" />
		<link rel="stylesheet" href="/StudentWorkRental/css/student/personalData.css" />
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
		<script type="text/javascript" charset="utf-8" src = "/StudentWorkRental/javascript/student/studentPersonalData.js"></script>
		<script type="text/javascript" charset="utf-8" src = "/StudentWorkRental/javascript/student/studentNavigation.js"></script>
	</head>

	<body>	 	 
	
		<div id="nav"></div>	
		
		<c:if test="${!empty successSkillUpload}">
			
			<c:if test="${!successSkillUpload}">
			
				<div class="alert alert-danger" role="alert">
					Sikertelen skill módosítás! Kérem számot adjon meg!
				</div>
			
			</c:if>
			
		</c:if>
 	 	
 	 	<c:if test="${!empty successUpload}">
   

	 	 	<c:if test="${successUpload}">
			
				<div class="alert alert-success" role="alert">
					Sikeres feltöltés!
				</div>
			
			</c:if>
			
			<c:if test="${!successUpload}">
			
				<div class="alert alert-danger" role="alert">
					Sikertelen feltöltés! Kérem adja meg a feltölteni kívánt fájlt!
				</div>
			
			</c:if>
			
		</c:if>
		
		<c:if test="${!empty goodFormat}">
 			
			<c:if test="${!goodFormat}">
			
				<div class="alert alert-danger" role="alert">
					Sikertelen feltöltés! Nem jó formátumot adott meg!
				</div>
			
			</c:if>
			
		</c:if>
 	 	
		<div class="container">
			  		
			<form class="well form-horizontal" action="" method="post"  id="contact_form" >
			
			 <img src="data:image/jpeg;base64,${picture}" id="profile-img" class="profile-img-card"/> 
			
				<fieldset>

					<legend><center><h2><b>Személyes adatok</b></h2></center></legend>
					
					<center><h3>Belépési adatok</h3></center>
					
					<div class="form-group">
					  <label class="col-md-4 control-label">E-Mail cím</label>  
					  <div class="col-md-4 inputGroupContainer">
					  	<div class="input-group">
					       	<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
					  		<input name="email" value="${user.EMail}" class="form-control"  type="text" readonly>
					   	</div>
					  </div>
					</div>
					
					<div class="form-group">
					  <label class="col-md-4 control-label" >Jelszó</label> 
					  <div class="col-md-4 inputGroupContainer">
					   	<div class="input-group">
					  		<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
					 		<input name="user_password" placeholder="Jelszó" class="form-control"  type="password">
					   	</div>
					  </div>
					</div>
					
					<div class="form-group">
					  <label class="col-md-4 control-label" >Jelszó megerősítése</label> 
					  <div class="col-md-4 inputGroupContainer">
					    <div class="input-group">
					  		<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
					  		<input name="confirm_password" placeholder="Jelszó megerősítése" class="form-control"  type="password">
					    </div>
					  </div>
					</div>
					
					<center><h3>Személyes adatok</h3></center>
					
					<div class="form-group">
					  <label class="col-md-4 control-label" >Vezetéknév</label> 
					  <div class="col-md-4 inputGroupContainer">
					    <div class="input-group">
					  		<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
					  		<input name="last_name" placeholder="Vezetéknév" class="form-control"  type="text" value="${user.lastName}">
					    </div>
					  </div>
					</div>
					
					<div class="form-group">
					  <label class="col-md-4 control-label">Keresztnév</label>  
					  <div class="col-md-4 inputGroupContainer">
					  	<div class="input-group">
					  		<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
					  		<input  name="first_name" placeholder="Keresztnév" class="form-control"  type="text" value="${user.firstName}">
					    </div>
					  </div>
					</div>
					
					<div class="form-group"> 
					  <label class="col-md-4 control-label">Neme</label>
					    <div class="col-md-4 selectContainer">
					   		<div class="input-group">
					        	<span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
					    		<select name="gender" class="form-control selectpicker">
					      			 <option value="${user.gender}" selected hidden>${user.gender}</option>
					      			<c:forEach var="GenderList" items="${genderList}">
	                					<option value="${GenderList}" >${GenderList}</option>
	            		  			</c:forEach>
					    		</select>
					  		</div>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label" >Anyja neve</label> 
						<div class="col-md-4 inputGroupContainer">
						   	<div class="input-group">
						 		 <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
						 		 <input name="mother_name" placeholder="Anyja neve" class="form-control"  type="text" value="${user.motherName}" readonly>
						   	</div>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label" >Születési hely</label> 
						<div class="col-md-4 inputGroupContainer">
						   	<div class="input-group">
						 		 <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
						 		 <input name="birth_place" placeholder="Születési hely" class="form-control"  type="text" value="${user.birthPlace}" readonly>
						   	</div>
						</div>
					</div>
						
					<div class="form-group">
						<label class="col-md-4 control-label" >Születési idő</label> 
						 <div class="col-md-4 inputGroupContainer">
						    <div class="input-group">
						    	<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
		        				<input name="birth_date" type="date" class="datepicker form-control" id="datePicker" max="2019-02-27"  value="${user.birthDate}" readonly>
						    </div>
						 </div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label" >Adóazonosító</label> 
						<div class="col-md-4 inputGroupContainer">
						   	<div class="input-group">
						 		 <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
						 		 <input name="tax_num" placeholder="7898458796" class="form-control"  type="text"  value="${user.taxId}">
						   	</div>
						</div>
					</div>
								
					<center><h3>Kontakt adatok</h3></center>
					
					<div class="form-group">
					  <label class="col-md-4 control-label">Telefonszám</label>  
					  <div class="col-md-4 inputGroupContainer">
					    <div class="input-group">
					        <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
					  		<input name="contact_no" placeholder="36305806303" class="form-control" type="text"  value="${user.phoneNumber}">
					    </div>
					  </div>
					</div>
					
					<div class="form-group"> 
						<label class="col-md-4 control-label">Ország</label>
					  	<div class="col-md-4 selectContainer">
					    	<div class="input-group">
					        	<span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
					    		<select name="country" class="form-control selectpicker">
					      			<option value="${user.country}" selected hidden>${user.country}</option>
					      			<c:forEach var="CountryList" items="${countryList}">
	                					<option value="${CountryList.country}" >${CountryList.country}</option>
	            		  			</c:forEach>
					    		</select>
					  		</div>
						</div>
					</div>
					
					<div class="form-group">
					  <label class="col-md-4 control-label" >Irányítószám</label> 
					  <div class="col-md-4 inputGroupContainer">
					    <div class="input-group">
					  		<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
					  		<input name="zip_code" placeholder="1102" class="form-control"  type="text" value="${user.zipCode}">
					    </div>
					  </div>
					</div>
					
					<div class="form-group">
					  <label class="col-md-4 control-label" >Város</label> 
					  <div class="col-md-4 inputGroupContainer">
					    <div class="input-group">
					  		<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
					  		<input name="city" placeholder="Budapest" class="form-control"  type="text"  value="${user.city}">
					    </div>
					  </div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label" >Cím</label> 
					    <div class="col-md-4 inputGroupContainer">
					    	<div class="input-group">
					  			<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
					  			<input name="address" placeholder="Kossuth utca 12" class="form-control"  type="text"  value="${user.address}">
					    	</div>
					  	</div>
					</div>
						
					
					<center><h3>Munka kereséssel kapcsolatos adatok</h3></center>	
						
					<div class="form-group">
					  <label class="col-md-4 control-label">Fizetés (Óra/HUF)</label>  
					  <div class="col-md-4 inputGroupContainer">
					    <div class="input-group">
					        <span class="input-group-addon"><i class="glyphicon glyphicon-euro"></i></span>
					  		<input name="hourly_wage" placeholder="1200" class="form-control" type="text"  value="${user.preferedHourlyWage}" data-format="#">
					    </div>
					  </div>
					</div>	
					
					<div class="form-group">
					  <label class="col-md-4 control-label">Preferált város</label>  
					  <div class="col-md-4 inputGroupContainer">
					    <div class="input-group">
					        <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
					        <c:if test="${empty user.preferedWorkPlace}">
								<input  value="Nincs megadva" class="form-control"  type="text" readonly>
							</c:if>
					        <c:if test="${!empty user.preferedWorkPlace}">
								<select disabled name="city_list" class="form-control selectpicker" multiple>
					      			<c:forEach var="CityList" items="${user.preferedWorkPlace}">
	                					<option value="${CityList}" >${CityList}</option>
	            		  			</c:forEach>
					    	</select>
							</c:if>   
					  		<select name="city_list" class="form-control selectpicker" multiple>
					      			<c:forEach var="CityList" items="${cityList}">
	                					<option value="${CityList.city}" >${CityList.city}</option>
	            		  			</c:forEach>
					    	</select>
					    </div>
					  </div>
					</div>	
					
					<div class="form-group">
					  <label class="col-md-4 control-label">Skillek</label>  
					  <div class="col-md-4 inputGroupContainer">
					    <div class="input-group">
					        <c:if test="${empty user.skills}">
					        	<span class="input-group-addon"><i class="glyphicon glyphicon-education"></i></span>
								<input  value="Nincs megadva" class="form-control"  type="text" readonly>
							</c:if>
					        <c:if test="${!empty user.skills}">
					        	<div class="input-group">
									<table class="table table-bordered table-striped table-highlight table-responsive">
										<c:forEach var="skillMap" items="${user.skills}">
			                				<tr>
			                					<td><c:out value="${skillMap['key']}"/></td>
			                					<td><c:out value="${skillMap['value']}"/></td>
			                				</tr>
			            		  		</c:forEach>
									</table>
								</div>
							</c:if>   
					    </div>
					  </div>
					</div>	
					
					<div class="form-group">
					  <label class="col-md-4">Preferált munkaidő</label>  
										    
					        <c:if test="${!empty user.getFreeTime()}">
					        	
									<table class="table table-bordered table-striped table-highlight table-responsive">
									
										<tr>
											<c:forEach var="freeTime" items="${user.getFreeTime()}">
													<c:if test="${freeTime.daysName == 'Hétfő'}">
					
													<td></td>
					                				<c:forEach var="freeTime" items="${freeTime.hours}">
					                					<td>${freeTime['key']}</td>
					                				</c:forEach>
					
													</c:if>
													                				
					            		  	</c:forEach>
										</tr>
										
										<tr>
											<c:forEach var="freeTime0" items="${user.getFreeTime()}">
													<c:if test="${freeTime0.daysName == 'Hétfő'}">
					
													<td>${freeTime0.daysName}</td>
					                				<c:forEach var="freeTime" items="${freeTime0.hours}">
					                					<td><input type="checkbox" name="${freeTime0.daysName}${freeTime['key']}" <c:if test="${freeTime['value']}">checked</c:if>></td>
					                				</c:forEach>
					
													</c:if>
													                				
					            		  	</c:forEach>
										</tr>
										
										
										<tr>
											<c:forEach var="freeTime0" items="${user.getFreeTime()}">
													<c:if test="${freeTime0.daysName == 'Kedd'}">
					
													<td>${freeTime0.daysName}</td>
					                				<c:forEach var="freeTime" items="${freeTime0.hours}">
					                					<td><input type="checkbox" name="${freeTime0.daysName}${freeTime['key']}"  <c:if test="${freeTime['value']}">checked</c:if>></td>
					                				</c:forEach>
					
													</c:if>
													                				
					            		  	</c:forEach>
										</tr>
										
										<tr>
											<c:forEach var="freeTime0" items="${user.getFreeTime()}">
													<c:if test="${freeTime0.daysName == 'Szerda'}">
					
													<td>${freeTime0.daysName}</td>
					                				<c:forEach var="freeTime" items="${freeTime0.hours}">
					                					<td><input type="checkbox" name="${freeTime0.daysName}${freeTime['key']}" <c:if test="${freeTime['value']}">checked</c:if>></td>
					                				</c:forEach>
					
													</c:if>
													                				
					            		  	</c:forEach>
										</tr>
										
										<tr>
											<c:forEach var="freeTime0" items="${user.getFreeTime()}">
													<c:if test="${freeTime0.daysName == 'Csütörtök'}">
					
													<td>${freeTime0.daysName}</td>
					                				<c:forEach var="freeTime" items="${freeTime0.hours}">
					                					<td><input type="checkbox" name="${freeTime0.daysName}${freeTime['key']}" <c:if test="${freeTime['value']}">checked</c:if>></td>
					                				</c:forEach>
					
													</c:if>
													                				
					            		  	</c:forEach>
										</tr>
										
										<tr>
											<c:forEach var="freeTime0" items="${user.getFreeTime()}">
													<c:if test="${freeTime0.daysName == 'Péntek'}">
					
													<td>${freeTime0.daysName}</td>
					                				<c:forEach var="freeTime" items="${freeTime0.hours}">
					                					<td><input type="checkbox" name="${freeTime0.daysName}${freeTime['key']}"  <c:if test="${freeTime['value']}">checked</c:if>></td>
					                				</c:forEach>
					
													</c:if>
													                				
					            		  	</c:forEach>
										</tr>
										
										<tr>
											<c:forEach var="freeTime0" items="${user.getFreeTime()}">
													<c:if test="${freeTime0.daysName == 'Szombat'}">
					
													<td>${freeTime0.daysName}</td>
					                				<c:forEach var="freeTime" items="${freeTime0.hours}">
					                					<td><input type="checkbox" name="${freeTime0.daysName}${freeTime['key']}" <c:if test="${freeTime['value']}">checked</c:if>></td>
					                				</c:forEach>
					
													</c:if>
													                				
					            		  	</c:forEach>
										</tr>
										
										<tr>
											<c:forEach var="freeTime0" items="${user.getFreeTime()}">
													<c:if test="${freeTime0.daysName == 'Vasárnap'}">
					
													<td>${freeTime0.daysName}</td>
					                				<c:forEach var="freeTime" items="${freeTime0.hours}">
					                					<td><input type="checkbox" name="${freeTime0.daysName}${freeTime['key']}" <c:if test="${freeTime['value']}">checked</c:if>></td>
					                				</c:forEach>
					
													</c:if>
													                				
					            		  	</c:forEach>
										</tr>								
									
									</table>
								
							</c:if>   
					   
					</div>	
						
					
					<br />
					
					<!-- Button -->
					<div class="form-group">
					  <label class="col-md-4 control-label"></label>
					  <div class="col-md-4"><br>
					    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button type="submit" class="btn btn-info" id="sub">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspAdatok módosítása <span class="glyphicon glyphicon-send"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
					  </div>
					</div>
				
				</fieldset>
			</form>
					
					
	
			<form class="well form-horizontal" action="UploadFileServlet" method="post" enctype="multipart/form-data" id="profile_picture_upload">
			  <div class="form-group">
			  	<label class="col-md-4 control-label">Profilkép (JPEG/PNG)</label> 
				<div class="col-md-4 inputGroupContainer">  
				  <label for="fileupload" id="buttonlabel">
				    <span role="button" aria-controls="filename" tabindex="0">Fáj feltöltése</span>
				  </label>
				  <input type="file" id="fileupload" name="profilePic" accept="image/x-png,image/gif,image/jpeg">
				  <label for="filename" class="hide">Feltöltendő fájl</label>
				  <input type="text" id="filename" class="fileinput" autocomplete="off" readonly placeholder="Még nincs fájl kiválasztva" name="profile_pciture_input">  
				  <button type="submit" class="uploadfile">Feltöltés</button>
				  <c:if test="${!empty user.profilePicture}">
		        	<br>
		        	<h6>${user.profilePicture}</h6>
		          </c:if>	
				</div>
			  </div>
			</form>		
					
					
			<form class="well form-horizontal" action="UploadFileServlet" method="post" enctype="multipart/form-data">
			  <div class="form-group">
			  	<label class="col-md-4 control-label">Önéletrajz (PDF)</label> 
				<div class="col-md-4 inputGroupContainer">  
				  <label for="fileupload2" id="buttonlabel">
				    <span role="button" aria-controls="filename" tabindex="0">Fáj feltöltése</span>
				  </label>
				  <input type="file" id="fileupload2" name="CV" accept=".pdf">
				  <label for="filename" class="hide">Feltöltendő fájl</label>
				  <input type="text" id="filename2" class="fileinput" autocomplete="off" readonly placeholder="Még nincs fájl kiválasztva">  
				  <button type="submit" class="uploadfile">Feltöltés</button>
				  <c:if test="${!empty user.cv}">
		        	<br>
		        	<h6>${user.cv}</h6>
		          </c:if>	
				</div>
			  </div>
			</form>
			
			<form class="well form-horizontal" action="UploadFileServlet" method="post">
			  <div class="form-group">
			  	<label class="col-md-4 control-label">Skill</label> 
				<div class="col-md-4 inputGroupContainer">  
				
				  <select name="skill_list" class="form-control selectpicker">
		      			<c:forEach var="SkillList" items="${skillList}">
           					<option value="${SkillList.skillName}" >${SkillList.skillName}</option>
      		  			</c:forEach>
				  </select>
				  
				  <select name="skill_point" class="form-control selectpicker">
		      			<option value="1" >1</option>
		      			<option value="2" >2</option>
		      			<option value="3" >3</option>
		      			<option value="4" >4</option>
		      			<option value="5" >5</option>	
				  </select>
				  
				  <button  class="form-control" name="skill-button" value="skills">Mentés</button>
				
				</div>
			  </div>
			</form>						
					
	

		
		</div>
		
	</body>
	
</html>