<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Munkák</title>
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<link rel="stylesheet" href="/StudentWorkRental/css/student/searchJob.css" />
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
				<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>	

		<script type="text/javascript" charset="utf-8" src = "/StudentWorkRental/javascript/student/studentNavigation.js"></script>
		<script type="text/javascript" charset="utf-8" src = "/StudentWorkRental/javascript/student/studentSearchJob.js"></script>
		
	</head>
	
	
	<body>
	
		<div id="nav"></div>
		
		<c:if test="${empty companyList}">

			<div class="alert alert-warning" role="alert">
				Nincs Ön által értékelhető cég!
			</div>

		</c:if>

		<c:if test="${!empty companyList}">
			<div class="container">
				
				<form action="/StudentWorkRental/RateCompanyServlet" method="post"  id="rate_company_form">
					
					<h3><u>Válaszd ki melyik céget szeretnéd értékelni:</u></h3>
					<select class="form-control" name="companies">                             
		                <c:forEach var="companyList" items="${companyList}">
							<option value="${companyList.id}" >${companyList.companyName}</option>
		    	   		</c:forEach>
		            </select>
		            
		            <h3><u>Kérlek add meg az értékelésed:</u></h3>
					<div class="form-check form-check-inline">
					  <input class="form-check-input" type="radio" id="inlineCheckbox" value="1" name="companyrate">
					  <label class="form-check-label" for="inlineCheckbox1">1</label>
					  <input class="form-check-input" type="radio" id="inlineCheckbox" value="2" name="companyrate">
					  <label class="form-check-label" for="inlineCheckbox2">2</label>
					  <input class="form-check-input" type="radio" id="inlineCheckbox" value="3" name="companyrate">
					  <label class="form-check-label" for="inlineCheckbox3">3</label>
					  <input class="form-check-input" type="radio" id="inlineCheckbox" value="4" name="companyrate">
					  <label class="form-check-label" for="inlineCheckbox4">4</label>
					  <input class="form-check-input" type="radio" id="inlineCheckbox" value="5" name="companyrate">
					  <label class="form-check-label" for="inlineCheckbox5">5</label>
					</div>
	
		            
		            <h3><u>Amennyiben szeretnél szöveges értékelést is adhatsz:</u></h3>
					<textarea class="form-control" name="ratingDescriprion" rows=10 style="min-width: 100%"></textarea>
					
					<br>
					<button type="submit" class="btn btn-secondary">Értékelés</button>
								
	            </form>
			
			
			
			</div>
		</c:if>

	</body>
	
	
</html>