<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Szerződéseim</title>
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>	

	
	</head>



	<body>
	
		<h2>Kérlek mielőtt belépnél töltsd ki az angol tesztet!</h2>
		<form action="/StudentWorkRental/QuizServlet" method="post">
			<table class="table table-striped table-responsive">
				<c:forEach var="quizList" items="${englishQuiz}">
					<tr>
						<td colspan=4><b>${quizList.question.question}</b></td>
					</tr>
					<tr>	
						<c:forEach var="quizListAnswer" items="${quizList.answers}">
						 	<td>
							  <input class="form-check-input" type="checkbox" name="${quizList.question.id}" value="${quizListAnswer.id}">
							  <label class="form-check-label" for="inlineCheckbox1">${quizListAnswer.answer}</label>
							
						    </td>         				
		 				</c:forEach>
		
					</tr>	        
					     				
			    </c:forEach>
		    </table>
		    <button type="submit" class="btn btn-secondary">Elküld</button>
		 </form>
	
	
	</body>
	
	
	
</html>