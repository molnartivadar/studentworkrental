<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Munkák</title>
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<link rel="stylesheet" href="/StudentWorkRental/css/student/searchJob.css" />
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
				<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>	

		<script type="text/javascript" charset="utf-8" src = "/StudentWorkRental/javascript/student/studentNavigation.js"></script>
		<script type="text/javascript" charset="utf-8" src = "/StudentWorkRental/javascript/student/studentSearchJob.js"></script>
		
	</head>
	
	<body>
	
		<div id="nav"></div>
		
		<form action="" method="post"  id="search_form">
			<fieldset>
				
				<div class="form-group inputGroupContainer search">
				  		<input type="text" class="form-control" placeholder="Munka neve" name="jobname">
				  		<input type="text" class="form-control" placeholder="Fizetés Ft/Óra" name="hourlywage">
				  		<input type="text" class="form-control" placeholder="Heti munkaidő" name="worktime">
				  		<select class="form-control" id="exampleFormControlSelect1" multiple name="skills">
                               <c:forEach var="skillList" items="${skillList}">
              						<option value="${skillList.skillName}" >${skillList.skillName}</option>
          		  				</c:forEach>
	                    </select>
                        <select class="form-control" id="multiselect" multiple="multiple" name="workplaces">                             
                               <c:forEach var="cityList" items="${cityList}">
             						<option value="${cityList.city}" >${cityList.city}</option>
         		  	     	   </c:forEach>
                        </select>
                        <br>    
                        <button type="submit" class="btn btn-secondary" id="sub">Keresés</button>       
				</div>
				<br>
				
			</fieldset>
		</form>
		
	
		<div>
		    <table class="table jobs">
		      <thead class="thead-light">
		        <tr>
		          <th scope="col">Munka</th>
		          <th scope="col">Munka leírás</th>
		          <th scope="col">Szükséges képesítések</th>
		          <th scope="col">Heti munkaidő</th>
		          <th scope="col">Fizetés</th>
		          <th scope="col">Munkavégzés helye</th>
		          <th scope="col">Cég</th>
		        </tr>
		      </thead>
		      <tbody>
		           	<c:forEach var="jobList" items="${jobList}">
					<tr>
					  <td><a href="/StudentWorkRental/JobServlet?id=${jobList.id}" target="_blank">${jobList.jobName}</a></td>
			          <td>${jobList.jobDescription}</td>
			          <td>
			          	<c:forEach var="skillList" items="${jobList.skills}" varStatus="loop">
				             	<c:out value = "${skillList}"/><c:if test="${!loop.last}">,</c:if> 
				        </c:forEach>
			          </td>
			          <td>${jobList.hoursOfWorkInOneWeek} óra</td>
			          <td><fmt:formatNumber value="${jobList.hourlyWage}" maxFractionDigits="0" /> Ft/Óra</td>
			          <td>${jobList.workPlace}</td>
			          <td><a href="/StudentWorkRental/MyProfileServlet?id=${jobList.company.id}" target="_blank">${jobList.company.companyName}</a></td>			
					</tr>																			                				
				</c:forEach>
		      </tbody>
		    </table>
  		</div>
		
	</body>
</html>