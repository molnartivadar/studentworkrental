<%@page import="com.bh08.ajfk.models.Days"%>
<%@page import="java.util.List"%>
<%@page import="com.bh08.ajfk.models.Student"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

<head>

  <meta charset="UTF-8">
  <title>Insert title here</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="/StudentWorkRental/css/student/studentNews.css" />
  <script type="text/javascript" charset="utf-8" src = "/StudentWorkRental/javascript/student/studentNavigation.js"></script>

 

</head>

<body>

  <div id="nav"></div>

  <main role="main">

    <div class="jumbotron">
      <div class="container">
        <h1 class="display-3"><c:out value="${news[3].heading}" /></h1>
        <p><c:out value="${news[3].text}" /></p>
      </div>
    </div>

    <div class="container">

      <div class="row">
        <div class="col-md-4">
          <h3><c:out value="${news[0].heading}" /></h3>
          <p><c:out value="${news[0].text}" /></p>
        </div>
        <div class="col-md-4">
          <h3><c:out value="${news[1].heading}" /></h3>
          <p><c:out value="${news[1].text}" /></p>
        </div>
        <div class="col-md-4">
          <h3><c:out value="${news[2].heading}" /></h3>
          <p><c:out value="${news[2].text}" /></p>
        </div>

      </div>

      <hr>

    </div>

  </main>
  <div class="container">
    <h2>Ajánlott munkák</h2>
    <table width="80%" class="table jobs">
      <thead class="thead-light">
        <tr>
          <th scope="col">Munka</th>
          <th scope="col">Munka leírás</th>
          <th scope="col">Szükséges képesítések</th>
          <th scope="col">Heti munkaidő</th>
          <th scope="col">Fizetés</th>
          <th scope="col">Munkavégzés helye</th>
          <th scope="col">Cég</th>
        </tr>
      </thead>
      <tbody>
           	<c:forEach var="jobList" items="${jobList}">
			<tr>
			  <td><a href="/StudentWorkRental/JobServlet?id=${jobList.id}" target="_blank">${jobList.jobName}</a></td>
	          <td>${jobList.jobDescription}</td>
	          <td>
	          	<c:forEach var="skillList" items="${jobList.skills}" varStatus="loop">
		             	<c:out value = "${skillList}"/><c:if test="${!loop.last}">,</c:if> 
		        </c:forEach>
	          </td>
	          <td>${jobList.hoursOfWorkInOneWeek} óra</td>
	          <td><fmt:formatNumber value="${jobList.hourlyWage}" maxFractionDigits="0" /> Ft/Óra</td>
	          <td>${jobList.workPlace}</td>
	          <td><a href="/StudentWorkRental/MyProfileServlet?id=${jobList.company.id}" target="_blank">${jobList.company.companyName}</a></td>			
			</tr>																			                				
		</c:forEach>
      </tbody>
    </table>
  </div>
  
  <footer class="container">
    <p>&copy; AJFK 2019</p>
  </footer>

</body>

</html>