<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
	
	<head>
	
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Diák profil</title>
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<link rel="stylesheet" href="/StudentWorkRental/css/student/studentProfile.css" />
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>

	</head>


	<body>
	
		<div class="container">
		  <div class="row">
		    <div class="col-md-6 img">	    	
		    	<img src="data:image/png;base64,${picture}" id="profile-img" class="profile-img-card"/>  
		    </div>
		    <div class="col-md-6 details main">
		      <blockquote>
		        <h4>${selectedUser.lastName} ${selectedUser.firstName}</h4>
		        <small><cite title="Source Title">${selectedUser.country}, ${selectedUser.city} <i class="icon-map-marker"></i></cite></small>
		      </blockquote>
		      <p>
		        <u>Email:</u> ${selectedUser.EMail} <br>
		        <u>Telefon:</u> ${selectedUser.phoneNumber} <br>
		        <u>Születési idő:</u> ${selectedUser.birthDate} <br>
		        <c:if test="${empty selectedUser.cv}">
		        	<u>Önéletrajz:</u> Nincs feltöltve!
		        </c:if>
		        <c:if test="${!empty selectedUser.cv}">
		        	<u>Önéletrajz:</u> <a href="/StudentWorkRental/DownloadServlet?file=${selectedUser.cv}">Letöltés</a>
		        </c:if>		        
		      </p>
		    </div>
		  </div>
		  
		  <br><br>
			
	    	<c:if test="${empty selectedUser.preferedWorkPlace}">
		    	<div class="work">
					<p><u>Preferált munkahely:</u> Nincs megadva!</p>
				</div>
			</c:if> 
			
			<c:if test="${!empty selectedUser.preferedWorkPlace}">
		   		<div class="work">
					<p><u>Preferált munkahely:</u>
			      		<c:forEach var="CityList" items="${selectedUser.preferedWorkPlace}" varStatus="loop">
		             		<c:out value = "${CityList}"/> <c:if test="${!loop.last}">, </c:if>
		         		</c:forEach>
		         	</p>
         		</div>	    
			</c:if>  
			
			
			
			
			<c:if test="${empty selectedUser.preferedHourlyWage}">
		    	<div class="work">
					<p><u>Preferált órabér:</u> Nincs megadva!</p>
				</div>
			</c:if> 
			
			<c:if test="${!empty selectedUser.preferedHourlyWage}">
		   		<div class="work">
					<p><u>Preferált Órabér:</u>
						<fmt:formatNumber value="${selectedUser.preferedHourlyWage}" maxFractionDigits="0" /> Ft/Óra
		         	</p>
         		</div>	    
			</c:if>  
			
			
			
			
			<c:if test="${empty selectedUser.skills}">
	        	<div class="col-md-6 img">
					<p><u>Skillek: </u> Nincs megadva!</p>
				</div>
			</c:if>
	        					        
	        <c:if test="${!empty selectedUser.skills}">
	        	<div class="col-md-6 img">
	        		<u>Skillek:</u> <br>
					<table class="table table-bordered table-striped table-highlight table-responsive">
						<c:forEach var="skillMap" items="${selectedUser.skills}">
               				<tr>
               					<td><c:out value="${skillMap['key']}"/></td>
               					<td><c:out value="${skillMap['value']}"/></td>
               				</tr>
           		  		</c:forEach>
					</table>
				</div>
			</c:if>   
			
			
			
			<c:if test="${empty selectedUser.rank}">
	        	<div class="col-md-6 img">
					<p><u>Értékelés cégek által: </u> Még nincs értékelés!</p>
				</div>
			</c:if>
			
			 <c:if test="${!empty selectedUser.rank}">
	        	<div class="col-md-6 details">
	        		<u>Értékelés cégek által: </u> <br>
					<table class="table table-bordered table-striped table-highlight table-responsive">
						<c:forEach var="rankMap" items="${selectedUser.rank}">
               				<tr>
               					<td><c:out value="${rankMap['key']}"/></td>
               					<td><c:out value="${rankMap['value']}"/></td>
               				</tr>
           		  		</c:forEach>
					</table>
				</div>
			</c:if>   
			
			
			
			 <c:if test="${!empty selectedUser.getFreeTime()}">
					<div class="work">  			     	
						<table class="table table-bordered table-striped table-highlight table-responsive">
							<caption><u>Preferált munkaidő: </u></caption>
							<tr>
								<c:forEach var="freeTime" items="${selectedUser.getFreeTime()}">
										<c:if test="${freeTime.daysName == 'Hétfő'}">
		
										<td ></td>
		                				<c:forEach var="freeTime" items="${freeTime.hours}">
		                					<th>${freeTime['key']}</th>
		                				</c:forEach>
		
										</c:if>
										                				
		            		  	</c:forEach>
							</tr>
							
							<tr>
								<c:forEach var="freeTime0" items="${selectedUser.getFreeTime()}">
										<c:if test="${freeTime0.daysName == 'Hétfő'}">
		
										<th>${freeTime0.daysName}</th>
		                				<c:forEach var="freeTime" items="${freeTime0.hours}">
		                					<td <c:if test="${freeTime['value']}">bgcolor="#00cc00"</c:if> <c:if test="${!freeTime['value']}">bgcolor="#ff8080"</c:if>></td>
		                				</c:forEach>
		
										</c:if>
										                				
		            		  	</c:forEach>
							</tr>
							
							
							<tr>
								<c:forEach var="freeTime0" items="${selectedUser.getFreeTime()}">
										<c:if test="${freeTime0.daysName == 'Kedd'}">
		
										<th>${freeTime0.daysName}</th>
		                				<c:forEach var="freeTime" items="${freeTime0.hours}">
		                					<td <c:if test="${freeTime['value']}">bgcolor="#00cc00"</c:if> <c:if test="${!freeTime['value']}">bgcolor="#ff8080"</c:if>></td>
		                				</c:forEach>
		
										</c:if>
										                				
		            		  	</c:forEach>
							</tr>
							
							<tr>
								<c:forEach var="freeTime0" items="${selectedUser.getFreeTime()}">
										<c:if test="${freeTime0.daysName == 'Szerda'}">
		
										<th>${freeTime0.daysName}</th>
		                				<c:forEach var="freeTime" items="${freeTime0.hours}">
		                					<td <c:if test="${freeTime['value']}">bgcolor="#00cc00"</c:if> <c:if test="${!freeTime['value']}">bgcolor="#ff8080"</c:if>></td>
		                				</c:forEach>
		
										</c:if>
										                				
		            		  	</c:forEach>
							</tr>
							
							<tr>
								<c:forEach var="freeTime0" items="${selectedUser.getFreeTime()}">
										<c:if test="${freeTime0.daysName == 'Csütörtök'}">
		
										<th>${freeTime0.daysName}</th>
		                				<c:forEach var="freeTime" items="${freeTime0.hours}">
		                					<td <c:if test="${freeTime['value']}">bgcolor="#00cc00"</c:if> <c:if test="${!freeTime['value']}">bgcolor="#ff8080"</c:if>></td>
		                				</c:forEach>
		
										</c:if>
										                				
		            		  	</c:forEach>
							</tr>
							
							<tr>
								<c:forEach var="freeTime0" items="${selectedUser.getFreeTime()}">
										<c:if test="${freeTime0.daysName == 'Péntek'}">
		
										<th>${freeTime0.daysName}</th>
		                				<c:forEach var="freeTime" items="${freeTime0.hours}">
		                					<td <c:if test="${freeTime['value']}">bgcolor="#00cc00"</c:if> <c:if test="${!freeTime['value']}">bgcolor="#ff8080"</c:if>></td>
		                				</c:forEach>
		
										</c:if>
										                				
		            		  	</c:forEach>
							</tr>
							
							<tr>
								<c:forEach var="freeTime0" items="${selectedUser.getFreeTime()}">
										<c:if test="${freeTime0.daysName == 'Szombat'}">
		
										<th>${freeTime0.daysName}</th>
		                				<c:forEach var="freeTime" items="${freeTime0.hours}">
		                					<td <c:if test="${freeTime['value']}">bgcolor="#00cc00"</c:if> <c:if test="${!freeTime['value']}">bgcolor="#ff8080"</c:if>></td>
		                				</c:forEach>
		
										</c:if>
										                				
		            		  	</c:forEach>
							</tr>
							
							<tr>
								<c:forEach var="freeTime0" items="${selectedUser.getFreeTime()}">
										<c:if test="${freeTime0.daysName == 'Vasárnap'}">
		
										<th>${freeTime0.daysName}</th>
		                				<c:forEach var="freeTime" items="${freeTime0.hours}">
		                					<td <c:if test="${freeTime['value']}">bgcolor="#00cc00"</c:if> <c:if test="${!freeTime['value']}">bgcolor="#ff8080"</c:if>></td>
		                				</c:forEach>
		
										</c:if>
										                				
		            		  	</c:forEach>
							</tr>								
						
						</table>
					</div>
				</c:if>   
							
		    </div>
		    
	</body>

</html>