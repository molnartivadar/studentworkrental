<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>

	<head>
	
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Regisztráció</title>
		
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
		<script type="text/javascript" charset="utf-8" src = "/StudentWorkRental/javascript/student/studentRegister.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>

		<link rel="stylesheet" href="/StudentWorkRental/css/student/studentRegister.css" />
				
	</head>

	<body>	
	
		<div class="container">
		
			<c:set var="context" value="${pageContext.request.contextPath}" />
			<form class="well form-horizontal" action="" method="post"  id="contact_form">
				<fieldset>
				
					<legend><center><h2><b>Regisztráció</b></h2></center></legend>
					
					<center><h3>Regisztrációs adatok</h3></center>
					
					<div class="form-group">
					  <label class="col-md-4 control-label">E-Mail cím</label>  
					  <div class="col-md-4 inputGroupContainer">
					  	<div class="input-group">
					       	<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
					  		<input name="email" placeholder="valami@valami.hu" class="form-control"  type="text">
					   	</div>
					  </div>
					</div>
					
					<div class="form-group">
					  <label class="col-md-4 control-label" >Jelszó</label> 
					  <div class="col-md-4 inputGroupContainer">
					   	<div class="input-group">
					  		<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
					 		<input name="user_password" placeholder="Jelszó" class="form-control"  type="password">
					   	</div>
					  </div>
					</div>
					
					<div class="form-group">
					  <label class="col-md-4 control-label" >Jelszó megerősítése</label> 
					  <div class="col-md-4 inputGroupContainer">
					    <div class="input-group">
					  		<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
					  		<input name="confirm_password" placeholder="Jelszó megerősítése" class="form-control"  type="password">
					    </div>
					  </div>
					</div>
					
					<center><h3>Személyes adatok</h3></center>
					
					<div class="form-group">
					  <label class="col-md-4 control-label" >Vezetéknév</label> 
					  <div class="col-md-4 inputGroupContainer">
					    <div class="input-group">
					  		<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
					  		<input name="last_name" placeholder="Vezetéknév" class="form-control"  type="text">
					    </div>
					  </div>
					</div>
					
					<div class="form-group">
					  <label class="col-md-4 control-label">Keresztnév</label>  
					  <div class="col-md-4 inputGroupContainer">
					  	<div class="input-group">
					  		<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
					  		<input  name="first_name" placeholder="Keresztnév" class="form-control"  type="text">
					    </div>
					  </div>
					</div>
					
					<div class="form-group"> 
					  <label class="col-md-4 control-label">Neme</label>
					    <div class="col-md-4 selectContainer">
					   		<div class="input-group">
					        	<span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
					    		<select name="gender" class="form-control selectpicker">
					      			 <option value="" selected disabled hidden>Kérjük válassz</option>
					      			<c:forEach var="GenderList" items="${genderList}">
	                					<option value="${GenderList}" >${GenderList}</option>
	            		  			</c:forEach>
					    		</select>
					  		</div>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label" >Anyja neve</label> 
						<div class="col-md-4 inputGroupContainer">
						   	<div class="input-group">
						 		 <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
						 		 <input name="mother_name" placeholder="Anyja neve" class="form-control"  type="text">
						   	</div>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label" >Születési hely</label> 
						<div class="col-md-4 inputGroupContainer">
						   	<div class="input-group">
						 		 <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
						 		 <input name="birth_place" placeholder="Születési hely" class="form-control"  type="text">
						   	</div>
						</div>
					</div>
						
					<div class="form-group">
						<label class="col-md-4 control-label" >Születési idő</label> 
						 <div class="col-md-4 inputGroupContainer">
						    <div class="input-group">
						    	<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
		        				<!-- <input class="form-control" id="date" name="birth_date" placeholder="ÉÉÉÉ/HH/NN" type="text"/>-->
		        				<input name="birth_date" type="date" class="datepicker form-control" id="datePicker" max="2019-02-27">
						    </div>
						 </div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label" >Adóazonosító</label> 
						<div class="col-md-4 inputGroupContainer">
						   	<div class="input-group">
						 		 <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
						 		 <input name="tax_num" placeholder="7898458796" class="form-control"  type="text">
						   	</div>
						</div>
					</div>
								
					<center><h3>Kontakt adatok</h3></center>
					
					<div class="form-group">
					  <label class="col-md-4 control-label">Telefonszám</label>  
					  <div class="col-md-4 inputGroupContainer">
					    <div class="input-group">
					        <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
					  		<input name="contact_no" placeholder="36305806303" class="form-control" type="text">
					    </div>
					  </div>
					</div>
					
					<div class="form-group"> 
						<label class="col-md-4 control-label">Ország</label>
					  	<div class="col-md-4 selectContainer">
					    	<div class="input-group">
					        	<span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
					    		<select name="country" class="form-control selectpicker">
					      			<option value="" selected disabled hidden>Kérjük válasszon</option>
					      			<c:forEach var="CountryList" items="${countryList}">
	                					<option value="${CountryList.country}" >${CountryList.country}</option>
	            		  			</c:forEach>
					    		</select>
					  		</div>
						</div>
					</div>
					
					<div class="form-group">
					  <label class="col-md-4 control-label" >Irányítószám</label> 
					  <div class="col-md-4 inputGroupContainer">
					    <div class="input-group">
					  		<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
					  		<input name="zip_code" placeholder="1102" class="form-control"  type="text">
					    </div>
					  </div>
					</div>
					
					<div class="form-group">
					  <label class="col-md-4 control-label" >Város</label> 
					  <div class="col-md-4 inputGroupContainer">
					    <div class="input-group">
					  		<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
					  		<input name="city" placeholder="Budapest" class="form-control"  type="text">
					    </div>
					  </div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label" >Cím</label> 
					    <div class="col-md-4 inputGroupContainer">
					    	<div class="input-group">
					  			<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
					  			<input name="address" placeholder="Kossuth utca 12" class="form-control"  type="text">
					    	</div>
					  	</div>
					</div>
					
					<br />
					
					<div class="form-group">
						<label class="col-md-4 control-label" ></label> 
					    <div class="col-md-4 inputGroupContainer">
					    	<div class="input-group">
					  			<input class="form-check-input" type="checkbox" value="true" id="gdpr" name="gdpr"> Regisztrációjával elfogadja, a <a href="/StudentWorkRental/static/GDPR.pdf" target="_blank">GDPR</a> nyilatkozatot!
					    	</div>
					  	</div>
					</div>
					
					<!-- Button -->
					<div class="form-group">
					  <label class="col-md-4 control-label"></label>
					  <div class="col-md-4"><br>
					    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button type="submit" class="btn btn-info" id="sub">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspRegisztrálok <span class="glyphicon glyphicon-send"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
					  </div>
					</div>
				
				</fieldset>
			</form>
			
		</div>
		
	</body>
	
</html>