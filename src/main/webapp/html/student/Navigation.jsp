<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<nav class="navbar navbar-inverse">
		    <div class="container-fluid">
		      <div class="navbar-header">
		        <a class="navbar-brand" href="/StudentWorkRental/NewsServlet">AJFK</a>
		      </div>
		      <ul class="nav navbar-nav">
		        <li><a href="/StudentWorkRental/SearchJobServlet">Munka keresés</a></li>
		        <li><a href="/StudentWorkRental/MyContractServlet">Korábbi/aktuális munkáim</a></li>
		        <li><a href="/StudentWorkRental/MyProfileServlet?id=${user.id}" target="_blank">Profilom</a></li>
		        <li><a href="/StudentWorkRental/RateCompanyServlet">Cégértékelés</a></li>
		      </ul>
		      <ul class="nav navbar-nav navbar-right">
		        <li><a href="/StudentWorkRental/PersonalDataServlet"><span class="glyphicon glyphicon-user"></span> Személyes adatok</a></li>
		        <li><a href="/StudentWorkRental/LoginServlet?reset=true"><span class="glyphicon glyphicon-log-in"></span>Kijelentkezés</a></li>
		      </ul>
		    </div>
 	 	</nav>	
</body>
</html>