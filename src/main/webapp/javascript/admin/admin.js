function showSubmitButton() {
	if (document.getElementById("lastname").value.length > 0
			&& document.getElementById("forname").value.length > 0
			&& document.getElementById("email").value.length > 0
			&& document.getElementById("password1").value.length > 0
			&& document.getElementById("password2").value.length > 0
			&& document.getElementById("country").value.length > 0
			&& document.getElementById("zipcode").value.length > 0
			&& document.getElementById("city").value.length > 0
			&& document.getElementById("address").value.length > 0
			&& document.getElementById("phonenumber").value.length > 0) {
		document.getElementById("submit").style.visibility = "visible";
	} else {
		document.getElementById("submit").style.visibility = "hidden";
	}
}

function showResetButton() {
	if (document.getElementById("lastname").value.length > 0
			|| document.getElementById("forname").value.length > 0
			|| document.getElementById("email").value.length > 0
			|| document.getElementById("password1").value.length > 0
			|| document.getElementById("password2").value.length > 0
			|| document.getElementById("country").value.length > 0
			|| document.getElementById("zipcode").value.length > 0
			|| document.getElementById("city").value.length > 0
			|| document.getElementById("address").value.length > 0
			|| document.getElementById("phonenumber").value.length > 0
			|| document.getElementById("profilepicture").value.length > 0) {
		document.getElementById("reset").style.visibility = "visible";
	} else {
		document.getElementById("reset").style.visibility = "hidden";
	}
}

function showUpdateButton() {
	document.getElementById("update").style.visibility = "visible";
}

function onClickResetButton() {
	document.getElementById("reset").style.visibility = "hidden";
	document.getElementById("submit").style.visibility = "hidden";
}

function checkPassword() {
	if (document.getElementById("password1").value.length > 0
			&& document.getElementById("password2").value.length > 0) {
		if (document.getElementById("password1").value != document
				.getElementById("password2").value) {
			alert("A jelszavak nem egyeznek!");
			document.getElementById("password1").value = "";
			document.getElementById("password2").value = "";
			showResetButton();
		}
	}
}

function newQuiz() {
	var html = '<table>';
	html += '<tr><td>Skill-típus:</td></tr>';
	html += "<tr><td><input type='text' id='quizSkill' name='quizSkill' value='"
			+ document.getElementById('newQuizSkill').value + "' /></td></tr>";
	html += '<tr><td>Kérdés:</td></tr>';
	html += "<tr><td><input type='text' id='newQuestion' name='newQuestion' /></td>";
	html += '<tr><td>Válaszlehetőségek:</td><td>Helyes válasz</td></tr>';

	for (var i = 0; i < 4; i++) {
		html += "<tr><td><input type='text' id='answer" + (i + 1)
				+ "' name='answer" + (i + 1) + "' /></td>";
		html += "<td><input type='checkbox' name='correctAnswer" + (i + 1)
				+ "' value='correctAnswer" + (i + 1) + "'></td></tr>";
	}
	html += '</table>';

	html += '<br />';
	html += "<input type='submit' class='btn' id='saveNewQuiz' name='btnQuiz' value= 'Save' />";

	var where = document.getElementById("newQuiz");
	where.innerHTML = html;
}

function editQuiz(rowNumber) {
	
    var oTable = document.getElementById('quiztable');
    var rowLength = oTable.rows.length;  

    var oCells = oTable.rows.item(rowNumber).cells;

	var html = "<input type='text' class='quizIdTextfield' id='editedQuizId' name='editedQuizId' value='" + oCells.item(0).innerHTML + "'  /> ";
	html += "<input type='text' class='quizQuestionTextfield' id='editedQuizQuestion' name='editedQuizQuestion' value='" + oCells.item(1).innerHTML + "' /> ";
	html += "<input type='text' class='quizTextfield' id='editedQuizAnswer1' name='editedQuizAnswer1' value='" + oCells.item(2).innerHTML + "' /> ";
	html += "<input type='checkbox' name='editedCorrectAnswer1' id='editedCorrectAnswer1' value='editedCorrectAnswer1' />";
	html += "<input type='text' class='quizTextfield' id='editedQuizAnswer2' name='editedQuizAnswer2' value='" + oCells.item(3).innerHTML + "' /> ";
	html += "<input type='checkbox' name='editedCorrectAnswer2' id='editedCorrectAnswer2' value='editedCorrectAnswer2' />";
	html += "<input type='text' class='quizTextfield' id='editedQuizAnswer3' name='editedQuizAnswer3' value='" + oCells.item(4).innerHTML + "' /> ";
	html += "<input type='checkbox' name='editedCorrectAnswer3' id='editedCorrectAnswer3' value='editedCorrectAnswer3' />";
	html += "<input type='text' class='quizTextfield' id='editedQuizAnswer4' name='editedQuizAnswer4' value='" + oCells.item(5).innerHTML + "' /> ";
	html += "<input type='checkbox' name='editedCorrectAnswer4' id='editedCorrectAnswer4' value='editedCorrectAnswer4' />";
	html += "<input type='submit' class='btn' name='btnQuiz' id='updateQuiz' value='Update' />"
	
	var where = document.getElementById("editQuiz");
	where.innerHTML = html;
	
}

function deleteQuiz() {
	document.quizAdmin.submit();
}

function showNavMenu() {

	var html = "<nav><ul class='left-side'><li><b>AJFK</b></li>";
	html += "<li><a href='/StudentWorkRental/adminServlet?name=mydata'>Adataim</a></li>";
	html += "<li><a href='/StudentWorkRental/adminServlet?name=newadmin'>Új adminisztrátor</a></li>";
	html += "<li><a href='/StudentWorkRental/adminServlet?name=users'>Felhasználók</a></li>";
	html += "<li><a href='/StudentWorkRental/adminServlet?name=quiz'>Kvíz-Skill</a></li>";
	html += "<li><a href='/StudentWorkRental/adminServlet?name=contracts'>Szerződések</a></li>";
	html += "<li><a href='/StudentWorkRental/adminServlet?name=statistics'>Statisztikák</a></li>";
	html += "<li><a href='/StudentWorkRental/adminServlet?name=news'>Hírek</a></li>";
	html += "<li><a href='/StudentWorkRental/LoginServlet?reset=true'>Kilépés</a></li>";
	html += "</ul></nav>";

	var where = document.getElementById("navMenu");
	where.innerHTML = html;
}

function showContractSubmitButton() {
	if (document.getElementById("contractDateTo").value <= document.getElementById("contractDateFrom").value) {
		document.getElementById("contractDateSubmit").style.visibility = "hidden";
	} else {
		document.getElementById("contractDateSubmit").style.visibility = "visible";
	}
}

function showIncomeSubmitButton() {
	if (document.getElementById("incomeDateTo").value <= document.getElementById("incomeDateFrom").value) {
		document.getElementById("incomeSubmit").style.visibility = "hidden";
	} else {
		document.getElementById("incomeSubmit").style.visibility = "visible";
	}
}