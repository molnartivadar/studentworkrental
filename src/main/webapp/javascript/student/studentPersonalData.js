$(document).ready(function() {
	
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear()-14;
	 if(dd<10){
	        dd='0'+dd
	    } 
	    if(mm<10){
	        mm='0'+mm
	    } 

	today = yyyy+'-'+mm+'-'+dd;
	console.log(today);
	var datePicker= document.getElementById("datePicker");
	console.log(datePicker);
	datePicker.setAttribute("max", today);
	
	$('#buttonlabel span[role=button]').bind('keypress keyup', function(e) {
		  if(e.which === 32 || e.which === 13){
		    e.preventDefault();
		    $('#fileupload').click();
		  }    
		});

	$('#fileupload').change(function(e) {
	  var filename = $('#fileupload').val().split('\\').pop();
	  $('#filename').val(filename);
	  $('#filename').attr('placeholder', filename);
	  $('#filename').focus();
	});
	
	
	$('#buttonlabel span[role=button]').bind('keypress keyup', function(e) {
		  if(e.which === 32 || e.which === 13){
		    e.preventDefault();
		    $('#fileupload2').click();
		  }    
		});

	$('#fileupload2').change(function(e) {
	  var filename = $('#fileupload2').val().split('\\').pop();
	  $('#filename2').val(filename);
	  $('#filename2').attr('placeholder', filename);
	  $('#filename2').focus();
	});


	$('.datepicker')
    .on('changeDate show', function(e) {
        $('#contact_form').bootstrapValidator('revalidateField', 'birth_date');
    });
	
    $('#contact_form').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            user_password: {
                validators: {
                     stringLength: {
                        min: 6,
                        message: 'Legalább 6 karakter hosszú jelszót adjon meg!'
                    },
                    identical: {
                        field: 'confirm_password',
                        message: 'A két jelszó nem egyezik!'
                    }
                }
            },
			confirm_password: {
                validators: {
                     stringLength: {
                        min: 6,
                        message: 'Legalább 6 karakter hosszú jelszót adjon meg!'
                    },
                    identical: {
                        field: 'user_password',
                        message: 'A két jelszó nem egyezik!'
                    }
                }
            },
            last_name: {
                validators: {
                    notEmpty: {
                        message: 'Kérjük adja meg a vezetéknevét!'
                    }
                }
            },
            first_name: {
                validators: {
                        notEmpty: {
                        message: 'Kérjük adja meg a keresztnevét!'
                    }
                }
            },
            gender: {
                validators: {
                        notEmpty: {
                        message: 'Kérjük válasszon!'
                    }
                }
            },
            mother_name: {
                validators: {
                        notEmpty: {
                        message: 'Kérjük adja meg az Anyja nevét!'
                    }
                }
            },
            birth_place: {
                validators: {
                        notEmpty: {
                        message: 'Kérjük adja meg a születési helyét!'
                    }
                }
            },
            birth_date: {
                validators: {
                    date: {
                        format: 'YYYY/MM/DD',
                        message: 'A következő formátumban adja meg a dátumot ÉÉÉÉ/HH/NN!'
                        
                    },
                    notEmpty: {
                        message: 'Kérjük adja meg születési idejét!'
                        	
                    },
                }
            },
            tax_num: {
                validators: {
                        notEmpty: {
                        message: 'Kérjük adja meg az adóazonosítóját!'
                    }
                }
            },
            contact_no: {
                validators: {
                	notEmpty: {
                        message: 'Kérjük adja meg telefonszámát!'
                     },
                     stringLength: {
                        min: 6,
                        message: 'Kérjük érvényes telefonszámot adjon meg!'                
                     },
                     digits: {
                         message: 'Kérjük csak számokat adjon meg!'
                     }
                }
            },
            country: {
                validators: {
                        notEmpty: {
                        message: 'Kérjük válasszon!'
                    }
                }
            },
            zip_code: {
                validators: {
                        notEmpty: {
                        message: 'Kérjük adja meg az irányítószámát!'
                    }
                }
            },
            city: {
                validators: {
                        notEmpty: {
                        message: 'Kérjük adja meg a várost!'
                    }
                }
            },
            address: {
                validators: {
                        notEmpty: {
                        message: 'Kérjük adja meg a címét!'
                    }
                }
            },
            gdpr: {
                validators: {
                	choice: {
                        min: 1,
                        message: 'A sikeres regisztrációhoz kérjük fogadja el a GDPR nyilatkozatot!'
                    }
                }
            },
            hourly_wage: {
                validators: {
                	 regexp: {
                         regexp:  '^([0-9]*\.?[0-9]+|[0-9]+\.?[0-9]*)$',
                         message: 'Kérjük érvényes Összeget adjon meg!'
                     }
                }
            },            
        }
        })
        .on('success.form.bv', function(e) {
                $('#contact_form').data('bootstrapValidator').resetForm();

            e.preventDefault();

            var $form = $(e.target);

            var bv = $form.data('bootstrapValidator');

            var datastring = $("#contact_form").serialize();
            
            $.ajax({
                type: "POST",
                url: "http://localhost:7180/StudentWorkRental/PersonalDataServlet",
                data: datastring,
                dataType: "json",
                success: function(data) {
                	if ("/StudentWorkRental/PersonalDataServlet"===data.redirectString) {
                			window.location.href = data.redirectString;
                		} else { 
                		    alert(data.redirectString)
                		}
                               	
                },
                error: function() {
                	
                }
            });         
        });  
             
    
 
});



