$(document).ready(function() {
	
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear()-14;
	 if(dd<10){
	        dd='0'+dd
	    } 
	    if(mm<10){
	        mm='0'+mm
	    } 

	today = yyyy+'-'+mm+'-'+dd;
	console.log(today);
	var datePicker= document.getElementById("datePicker");
	console.log(datePicker);
	datePicker.setAttribute("max", today);
	

	$('.datepicker')
    .on('changeDate show', function(e) {
        $('#contact_form').bootstrapValidator('revalidateField', 'birth_date');
    });
	
    $('#contact_form').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
        	email: {
                validators: {
                    notEmpty: {
                        message: 'Kérjük adja meg az Email címét!'
                    },
                    regexp: {
                        regexp:  /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i,
                        message: 'Kérjük érvényes Email címet adjon meg!'
                    }
                }
            },
            user_password: {
                validators: {
                     stringLength: {
                        min: 6,
                        message: 'Legalább 6 karakter hosszú jelszót adjon meg!'
                    },
                    notEmpty: {
                        message: 'Kérjük adja meg jelszavát!'
                    },
                    identical: {
                        field: 'confirm_password',
                        message: 'A két jelszó nem egyezik!'
                    }
                }
            },
			confirm_password: {
                validators: {
                     stringLength: {
                        min: 6,
                        message: 'Legalább 6 karakter hosszú jelszót adjon meg!'
                    },
                    notEmpty: {
                        message: 'Kérjük adja meg jelszavát!'
                    },
                    identical: {
                        field: 'user_password',
                        message: 'A két jelszó nem egyezik!'
                    }
                }
            },
            last_name: {
                validators: {
                    notEmpty: {
                        message: 'Kérjük adja meg a vezetéknevét!'
                    }
                }
            },
            first_name: {
                validators: {
                        notEmpty: {
                        message: 'Kérjük adja meg a keresztnevét!'
                    }
                }
            },
            gender: {
                validators: {
                        notEmpty: {
                        message: 'Kérjük válasszon!'
                    }
                }
            },
            mother_name: {
                validators: {
                        notEmpty: {
                        message: 'Kérjük adja meg az Anyja nevét!'
                    }
                }
            },
            birth_place: {
                validators: {
                        notEmpty: {
                        message: 'Kérjük adja meg a születési helyét!'
                    }
                }
            },
            birth_date: {
                validators: {
                    date: {
                        format: 'YYYY/MM/DD',
                        message: 'A következő formátumban adja meg a dátumot ÉÉÉÉ/HH/NN!'
                        
                    },
                    notEmpty: {
                        message: 'Kérjük adja meg születési idejét!'
                        	
                    },
                }
            },
            tax_num: {
                validators: {
                        notEmpty: {
                        message: 'Kérjük adja meg az adóazonosítóját!'
                    }
                }
            },
            contact_no: {
                validators: {
                	notEmpty: {
                        message: 'Kérjük adja meg telefonszámát!'
                     },
                     stringLength: {
                        min: 6,
                        message: 'Kérjük érvényes telefonszámot adjon meg!'                
                     },
                     digits: {
                         message: 'Kérjük csak számokat adjon meg!'
                     }
                }
            },
            country: {
                validators: {
                        notEmpty: {
                        message: 'Kérjük válasszon!'
                    }
                }
            },
            zip_code: {
                validators: {
                        notEmpty: {
                        message: 'Kérjük adja meg az irányítószámát!'
                    }
                }
            },
            city: {
                validators: {
                        notEmpty: {
                        message: 'Kérjük adja meg a várost!'
                    }
                }
            },
            address: {
                validators: {
                        notEmpty: {
                        message: 'Kérjük adja meg a címét!'
                    }
                }
            },
            gdpr: {
                validators: {
                	choice: {
                        min: 1,
                        message: 'A sikeres regisztrációhoz kérjük fogadja el a GDPR nyilatkozatot!'
                    }
                }
            },
            
        }
        })
        .on('success.form.bv', function(e) {
                $('#contact_form').data('bootstrapValidator').resetForm();

            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            var datastring = $("#contact_form").serialize();
            
            $.ajax({
                type: "POST",
                url: "/StudentWorkRental/RegisterServlet",
                data: datastring,
                dataType: "json",
                success: function(data) {
                	if ("/StudentWorkRental/LoginServlet"===data.redirectString) {
                			window.location.href = data.redirectString;
                		} else { 
                		    alert(data.redirectString)
                		}
                               	
                },
                error: function() {
                	
                }
            });         
        });  
   
    
 
});



