$(document).ready(function() {	
	
    $('#search_form').bootstrapValidator({
        fields: {
            hourlywage: {
                validators: {
                	 regexp: {
                         regexp:  '^([0-9]*\.?[0-9]+|[0-9]+\.?[0-9]*)$',
                         message: 'Kérjük számot adjon meg!'
                     }
                }
            },          
            worktime: {
                validators: {
                	 integer: {
                         message: 'Kérjük egész számot adjon meg!'
                     }
                }
            },            
        }
        })
        .on('success.form.bv', function(e) {
                $('#search_form').data('bootstrapValidator').resetForm();

            e.preventDefault();

            var $form = $(e.target);

            var bv = $form.data('bootstrapValidator');

            var datastring = $("#search_form").serialize();
            
            $.ajax({
                type: "POST",
                url: "/StudentWorkRental/SearchJobServlet",
                data: datastring,
                dataType: "json",
                success: function(data) {
                	
                			window.location.href = data.redirectString;
                               	
                },
                error: function() {
                	
                }
            });         
        });  
             
    
 
});
